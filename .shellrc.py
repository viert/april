from app.models import (
    User, Token, Session, Role, Team, Group,
    Location, Rack, Host, VirtualMachine, DBInstance, Selector,
    State, OsImage
)
from app.models.components import Component, Memory
from app.models.links import Link, Layout
from warnings import warn

warn("VirtualMachine model should not be used. It will be removed from the project in the nearest future")
