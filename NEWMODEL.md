## How to add a new model

Model must inherit RBACModel if it has its own API. Otherwise,
it's not necessary. 

### Steps

- add model
- consider indexes
- add model api descriptors
- if it's a rbac
  - add RBAC_NAMESPACE to model
  - add relation to model
  - add permissions section in app.lib.rbac
  - regenerate default roles to pick up new permissions
- add API handlers
