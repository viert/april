from croydon.taskq.task import BaseTask


class TeamCacheTask(BaseTask[None]):

    TYPE = "TASK_TEAM_CACHE"


TeamCacheTask.register()
