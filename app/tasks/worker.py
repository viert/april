from time import time
from croydon import ctx
from croydon.taskq.worker import BaseWorker
from croydon.taskq.types import TBaseTask
from app.models.team import Team


class Worker(BaseWorker):

    async def run_task(self, task: TBaseTask) -> None:
        from .team_cache_task import TeamCacheTask
        if task.TYPE == TeamCacheTask.TYPE:
            t1 = time()
            team_id = task.data.get("team_id")
            ctx.log.info(f"processing team cache for team_id {team_id}")
            t = await Team.get(team_id)
            if t is None:
                await task.done(f"team {team_id} was not found")
            await t.assign_descendants_cache()
            t2 = time()
            ctx.log.info(f"team cache for team_id {team_id} processed in %.3f seconds", t2-t1)

        else:
            raise NotImplemented(f"task type {task.TYPE} is not supported by worker")
