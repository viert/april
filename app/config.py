from pydantic import Field, BaseModel
from croydon.config import BaseConfig


class AprilConfig(BaseModel):
    enable_passwords: bool = True


class GoogleOAuthConfig(BaseModel):
    enabled: bool = False
    client_id: str = ""
    client_secret: str = ""
    redirect_base_uri: str = ""


class Config(BaseConfig):
    april: AprilConfig = Field(default_factory=AprilConfig)
    google_oauth: GoogleOAuthConfig = Field(default_factory=GoogleOAuthConfig)
