from typing import List, Optional, Dict, Any, TypeVar
from pydantic import BaseModel


class BIOSDef(BaseModel):
    bdate: Optional[str]
    bmanufacturer: Optional[str]
    bversion: Optional[str]
    skunumber: Optional[str]
    smanufacturer: Optional[str]
    smodel: Optional[str]
    ssn: Optional[str]


class CPUDef(BaseModel):
    arch: Optional[str]
    core: Optional[int]
    familyname: Optional[str]
    familynumber: Optional[str]
    id: Optional[str]
    manufacturer: Optional[str]
    model: Optional[str]
    name: Optional[str]
    speed: Optional[int]
    stepping: Optional[int]
    thread: Optional[int]
    
    def to_attrs(self) -> Dict[str, Any]:
        family_number = self.familynumber
        try:
            family_number = int(family_number)
        except ValueError:
            family_number = 0
        return dict(
            arch=self.arch,
            name=self.name,
            cores=self.core,
            family_name=self.familyname,
            family_number=family_number,
            manufacturer=self.manufacturer,
            model=self.model,
            cpu_id=self.id,
            speed=self.speed,
            threads=self.thread,
            stepping=self.stepping,
        )


class MemorySlotDef(BaseModel):
    caption: str
    description: Optional[str]
    memorycorrection: Optional[str]
    numslots: int
    type: Optional[str]
    

class MemoryDef(MemorySlotDef):
    capacity: int
    manufacturer: str
    model: str
    serialnumber: str
    speed: str

    def to_attrs(self) -> Dict[str, Any]:
        speed = self.speed
        try:
            speed = int(speed)
        except ValueError:
            speed = 0
        return dict(
            serial=self.serialnumber,
            capacity=self.capacity,
            description=self.description,
            manufacturer=self.manufacturer,
            model=self.model,
            speed=speed,
            type=self.type
        )


class TZDef(BaseModel):
    name: str
    offset: str


class OSDef(BaseModel):
    arch: str
    boot_time: str
    dns_domain: str
    fqdn: str
    full_name: str
    hostid: str
    install_date: str
    kernel_name: str
    kernel_version: str
    name: str
    ssh_key: str
    version: str
    timezone: TZDef


class VirtualMachine(BaseModel):
    vmtype: str
    status: str
    name: str
    uuid: str
    image: Optional[str] = None
    memory: Optional[int] = 0
    vcpu: Optional[int] = 0
    subsystem: Optional[str] = None


class NetworkDef(BaseModel):
    description: str
    ipaddress: Optional[str]
    ipaddress6: Optional[str]
    ipgateway: Optional[str]
    ipmask: Optional[str]
    ipmask6: Optional[str]
    ipsubnet: Optional[str]
    ipsubnet6: Optional[str]
    mac: Optional[str]
    speed: Optional[str]
    virtualdev: bool = False
    management: bool = False


class HardwareDef(BaseModel):
    chassis_type: Optional[str]
    defaultgateway: Optional[str]
    dns: Optional[str]
    memory: Optional[int]
    uuid: Optional[str]
    vmsystem: Optional[str]
    workgroup: Optional[str]


class InventoryContent(BaseModel):
    bios: BIOSDef
    cpus: List[CPUDef]
    memories: List[MemoryDef | MemorySlotDef]
    hardware: Optional[HardwareDef]
    operatingsystem: OSDef
    versionclient: str
    virtualmachines: Optional[List[VirtualMachine]]
    networks: Optional[List[NetworkDef]]


class Inventory(BaseModel):
    action: str
    content: InventoryContent
    deviceid: str
    itemtype: str
