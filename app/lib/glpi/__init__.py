import asyncio

from typing import List, Optional

from croydon import ctx
from croydon.errors import InputDataError
from croydon.util import now

from app.models import Host
from app.models.host import Interface, InterfaceNetwork, generate_serial
from app.models.components import CPU, Memory
from .types import Inventory, MemoryDef, NetworkDef


def group_interfaces(networks: List[NetworkDef]) -> List[Interface]:
    ifmap = {}
    for netdef in networks:
        network = None
        if netdef.ipaddress:
            network = InterfaceNetwork(
                family=4,
                ip=netdef.ipaddress,
                gw=netdef.ipgateway,
                mask=netdef.ipmask,
                subnet=netdef.ipsubnet
            )
        elif netdef.ipaddress6:
            network = InterfaceNetwork(
                family=6,
                ip=netdef.ipaddress6,
                mask=netdef.ipmask6,
                subnet=netdef.ipsubnet6
            )

        if netdef.description not in ifmap:
            if netdef.mac is not None:
                nets = [network] if network else []
                iface = Interface(
                    name=netdef.description,
                    mac=netdef.mac,
                    speed=netdef.speed,
                    mgmt=netdef.management,
                    virtual=netdef.virtualdev,
                    networks=nets
                )
                ifmap[netdef.description] = iface
        elif network:
            ifmap[netdef.description].networks.append(network)
    return [iface for iface in ifmap.values()]


async def glpi_agent_sync(data: Inventory) -> Optional[Host]:
    if data.action != "inventory":
        raise InputDataError("glpi inventory data is expected")

    ctx.log.info(f"importing host from glpi inventory {data.deviceid}")

    content = data.content
    if not content:
        raise InputDataError("glpi inventory detected but no content found")

    bios = content.bios
    cpus = content.cpus
    memories = content.memories
    os = content.operatingsystem
    vms = content.virtualmachines or []
    networks = content.networks or []

    serial = bios.ssn
    fqdn = os.fqdn
    interfaces = [iface.dict() for iface in group_interfaces(networks)]
    uuid = content.hardware.uuid if content.hardware is not None and content.hardware.uuid is not None else None

    model = bios.smodel
    if model == "KVM":
        ctx.log.debug(f"ignoring host {fqdn} due to it being a KVM")
        return None

    host = None
    if serial:
        host = await Host.find_one({"serial": serial})
        if not host:
            ctx.log.info(f"host not found by serial {serial}")

    if not host:
        host = await Host.get(fqdn)
        if not host:
            ctx.log.info(f"host not found by fqdn {fqdn}")

    if not host:
        host = Host.create(
            fqdn=fqdn,
            serial=serial or generate_serial(),
            manufacturer=bios.smanufacturer,
            model=bios.smodel,
            interfaces=interfaces,
            uuid=uuid,
            last_reported_at=now()
        )
        ctx.log.debug(f"creating host {host}")
        await host.save()
    else:
        serial = serial or host.serial or generate_serial()
        await host.update(dict(
            fqdn=fqdn,
            serial=serial,
            manufacturer=bios.smanufacturer,
            model=bios.smodel,
            interfaces=interfaces,
            uuid=uuid,
            last_reported_at=now()
        ))

    domain = host.fqdn.split(".")[-1]

    db_cpus = await CPU.find({"host_id": host.id}).all()
    if len(db_cpus) == len(cpus):
        for i, cpu in enumerate(cpus):
            update = cpu.to_attrs()
            await db_cpus[i].update(update)
    else:
        if len(db_cpus):
            await CPU.destroy_many({"host_id": host.id})
        for cpu in cpus:
            update = cpu.to_attrs()
            update["host_id"] = host.id
            db_cpu = CPU(update)
            await db_cpu.save()

    memories = [mem for mem in memories if isinstance(mem, MemoryDef)]
    db_mems = await Memory.find({"host_id": host.id}).all()

    if len(db_mems) == len(memories):
        for i, mem in enumerate(memories):
            update = mem.to_attrs()
            await db_mems[i].update(update)
    else:
        if len(db_mems):
            await Memory.destroy_many({"host_id": host.id})
        for mem in memories:
            update = mem.to_attrs()
            update["host_id"] = host.id
            db_mem = Memory(update)
            await db_mem.save()

    vm_ids = set(await Host.find_ids({"vm_host_id": host.id}))
    updates = []
    creates = []

    has_kvm = False
    has_docker = False

    for vm in vms:
        if vm.vmtype == "docker":
            has_docker = True
            continue

        has_kvm = True

        if "." not in vm.name:
            vm.name = f"{vm.name}.{domain}"

        machine = await Host.get(vm.name)
        if machine:
            if machine.id in vm_ids:
                vm_ids.remove(machine.id)
            update = machine.update({
                "status": vm.status,
                "uuid": vm.uuid,
                "last_reported_at": now()
            })
            updates.append(update)
        else:
            fqdn = vm.name
            if "." not in fqdn:
                domain = host.fqdn.split(".")[-1]
                fqdn = f"{fqdn}.{domain}"

            machine = Host.create(
                fqdn=fqdn,
                vm_type="kvm",
                status=vm.status,
                uuid=vm.uuid,
                vcpu=vm.vcpu,
                memory=vm.memory,
                vm_host_id=host.id,
                last_reported_at=now()
            )
            creates.append(machine.save())

    await Host.destroy_many({"_id": {"$in": list(vm_ids)}})
    await asyncio.gather(*updates, *creates)

    if has_kvm:
        host.add_local_tag("kvmhost")
    if has_docker:
        host.add_local_tag("dockerhost")
    await host.save()

    return host
