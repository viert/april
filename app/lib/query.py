from abc import abstractmethod, ABC
from typing import List, Dict, Any, Optional

import pymongo
from bson import ObjectId

from croydon.util import NilObjectId
from croydon.errors import NotFound, InputDataError
from croydon.types import TModelType

from app.extractors import NameFilter, AuthData
from app.lib.rbac.check import check_read
from app.lib.rbac.host import HostRelation
from app.lib.rbac.group import GroupRelation
from app.lib.rbac.user import UserRelation
from app.lib.rbac.team import TeamRelation
from app.lib.rbac.db import DBInstanceRelation
from app.models import Group, Host, Location, Team, User, DBInstance, Selector, Rack, Role, VirtualMachine, Layout


class BaseQueryBuilder(ABC):

    FILTER_FIELDS: List[str] = ["name"]
    CTOR: TModelType

    _components: List[Dict[str, Any]]
    _permissions: List[Dict[str, Any]]
    _final: bool
    _auth_data: AuthData
    _sort: Optional[List[str]] = None

    def __init__(self, auth_data: AuthData):
        self._auth_data = auth_data
        self._components = []
        self._permissions = []
        self._final = False

    def _add(self, component: Dict[str, Any]) -> None:
        if self._final:
            return
        self._components.append(component)

    def _add_permission(self, perm: Dict[str, Any]):
        self._permissions.append(perm)

    def cutoff(self):
        self._add({"_id": NilObjectId})
        self._final = True

    def ids(self, ids: Optional[List[ObjectId]]) -> None:
        if ids is not None:
            self._add({"_id": {"$in": ids}})

    def name_filter(self, flt: NameFilter) -> None:
        if flt is not None:
            if len(self.FILTER_FIELDS) == 0:
                pass
            elif len(self.FILTER_FIELDS) == 1:
                self._add({self.FILTER_FIELDS[0]: flt})
            else:
                self._add({"$or": [
                    {field: flt} for field in self.FILTER_FIELDS
                ]})

    @abstractmethod
    def permissions(self) -> None: ...

    def render_permissions(self) -> Optional[Dict[str, Any]]:
        if len(self._permissions) == 0:
            return None
        elif len(self._permissions) == 1:
            return self._permissions[0]
        else:
            return {"$or": self._permissions}

    def render_components(self) -> Optional[Dict[str, Any]]:
        if len(self._components) == 0:
            return None
        elif len(self._components) == 1:
            return self._components[0]
        else:
            keys_used = set()
            duplicates_found = False
            for component in self._components:
                if not duplicates_found:
                    for key in component:
                        if key in keys_used:
                            duplicates_found = True
                            break
                        keys_used.add(key)
            if not duplicates_found:
                # merge components
                query = {}
                for component in self._components:
                    query.update(component)
                return query
            else:
                return {"$and": self._components}

    def build(self) -> Dict[str, Any]:
        c = self.render_components()
        p = self.render_permissions()

        if p:
            if c:
                return {"$and": [c, p]}
            return p
        else:
            if c:
                return c
            return {}

    def sort(self, sort_opts: Optional[str]) -> None:
        if sort_opts is None:
            sort_opts = [self.CTOR.KEY_FIELD, "asc"]
        else:
            tokens = sort_opts.split(":")
            sort_field = tokens[0]
            if sort_field not in self.CTOR._fields:
                valid_fields = ",".join([f"\"{field}\"" for field in self.CTOR._fields.keys()])
                raise InputDataError(f"invalid sort field, valid fields are {valid_fields}")
            if len(tokens) < 2:
                sort_opts = [sort_field, "asc"]
            else:
                sort_direction = tokens[1]
                if sort_direction not in ["asc", "desc"]:
                    raise InputDataError(f"invalid sort direction \"{sort_direction}\", "
                                         f"valid values are \"asc\", \"desc\"")
                sort_opts = [sort_field, sort_direction]
        self._sort = sort_opts

    def find(self):
        query = self.build()
        if self._sort:
            sort = self._sort
        else:
            sort = [self.CTOR.KEY_FIELD, "asc"]

        sort[1] = pymongo.DESCENDING if sort[1] == "desc" else pymongo.ASCENDING

        return self.CTOR.find(query).sort(*sort)


class TaggedQueryBuilder(BaseQueryBuilder, ABC):

    def local_tags(self, tags: Optional[List[str]]) -> None:
        if tags is not None:
            self._add({"local_tags": {"$in": tags}})

    @abstractmethod
    async def tags(self, tags: Optional[List[str]]) -> None: ...


class UserQueryBuilder(BaseQueryBuilder):

    FILTER_FIELDS = ["username", "first_name", "last_name"]
    CTOR = User

    def permissions(self) -> None:
        perm_set = self._auth_data.permissions.users.cani("read")
        myself = perm_set[UserRelation.MYSELF.value]
        others = perm_set[UserRelation.OTHERS.value]

        if not myself and not others:
            self.cutoff()
            return

        show_all = myself and others
        if not show_all:
            if myself:
                self._add({"_id": self._auth_data.user.id})
            else:
                self._add({"_id": {"$ne": self._auth_data.user.id}})


class TeamQueryBuilder(BaseQueryBuilder):

    CTOR = Team

    def permissions(self, mine_only: bool = False) -> None:
        perm_set = self._auth_data.permissions.teams.cani("read")
        owned = perm_set[TeamRelation.OWNED.value]
        member = perm_set[TeamRelation.MEMBER.value]
        others = perm_set[TeamRelation.OTHERS.value] and not mine_only

        if not owned and not member and not others:
            self.cutoff()
            return

        show_all = owned and member and others
        if not show_all:
            if owned:
                self._add_permission({"owner_id": self._auth_data.user.id})
            if member:
                self._add_permission({"member_ids": self._auth_data.user.id})
            if others:
                self._add_permission({
                    "owner_id": {"$ne": self._auth_data.user.id},
                    "member_ids": {"$ne": self._auth_data.user.id}
                })


class HostQueryBuilder(TaggedQueryBuilder):

    FILTER_FIELDS = ["fqdn", "serial", "aliases"]
    CTOR = Host

    def permissions(self, mine_only: bool = False) -> None:
        perm_set = self._auth_data.permissions.hosts.cani("read")
        owned = perm_set[HostRelation.OWNED.value]
        others = perm_set[HostRelation.OTHERS.value] and not mine_only
        unowned = perm_set[HostRelation.UNOWNED.value] and not mine_only

        if not owned and not others and not unowned:
            self.cutoff()
            return

        show_all = owned and others and unowned
        if not show_all:
            if owned:
                self._add_permission({"owner_ids": self._auth_data.user.id})
            if others:
                self._add_permission({"owner_ids": {"$ne": self._auth_data.user.id}, "group_id": {"$ne": None}})
            if unowned:
                self._add_permission({"group_id": None})

    def groups(self, group_ids: Optional[List[ObjectId | None]]) -> None:
        if group_ids is not None:
            self._add({"group_id": {"$in": group_ids}})

    async def team(self, team_id: Optional[ObjectId]) -> None:
        if team_id is not None:
            group_ids = await Group.find_ids({"team_id": team_id})
            self.groups(group_ids)

    def state(self, state_id: Optional[ObjectId]) -> None:
        if state_id is not None:
            self._add({"state_id": state_id})

    def unowned_only(self) -> None:
        self._add({"group_id": None})

    def locations(self, location_ids: Optional[List[ObjectId | None]]) -> None:
        if location_ids is not None:
            self._add({"location_id": {"$in": location_ids}})

    async def tags(self, tags: Optional[List[str]]) -> None:
        if tags is not None:
            group_ids = await Group.find_ids_by_tags_recursive(tags)
            location_ids = await Location.find_ids_by_tags_recursive(tags)
            self._add({
                "$or": [
                    {"group_id": {"$in": group_ids}},
                    {"location_id": {"$in": location_ids}},
                    {"local_tags": {"$in": tags}}
                ]
            })

    def ip(self, ip: Optional[str]) -> None:
        if ip is not None:
            self._add({"interfaces.networks.ip": ip})

    def mac(self, mac: Optional[str]) -> None:
        if mac is not None:
            self._add({"interfaces.mac": mac})

    def vm_type(self, vm_type: Optional[str]) -> None:
        if vm_type is not None:
            self._add({"vm_type": vm_type})

    def vm_host(self, vm_host_id: Optional[ObjectId]) -> None:
        if vm_host_id is not None:
            self._add({"vm_host_id": vm_host_id})


class VirtualMachineQueryBuilder(HostQueryBuilder):

    FILTER_FIELDS = ["fqdn"]
    CTOR = VirtualMachine

    def host(self, host_id: Optional[ObjectId]) -> None:
        if host_id is not None:
            self._add({"host_id": host_id})

    async def locations(self, location_ids: Optional[List[ObjectId]]) -> None:
        if location_ids is not None:
            host_ids = await Host.find_ids({"location_id": {"$in": location_ids}})
            self._add({"host_id": {"$in": host_ids}})


class GroupQueryBuilder(TaggedQueryBuilder):

    CTOR = Group

    def permissions(self, mine_only: bool = False) -> None:
        perm_set = self._auth_data.permissions.teams.cani("read")
        owned = perm_set[GroupRelation.OWNED.value]
        others = perm_set[GroupRelation.OTHERS.value] and not mine_only

        if not owned and not others:
            self.cutoff()
            return

        show_all = owned and others
        if not show_all:
            if owned:
                self._add({"owner_ids": self._auth_data.user.id})
            else:
                self._add({"owner_ids": {"$ne": self._auth_data.user.id}})

    async def tags(self, tags: Optional[List[str]]) -> None:
        if tags is not None:
            group_ids = await Group.find_ids_by_tags_recursive(tags)
            self._add({"_id": {"$in": group_ids}})

    def team(self, team_id: Optional[ObjectId]) -> None:
        if team_id is not None:
            self._add({"team_id": team_id})


class RoleQueryBuilder(BaseQueryBuilder):

    CTOR = Role

    def permissions(self) -> None:
        if not self._auth_data.permissions.roles.all.read:
            self.cutoff()


class RackQueryBuilder(BaseQueryBuilder):

    FILTER_FIELDS = ["name", "full_name"]
    CTOR = Rack

    def permissions(self) -> None:
        if not self._auth_data.permissions.racks.all.read:
            self.cutoff()

    def locations(self, location_ids: Optional[List[ObjectId]]) -> None:
        if location_ids is not None:
            self._add({"location_id": {"$in": location_ids}})


class LocationQueryBuilder(BaseQueryBuilder):

    FILTER_FIELDS = ["name", "full_name"]
    CTOR = Location

    def permissions(self) -> None:
        if not self._auth_data.permissions.locations.all.read:
            self.cutoff()

    async def parent(self, parent_id: Optional[ObjectId]) -> None:
        if parent_id is not None:
            parent = await Location.get(parent_id, NotFound("parent not found"))
            check_read(self._auth_data, parent, NotFound("parent not found"))
            self._add({"parent_id": parent.id})

    def root_only(self):
        self._add({"parent_id": None})


class LayoutQueryBuilder(BaseQueryBuilder):

    CTOR = Layout

    def permissions(self, mine_only: bool = False) -> None:
        owned = self._auth_data.permissions.layouts.owned.read
        others = self._auth_data.permissions.layouts.others.read and not mine_only

        if not owned and not others:
            self.cutoff()
            return

        show_all = owned and others
        if not show_all:
            if owned:
                self._add({"owner_ids":  self._auth_data.user.id})
            else:
                self._add({"owner_ids":  {"$ne": self._auth_data.user.id}})


class SelectorQueryBuilder(BaseQueryBuilder):

    CTOR = Selector

    def permissions(self) -> None:
        if not self._auth_data.permissions.selectors.all.read:
            self.cutoff()

    def submodel(self, submodel: str) -> None:
        self._add({"submodel": submodel})


class DBInstanceQueryBuilder(BaseQueryBuilder):

    CTOR = DBInstance

    def permissions(self, mine_only: bool = False) -> None:
        perm_set = self._auth_data.permissions.db_instances.cani("read")
        owned = perm_set[DBInstanceRelation.OWNED.value]
        others = perm_set[DBInstanceRelation.OTHERS.value] and not mine_only
        unowned = perm_set[DBInstanceRelation.UNOWNED.value] and not mine_only

        if not owned and not others and not unowned:
            self.cutoff()
            return

        show_all = owned and others and unowned
        if not show_all:
            if owned:
                self._add_permission({"owner_ids": self._auth_data.user.id})
            if others:
                self._add_permission({"owner_ids": {"$ne": self._auth_data.user.id}, "team_id": {"$ne": None}})
            if unowned:
                self._add_permission({"team_id": None})

    def team(self, team_id: Optional[ObjectId]) -> None:
        if team_id is not None:
            self._add({"team_id": team_id})

    def host(self, host_id: Optional[ObjectId]) -> None:
        if host_id is not None:
            self._add({"host_id": host_id})

    def port(self, port: Optional[int]) -> None:
        if port is not None:
            self._add({"port": port})
