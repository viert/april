import asyncio
import math
from time import time
from typing import Generic, List, Optional, Callable, Awaitable
from pydantic import BaseModel
from pydantic.generics import GenericModel
from croydon.types import T, TModel
from croydon.db import ObjectsCursor
from croydon import ctx


class ItemsList(GenericModel, Generic[T]):
    data: List[T]


class PaginatedList(ItemsList, Generic[T]):
    count: int
    page: int
    total_pages: int


class PaginationParams(BaseModel):
    page: int
    limit: int


async def paginated(cur: ObjectsCursor[TModel],
                    *,
                    transform: Callable[[TModel], Awaitable[T]],
                    params: Optional[PaginationParams] = None,) -> ItemsList[T] | PaginatedList[T]:
    t1 = time()
    if params is None:
        items = await cur.all()
        tasks = [transform(x) for x in items]
        data = await asyncio.gather(*tasks)
        ctx.log.debug(f"paginated {cur.ctor.__self__.__name__} list with no_paging took %.3f secs", time() - t1)
        return ItemsList(data=data)

    offset = (params.page-1) * params.limit
    total_count = await cur.count()
    total_pages = math.ceil(total_count / params.limit)
    cur = cur.skip(offset).limit(params.limit)
    items = await cur.all()
    tasks = [transform(x) for x in items]
    data = await asyncio.gather(*tasks)
    ctx.log.debug(f"paginated {cur.ctor.__self__.__name__} list with limit={params.limit} took %.3f secs", time() - t1)
    return PaginatedList(count=total_count, page=params.page, total_pages=total_pages, data=data)
