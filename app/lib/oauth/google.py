import aiohttp
from croydon import ctx
from urllib.parse import urlencode
from typing import Optional, Dict

SCOPES = [
    "https://www.googleapis.com/auth/userinfo.profile"
]


class GoogleOAuthError(Exception):
    response: Dict[str, str]

    def __init__(self, response: Dict[str, str]):
        super().__init__("google oauth error")
        self.response = response


def get_oauth_authorize_url() -> Optional[str]:
    if not ctx.cfg.google_oauth.enabled:
        return None

    params = {
        "client_id": ctx.cfg.google_oauth.client_id,
        "client_secret": ctx.cfg.google_oauth.client_secret,
        "redirect_uri": ctx.cfg.google_oauth.redirect_base_uri,
        "scope": " ".join(SCOPES),
        "access_type": "offline",
        "include_granted_scopes": "true",
    }

    return "https://accounts.google.com/o/oauth2/v2/auth?" + urlencode(params)


async def fetch_token(code: str) -> str:

    form = aiohttp.FormData()
    form.add_field("code", code)
    form.add_field("client_id", ctx.cfg.google_oauth.client_id)
    form.add_field("client_secret", ctx.cfg.google_oauth.client_secret)
    form.add_field("redirect_uri", f"{ctx.cfg.google_oauth.redirect_base_uri}/api/v1/account/oauth")
    form.add_field("grant_type", "authorization_code")

    async with aiohttp.ClientSession() as s:
        async with s.post("https://oauth2.googleapis.com/token", data=form) as response:
            data = await response.json()
            if "access_token" in data:
                return data["access_token"]
            raise GoogleOAuthError(data)


async def load_userinfo(token: str) -> Dict[str, str]:
    headers = {"Authorization": f"Bearer {token}"}
    async with aiohttp.ClientSession() as s:
        async with s.get("https://www.googleapis.com/oauth2/v3/userinfo", headers=headers) as response:
            return await response.json()
