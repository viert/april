import re
import string
from typing import Optional, Generator, Tuple, List
from dataclasses import dataclass
from croydon.errors import InputDataError

ALPHA_LOWER = string.ascii_lowercase
ALPHA_UPPER = string.ascii_uppercase
DIGITS = string.digits
GLOB_EXPR = re.compile(r"([0-9a-zA-Z]+)-([0-9a-zA-Z]+)")


class ExtrapolationError(InputDataError):
    error_key = "extrapolation_error"


class ParseError(InputDataError):
    error_key = "parse_error"


class RubyLikeString(str):
    """
    Ruby-style string extensions
    """

    @dataclass
    class CharExtrapolation:
        char: str
        alphabet: str
        carry: bool

    @staticmethod
    def _symbol_alphabet(sym: str) -> str | None:
        if ALPHA_LOWER[0] <= sym <= ALPHA_LOWER[-1]:
            return ALPHA_LOWER
        if ALPHA_UPPER[0] <= sym <= ALPHA_UPPER[-1]:
            return ALPHA_UPPER
        if DIGITS[0] <= sym <= DIGITS[-1]:
            return DIGITS
        return None

    def _extrapolate_char(self, char: str) -> Optional[CharExtrapolation]:
        alphabet = self._symbol_alphabet(char)
        if alphabet is None:
            return None

        carry = False
        idx = alphabet.index(char) + 1
        if idx >= len(alphabet):
            carry = True
            idx = 0
        return RubyLikeString.CharExtrapolation(
            char=alphabet[idx],
            alphabet=alphabet,
            carry=carry
        )

    def succ(self):
        """
        Generates a next string in the sequence according to ruby-style char alphabet
        """
        res = ""
        expl = RubyLikeString.CharExtrapolation(char="", alphabet=DIGITS, carry=True)
        for char in reversed(self):
            if expl and expl.carry:
                expl = self._extrapolate_char(char)
                if expl is None:
                    raise ExtrapolationError(f"string \"{self}\" has chars impossible to extrapolate")
                res += expl.char
            else:
                res += char
        if expl and expl.carry:
            if expl.alphabet is DIGITS:
                res += expl.alphabet[1]
            else:
                res += expl.alphabet[0]

        return RubyLikeString("".join(reversed(res)))

    @classmethod
    def range(cls, start: str, end: str, exclude_last: bool = False) -> Generator["RubyLikeString", None, None]:
        """
        Ruby-style string range generator
        Ruby's "c9".."z4" is equal to RubyLikeString.range("c9", "z4", False)

        There's no easy way to check if incrementing :start: would finally lead to :end:,
        so there is a length check to avoid infinite generating
        """
        s = RubyLikeString(start)
        while s != end and len(s) <= len(end):
            yield s
            s = s.succ()
        if s == end and not exclude_last:
            yield s


def expand_single_glob(s: str) -> Generator[str, None, None]:
    if s.startswith("[") and s.endswith("]"):
        s = s[1:-1]

    patterns = s.split(",")
    for pattern in patterns:
        matches = GLOB_EXPR.search(pattern)
        if not matches:
            # a single variant
            yield pattern
        else:
            for variant in RubyLikeString.range(*matches.groups()):
                yield variant


def find_first_glob(s: str) -> Optional[Tuple[int, int]]:
    """
    finds brackets and check for their integrity
    """
    opening_found = False
    opening_index: int | None = None

    for idx, char in enumerate(s):
        if char == "[":
            if opening_found:
                raise ParseError("nested brackets are not allowed")
            opening_found = True
            opening_index = idx
        elif char == "]":
            if not opening_found:
                raise ParseError("closing bracket before an opening one")
            return opening_index, idx

    if opening_found:
        raise ParseError("no closing bracket found")
    return None


def expand(s: str) -> Generator[str, None, None]:
    indices = find_first_glob(s)

    if not indices:
        yield s
        return

    start, end = indices

    prefix = s[:start]
    suffix = s[end+1:]
    pattern = s[start+1:end]

    for variant in expand_single_glob(pattern):
        yield from expand(prefix + variant + suffix)


def expand_with_matches(s: str,
                        matches: Optional[List[str]] = None) -> Generator[Tuple[str, List[str]], None, None]:
    if matches is None:
        matches = []

    indices = find_first_glob(s)

    if not indices:
        yield s, [s] + matches
        return

    start, end = indices

    prefix = s[:start]
    suffix = s[end+1:]
    pattern = s[start+1:end]

    for variant in expand_single_glob(pattern):
        yield from expand_with_matches(prefix + variant + suffix, matches + [variant])


def apply_matches(s: str, matches: List[str]) -> str:
    for idx, match in enumerate(matches):
        s = s.replace(f"${idx}", match)
    return s
