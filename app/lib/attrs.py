from typing import Dict, Any
from copy import deepcopy


def merge(parent: Dict[str, Any], child: Dict[str, Any]) -> Dict[str, Any]:
    result = deepcopy(parent)
    for key in child:
        if key in parent and isinstance(parent[key], dict) and isinstance(child[key], dict):
            result[key] = merge(parent[key], child[key])
        else:
            result[key] = child[key]
    return result
