from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class RoleRelation(BaseRelation):
    ALL = "all"


class RolePermissions(BasePermissions):
    set_permissions: bool = False


class RolePermissionsRelated(BasePermissionsRelated):
    all: RolePermissions = Field(default_factory=RolePermissions)
