from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class TeamRelation(BaseRelation):
    OWNED = "owned"
    MEMBER = "member"
    OTHERS = "others"


class TeamPermissions(BasePermissions):

    set_members: bool = False
    change_owner: bool = False


class TeamPermissionsRelated(BasePermissionsRelated):

    owned: TeamPermissions = Field(default_factory=TeamPermissions)
    member: TeamPermissions = Field(default_factory=TeamPermissions)
    others: TeamPermissions = Field(default_factory=TeamPermissions)

    def get(self, relation: TeamRelation) -> TeamPermissions:
        return getattr(self, relation.value)
