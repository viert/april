from typing import TypeVar, Dict
from pydantic import BaseModel
from enum import Enum


class BasePermissions(BaseModel):

    create: bool = False
    read: bool = False
    update: bool = False
    destroy: bool = False


class BasePermissionsRelated(BaseModel):

    def cani(self, action: str) -> Dict[str, bool]:
        return {
            field: getattr(getattr(self, field), action)
            for field in self.__fields__.keys()
        }


class BaseRelation(Enum):
    pass


TBaseRelation = TypeVar("TBaseRelation", bound=BaseRelation)
