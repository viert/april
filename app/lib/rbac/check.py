from typing import Optional
from croydon.errors import ApiError
from app.extractors import AuthData
from .rbac_model_mixin import TRBACModel


def check_read(auth_data: AuthData, target: TRBACModel, error: ApiError) -> None:
    rel = target.relation(auth_data.user)
    if not auth_data.permissions.check(target.RBAC_NAMESPACE.value, rel.value, "read"):
        raise error


def check_read_and_update(
        auth_data: AuthData,
        target: TRBACModel,
        read_error: ApiError,
        update_error: Optional[ApiError] = None) -> None:
    rel = target.relation(auth_data.user)

    read = auth_data.permissions.check(target.RBAC_NAMESPACE.value, rel.value, "read")
    if not read:
        raise read_error

    update = auth_data.permissions.check(target.RBAC_NAMESPACE.value, rel.value, "update")
    if not update:
        if update_error:
            raise update_error
        raise read_error


def check_read_and_destroy(
        auth_data: AuthData,
        target: TRBACModel,
        read_error: ApiError,
        destroy_error: Optional[ApiError] = None) -> None:
    rel = target.relation(auth_data.user)

    read = auth_data.permissions.check(target.RBAC_NAMESPACE.value, rel.value, "read")
    if not read:
        raise read_error

    destroy = auth_data.permissions.check(target.RBAC_NAMESPACE.value, rel.value, "destroy")
    if not destroy:
        if destroy_error:
            raise destroy_error
        raise read_error
