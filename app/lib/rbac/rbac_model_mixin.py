from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, TypeVar, List
from croydon.models import StorableModel
from . import PermissionsNamespace
from .base import TBaseRelation
if TYPE_CHECKING:
    from app.models.user import User


class RBACModel(StorableModel, ABC):

    RBAC_NAMESPACE: PermissionsNamespace

    @abstractmethod
    def relation(self, user: "User") -> TBaseRelation: ...

    @classmethod
    def exposed_fields(cls) -> List[str]:
        # All models also expose "rel" field which requires additional
        # arguments to be passed from an API request data so can't be
        # exposed with @api_field decorator
        return super().exposed_fields() + ["rel"]


TRBACModel = TypeVar("TRBACModel", bound=RBACModel)
