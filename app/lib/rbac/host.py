from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class HostRelation(BaseRelation):
    OWNED = "owned"
    UNOWNED = "unowned"
    OTHERS = "others"


class HostPermissions(BasePermissions):
    set_state: bool = False


class HostPermissionsRelated(BasePermissionsRelated):

    owned: HostPermissions = Field(default_factory=HostPermissions)
    others: HostPermissions = Field(default_factory=HostPermissions)
    unowned: HostPermissions = Field(default_factory=HostPermissions)
