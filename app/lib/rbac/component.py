from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class ComponentRelation(BaseRelation):
    ALL = "all"


class ComponentPermissions(BasePermissions):
    pass


class ComponentPermissionsRelated(BasePermissionsRelated):
    all: ComponentPermissions = Field(default_factory=ComponentPermissions)
