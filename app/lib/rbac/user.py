from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class UserRelation(BaseRelation):
    MYSELF = "myself"
    OTHERS = "others"


class UserPermissions(BasePermissions):

    read_tokens: bool = False
    create_tokens: bool = False
    revoke_tokens: bool = False
    assign_roles: bool = False
    assign_exceeding_roles: bool = False


class UserPermissionsRelated(BasePermissionsRelated):

    myself: UserPermissions = Field(default_factory=UserPermissions)
    others: UserPermissions = Field(default_factory=UserPermissions)

    def get(self, relation: UserRelation) -> UserPermissions:
        return getattr(self, relation.value)
