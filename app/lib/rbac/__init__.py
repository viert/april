from pydantic import BaseModel, Field
from enum import Enum
from .user import UserPermissionsRelated, UserPermissions
from .team import TeamPermissionsRelated, TeamPermissions
from .group import GroupPermissionsRelated, GroupPermissions
from .host import HostPermissionsRelated, HostPermissions
from .location import LocationPermissionsRelated, LocationPermissions
from .role import RolePermissionsRelated, RolePermissions
from .rack import RackPermissionsRelated, RackPermissions
from .component import ComponentPermissionsRelated, ComponentPermissions
from .layout import LayoutPermissionsRelated, LayoutPermissions
from .virtual_machine import VirtualMachinePermissionsRelated, VirtualMachinePermissions
from .selector import SelectorPermissionsRelated, SelectorPermissions
from .db import DBInstancePermissionsRelated, DBInstancePermissions


class PermissionsNamespace(Enum):
    USERS = "users"
    TEAMS = "teams"
    GROUPS = "groups"
    HOSTS = "hosts"
    VIRTUAL_MACHINES = "virtual_machines"
    LOCATIONS = "locations"
    RACKS = "racks"
    ROLES = "roles"
    COMPONENTS = "components"
    LAYOUTS = "layouts"
    SELECTORS = "selectors"
    DATABASE_INSTANCES = "db_instances"


class Permissions(BaseModel):

    users: UserPermissionsRelated = Field(default_factory=UserPermissionsRelated)
    teams: TeamPermissionsRelated = Field(default_factory=TeamPermissionsRelated)
    groups: GroupPermissionsRelated = Field(default_factory=GroupPermissionsRelated)
    hosts: HostPermissionsRelated = Field(default_factory=HostPermissionsRelated)
    locations: LocationPermissionsRelated = Field(default_factory=LocationPermissionsRelated)
    roles: RolePermissionsRelated = Field(default_factory=RolePermissionsRelated)
    racks: RackPermissionsRelated = Field(default_factory=RackPermissionsRelated)
    components: ComponentPermissionsRelated = Field(default_factory=ComponentPermissionsRelated)
    layouts: LayoutPermissionsRelated = Field(default_factory=LayoutPermissionsRelated)
    virtual_machines: VirtualMachinePermissionsRelated = Field(default_factory=VirtualMachinePermissionsRelated)
    selectors: SelectorPermissionsRelated = Field(default_factory=SelectorPermissionsRelated)
    db_instances: DBInstancePermissionsRelated = Field(default_factory=DBInstancePermissionsRelated)

    def check(self, namespace: str, relation: str, action: str) -> bool:
        p = self.dict()
        pr = p.get(namespace)
        if pr is None:
            return False
        rel = pr.get(relation)
        if rel is None:
            return False
        return rel[action] or False

    def binary(self) -> int:
        acc = ""
        dct = self.dict()

        for i in sorted(dct.keys()):
            for j in sorted(dct[i].keys()):
                for k in sorted(dct[i][j].keys()):
                    value = "1" if dct[i][j][k] else "0"
                    acc = value + acc

        return int(acc, 2)

    def __eq__(self, other: "Permissions") -> bool:
        return self.binary() == other.binary()

    def __ne__(self, other: "Permissions") -> bool:
        return self.binary() != other.binary()

    def exceeds(self, other: "Permissions") -> bool:
        """
        "exceeds" in the context of permissions means one set of permissions
        has some property the other one does not have.

        This also means that
            **p1 can exceed p2**
        and at the same time
            **p2 can exceed p1**

        Example:
        p1 has hosts.unowned.read set to True, all the other props are False
        p2 has hosts.unowned.update set to True, all the others a re False

        p1 > p2 because p2 can't do something p1 can: read unowned hosts
        p2 > p1 because p1 can't update unowned hosts whilst p2 can

        Given this, the most efficient way of checking if a permissions set
        exceeds another is to binmask the other with it and see if it's changed
        """
        value = self.binary()
        other_value = other.binary()
        return other_value != value and value & other_value != value


DEFAULT_PERMISSIONS = Permissions(
    users=UserPermissionsRelated(
        myself=UserPermissions(
            read=True,
            update=True,
            read_tokens=True,
            revoke_tokens=True,
            create_tokens=True,
        ),
        others=UserPermissions(
            read=True,
        )
    ),
    teams=TeamPermissionsRelated(
        owned=TeamPermissions(
            read=True,
            update=True,
            destroy=True,
            set_members=True,
            change_owner=True,
        ),
        member=TeamPermissions(
            read=True,
            update=True,
        ),
        others=TeamPermissions(
            read=True,
            create=True,
        ),
    ),
    groups=GroupPermissionsRelated(
        owned=GroupPermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        others=GroupPermissions(
            read=True,
            create=True,
        ),
    ),
    hosts=HostPermissionsRelated(
        owned=HostPermissions(
            read=True,
            update=True,
            destroy=True,
            set_state=True,
        ),
        others=HostPermissions(read=True),
        unowned=HostPermissions(
            read=True,
            create=True,
            update=True,
            set_state=True,
        ),
    ),
    virtual_machines=VirtualMachinePermissionsRelated(
        owned=VirtualMachinePermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        others=VirtualMachinePermissions(read=True),
        unowned=VirtualMachinePermissions(
            read=True,
            create=True,
            update=True,
        ),
    ),
    locations=LocationPermissionsRelated(
        all=LocationPermissions(read=True)
    ),
    racks=RackPermissionsRelated(
        all=RackPermissions(read=True)
    ),
    components=ComponentPermissionsRelated(
        all=ComponentPermissions(read=True)
    ),
    roles=RolePermissionsRelated(
        all=RolePermissions(
            read=True, create=True
        )
    ),
    layouts=LayoutPermissionsRelated(
        owned=LayoutPermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        others=LayoutPermissions(
            read=True,
            create=True
        )
    ),
    selectors=SelectorPermissionsRelated(
        all=SelectorPermissions(
            read=True,
        )
    ),
    db_instances=DBInstancePermissionsRelated(
        owned=DBInstancePermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        others=DBInstancePermissions(read=True),
        unowned=DBInstancePermissions(
            read=True,
            create=True,
            update=True,
        ),
    ),

)

SUPERUSER_PERMISSIONS = Permissions(
    users=UserPermissionsRelated(
        myself=UserPermissions(
            read=True,
            update=True,
            destroy=False,
            read_tokens=True,
            revoke_tokens=True,
            create_tokens=True,
        ),
        others=UserPermissions(
            read=True,
            update=True,
            create=True,
            destroy=True,
            read_tokens=True,
            revoke_tokens=True,
            create_tokens=True,
            assign_roles=True,
            assign_exceeding_roles=True,
        )
    ),
    teams=TeamPermissionsRelated(
        owned=TeamPermissions(
            read=True,
            update=True,
            destroy=True,
            set_members=True,
            change_owner=True,
        ),
        member=TeamPermissions(
            read=True,
            update=True,
            destroy=True,
            set_members=True,
            change_owner=True,
        ),
        others=TeamPermissions(
            read=True,
            update=True,
            destroy=True,
            set_members=True,
            change_owner=True,
            create=True
        ),
    ),
    groups=GroupPermissionsRelated(
        owned=GroupPermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        others=GroupPermissions(
            read=True,
            update=True,
            destroy=True,
            create=True,
        ),
    ),
    hosts=HostPermissionsRelated(
        owned=HostPermissions(
            read=True,
            update=True,
            destroy=True,
            set_state=True,
        ),
        others=HostPermissions(
            read=True,
            update=True,
            destroy=True,
            set_state=True,
        ),
        unowned=HostPermissions(
            read=True,
            create=True,
            update=True,
            destroy=True,
            set_state=True,
        ),
    ),
    virtual_machines=VirtualMachinePermissionsRelated(
        owned=VirtualMachinePermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        others=VirtualMachinePermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        unowned=VirtualMachinePermissions(
            read=True,
            create=True,
            update=True,
            destroy=True,
        ),
    ),
    locations=LocationPermissionsRelated(
        all=LocationPermissions(
            read=True,
            create=True,
            update=True,
            destroy=True,
        )
    ),
    racks=RackPermissionsRelated(
        all=RackPermissions(
            read=True,
            create=True,
            update=True,
            destroy=True,
        )
    ),
    components=ComponentPermissionsRelated(
        all=ComponentPermissions(
            read=True,
            create=True,
            update=True,
            destroy=True,
        )
    ),
    roles=RolePermissionsRelated(
        all=RolePermissions(
            read=True,
            create=True,
            update=True,
            destroy=True,
        )
    ),
    layouts=LayoutPermissionsRelated(
        owned=LayoutPermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        others=LayoutPermissions(
            read=True,
            update=True,
            destroy=True,
            create=True
        )
    ),
    selectors=SelectorPermissionsRelated(
        all=SelectorPermissions(
            read=True,
            update=True,
            create=True,
            destroy=True
        )
    ),
    db_instances=DBInstancePermissionsRelated(
        owned=DBInstancePermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        others=DBInstancePermissions(
            read=True,
            update=True,
            destroy=True,
        ),
        unowned=DBInstancePermissions(
            read=True,
            create=True,
            update=True,
            destroy=True,
        ),
    ),

)

INVENTORY_PERMISSIONS = Permissions(
    users=UserPermissionsRelated(
        myself=UserPermissions(
            read=True,
        ),
    ),
    hosts=HostPermissionsRelated(
        owned=HostPermissions(
            read=True,
            update=True,
            set_state=True,
        ),
        others=HostPermissions(
            read=True,
            update=True,
            set_state=True,
        ),
        unowned=HostPermissions(
            read=True,
            create=True,
            update=True,
            set_state=True,
        ),
    ),
    virtual_machines=VirtualMachinePermissionsRelated(
        owned=VirtualMachinePermissions(
            read=True,
            update=True,
        ),
        others=VirtualMachinePermissions(
            read=True,
            update=True,
        ),
        unowned=VirtualMachinePermissions(
            read=True,
            create=True,
            update=True,
        ),
    ),
    components=ComponentPermissionsRelated(
        all=ComponentPermissions(
            read=True,
            create=True,
            update=True,
            destroy=True,
        )
    ),
    db_instances=DBInstancePermissionsRelated(
        owned=DBInstancePermissions(
            read=True,
            update=True,
        ),
        others=DBInstancePermissions(
            read=True,
            update=True,
        ),
        unowned=DBInstancePermissions(
            read=True,
            create=True,
            update=True,
        ),
    ),
    selectors=SelectorPermissionsRelated(
        all=SelectorPermissions(
            read=True,
        )
    ),
)
