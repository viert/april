from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class VirtualMachineRelation(BaseRelation):
    OWNED = "owned"
    UNOWNED = "unowned"
    OTHERS = "others"


class VirtualMachinePermissions(BasePermissions):
    pass


class VirtualMachinePermissionsRelated(BasePermissionsRelated):

    owned: VirtualMachinePermissions = Field(default_factory=VirtualMachinePermissions)
    others: VirtualMachinePermissions = Field(default_factory=VirtualMachinePermissions)
    unowned: VirtualMachinePermissions = Field(default_factory=VirtualMachinePermissions)
