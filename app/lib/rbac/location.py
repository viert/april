from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class LocationRelation(BaseRelation):
    ALL = "all"


class LocationPermissions(BasePermissions):
    pass


class LocationPermissionsRelated(BasePermissionsRelated):
    all: LocationPermissions = Field(default_factory=LocationPermissions)
