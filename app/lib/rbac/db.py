from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class DBInstanceRelation(BaseRelation):
    OWNED = "owned"
    UNOWNED = "unowned"
    OTHERS = "others"


class DBInstancePermissions(BasePermissions):
    pass


class DBInstancePermissionsRelated(BasePermissionsRelated):

    owned: DBInstancePermissions = Field(default_factory=DBInstancePermissions)
    others: DBInstancePermissions = Field(default_factory=DBInstancePermissions)
    unowned: DBInstancePermissions = Field(default_factory=DBInstancePermissions)
