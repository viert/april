from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class GroupRelation(BaseRelation):
    OWNED = "owned"
    OTHERS = "others"


class GroupPermissions(BasePermissions):

    set_protected_props: bool = False


class GroupPermissionsRelated(BasePermissionsRelated):

    owned: GroupPermissions = Field(default_factory=GroupPermissions)
    others: GroupPermissions = Field(default_factory=GroupPermissions)

