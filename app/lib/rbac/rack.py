from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class RackRelation(BaseRelation):
    ALL = "all"


class RackPermissions(BasePermissions):
    pass


class RackPermissionsRelated(BasePermissionsRelated):
    all: RackPermissions = Field(default_factory=RackPermissions)
