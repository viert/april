from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class LayoutRelation(BaseRelation):
    OWNED = "owned"
    OTHERS = "others"


class LayoutPermissions(BasePermissions):
    pass


class LayoutPermissionsRelated(BasePermissionsRelated):
    owned: LayoutPermissions = Field(default_factory=LayoutPermissions)
    others: LayoutPermissions = Field(default_factory=LayoutPermissions)
