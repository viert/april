from pydantic import Field
from .base import BasePermissions, BasePermissionsRelated, BaseRelation


class SelectorRelation(BaseRelation):
    ALL = "all"


class SelectorPermissions(BasePermissions):
    pass


class SelectorPermissionsRelated(BasePermissionsRelated):
    all: SelectorPermissions = Field(default_factory=SelectorPermissions)
