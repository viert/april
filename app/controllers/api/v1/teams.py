import asyncio
from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon.errors import NotFound, Forbidden, InputDataError

from app.models import Team, User
from app.models.api.team import TeamResponse, TeamRequest, TeamSetOwnerRequest, TeamSetMembersRequest
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    ObjectIdListParam,
    StringListParam,
    user_and_permissions,
    pagination_params,
    name_filter
)
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.query import TeamQueryBuilder

teams_ctrl = APIRouter(prefix="/api/v1/teams")


def teams_transform(fields: Optional[List[str]], auth_data: AuthData) -> Callable[[Team], Awaitable[TeamResponse]]:
    need_rel = fields and "rel" in fields

    async def transform(t: Team) -> TeamResponse:
        dct = await t.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = t.relation(auth_data.user).value
        return TeamResponse(**dct)
    return transform


@teams_ctrl.get("/", tags=["teams"], name="Teams index and search", response_model_exclude_unset=True)
async def index(
        auth_data: AuthData = Depends(user_and_permissions),
        pagination: Optional[PaginationParams] = Depends(pagination_params),
        mine_only: bool = Query(False),
        ids: Optional[ObjectIdListParam] = Query(default=None),
        fields: Optional[StringListParam] = Query(default=None),
        sort: Optional[str] = Query(default=None),
        name: NameFilter = Depends(name_filter),
) -> PaginatedList[TeamResponse] | ItemsList[TeamResponse]:

    qb = TeamQueryBuilder(auth_data)
    qb.permissions(mine_only=mine_only)
    qb.name_filter(name)
    qb.ids(ids)
    qb.sort(sort)

    teams = qb.find()
    return await paginated(teams, transform=teams_transform(fields, auth_data), params=pagination)


@teams_ctrl.get("/{team_id}", tags=["teams"], name="Get a team", response_model_exclude_unset=True)
async def show(team_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> TeamResponse:
    team = await Team.get(team_id, NotFound("team not found"))
    check_read(auth_data, team, NotFound("team not found"))
    return await teams_transform(fields, auth_data)(team)


@teams_ctrl.post("/", tags=["teams"], name="Create a team", status_code=201)
async def create(body: TeamRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[TeamResponse]:
    perm = auth_data.permissions
    if not perm.teams.others.create:
        raise Forbidden("no permissions to create new teams")

    team = Team.from_pydantic(body)
    team.owner_id = auth_data.user.id
    await team.save()

    return StatusResponse(
        status="team created",
        data=await teams_transform(fields, auth_data)(team)
    )


@teams_ctrl.patch("/{team_id}", tags=["teams"], name="Update a team")
async def update(team_id: str,
                 body: TeamRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[TeamResponse]:
    team = await Team.get(team_id, NotFound("team not found"))
    check_read_and_update(
        auth_data,
        team,
        NotFound("team not found"),
        Forbidden("no permissions to modify this team")
    )
    await team.update_from_pydantic(body)
    return StatusResponse(
        status="team updated",
        data=await teams_transform(fields, auth_data)(team)
    )


@teams_ctrl.delete("/{team_id}", tags=["teams"], name="Delete a team")
async def delete(team_id: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[TeamResponse]:
    team = await Team.get(team_id, NotFound("team not found"))
    check_read_and_destroy(
        auth_data,
        team,
        NotFound("team not found"),
        Forbidden("no permissions to delete this team")
    )

    await team.destroy()

    return StatusResponse(
        status="team destroyed",
        data=await teams_transform(fields, auth_data)(team)
    )


@teams_ctrl.post("/{team_id}/owner", tags=["teams"], name="Set a new owner of a team")
async def set_owner(team_id: str,
                    body: TeamSetOwnerRequest,
                    fields: Optional[StringListParam] = Query(default=None),
                    auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[TeamResponse]:
    team = await Team.get(team_id, NotFound("team not found"))
    check_read(auth_data, team, NotFound("team not found"))

    allowed = auth_data.permissions.teams.get(team.relation(auth_data.user)).change_owner
    if not allowed:
        raise Forbidden("no permissions to modify this team's owner")

    new_owner = await User.get(body.owner_id, NotFound("new owner not found"))
    if new_owner.id == team.owner_id:
        raise InputDataError("new owner matches the current one")

    team.owner_id = new_owner.id
    await team.save()

    return StatusResponse(
        status="team owner set",
        data=await teams_transform(fields, auth_data)(team)
    )


@teams_ctrl.post("/{team_id}/members", tags=["teams"], name="Set team members")
async def set_members(team_id: str,
                      body: TeamSetMembersRequest,
                      fields: Optional[StringListParam] = Query(default=None),
                      auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[TeamResponse]:
    team = await Team.get(team_id, NotFound("team not found"))
    check_read(auth_data, team, NotFound("team not found"))

    allowed = auth_data.permissions.teams.get(team.relation(auth_data.user)).set_members
    if not allowed:
        raise Forbidden("no permissions to modify this team's member list")

    tasks = [User.get(id_, NotFound(f"user \"{id_}\" not found")) for id_ in body.member_ids]
    users = await asyncio.gather(*tasks)

    users = filter(lambda u: u.id != team.owner_id, users)
    member_ids = [u.id for u in users]

    team.member_ids = member_ids
    await team.save()

    return StatusResponse(
        status="team member list set",
        data=await teams_transform(fields, auth_data)(team)
    )
