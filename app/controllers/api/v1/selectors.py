from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon.errors import NotFound, Forbidden, InputDataError

from app.models.selector import Selector
from app.models.api.selector import SelectorResponse, SelectorRequest
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    StringListParam,
    user_and_permissions,
    pagination_params,
    name_filter
)
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.query import SelectorQueryBuilder

selectors_ctrl = APIRouter(prefix="/api/v1/selectors")


def selectors_transform(fields: Optional[List[str]],
                        auth_data: AuthData) -> Callable[[Selector], Awaitable[SelectorResponse]]:
    need_rel = fields and "rel" in fields

    async def transform(s: Selector) -> SelectorResponse:
        dct = await s.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = s.relation(auth_data.user).value
        return SelectorResponse(**dct)
    return transform


@selectors_ctrl.get("/{submodel}/", tags=["selectors"], name="Selectors index and search", response_model_exclude_unset=True)
async def index(
        submodel: str,
        auth_data: AuthData = Depends(user_and_permissions),
        pagination: Optional[PaginationParams] = Depends(pagination_params),
        fields: Optional[StringListParam] = Query(default=None),
        sort: Optional[str] = Query(default=None),
        name: NameFilter = Depends(name_filter)) -> PaginatedList[SelectorResponse] | ItemsList[SelectorResponse]:

    qb = SelectorQueryBuilder(auth_data)
    qb.submodel(submodel)
    qb.permissions()
    qb.name_filter(name)
    qb.sort(sort)

    selectors = qb.find()
    return await paginated(selectors, transform=selectors_transform(fields, auth_data), params=pagination)


@selectors_ctrl.get("/{submodel}/{selector_id}", tags=["selectors"], name="Get a selector", response_model_exclude_unset=True)
async def show(submodel: str,
               selector_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> SelectorResponse:
    selector = await Selector.get(selector_id, NotFound(f"{submodel} not found"), submodel=submodel)
    check_read(auth_data, selector, NotFound(f"{submodel} not found"))
    return await selectors_transform(fields, auth_data)(selector)


@selectors_ctrl.post("/{submodel}/", tags=["selectors"], name="Create a selector", status_code=201)
async def create(body: SelectorRequest,
                 submodel: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[SelectorResponse]:
    perm = auth_data.permissions
    if not perm.selectors.all.create:
        raise Forbidden(f"no permissions to create a new {submodel}")

    ctor = Selector.__submodel_loaders__.get(submodel)
    if ctor is None:
        raise InputDataError(f"invalid selector type {submodel}")

    selector = ctor.from_pydantic(body)
    await selector.save()

    return StatusResponse(
        status=f"{submodel} \"{selector.name}\" created",
        data=await selectors_transform(fields, auth_data)(selector)
    )


@selectors_ctrl.patch("/{submodel}/{selector_id}", tags=["selectors"], name="Update a selector")
async def update(selector_id: str,
                 submodel: str,
                 body: SelectorRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[SelectorResponse]:
    selector = await Selector.get(selector_id, NotFound(f"{submodel} not found"), submodel=submodel)
    check_read_and_update(
        auth_data,
        selector,
        NotFound(f"{submodel} not found"),
        Forbidden(f"no permissions to modify this {submodel}")
    )

    await selector.update_from_pydantic(body)
    return StatusResponse(
        status=f"{submodel} \"{selector.name}\" updated",
        data=await selectors_transform(fields, auth_data)(selector)
    )


@selectors_ctrl.delete("/{submodel}/{selector_id}", tags=["selectors"], name="Delete a selector")
async def delete(selector_id: str,
                 submodel: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[SelectorResponse]:
    selector = await Selector.get(selector_id, NotFound(f"{submodel} not found"), submodel=submodel)
    check_read_and_destroy(
        auth_data,
        selector,
        NotFound(f"{submodel} not found"),
        Forbidden(f"no permissions to delete this {submodel}")
    )

    await selector.destroy()
    return StatusResponse(
        status=f"{submodel} \"{selector.name}\" destroyed",
        data=await selectors_transform(fields, auth_data)(selector)
    )
