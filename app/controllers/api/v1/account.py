from typing import Literal, Optional
from fastapi import APIRouter, Depends, Body
from fastapi.responses import RedirectResponse
from pydantic import BaseModel
from croydon import ctx
from croydon.errors import BadRequest, Forbidden, InputDataError

from app.extractors import current_session, AuthData, user_and_permissions
from app.models import User, Session, Role
from app.models.api import StatusResponse
from app.models.api.user import AccountMeResponse, AccountMeUpdateRequest, SetPasswordRequest, SettingsRequest
from app.errors import AuthenticationError
from app.lib.oauth.google import fetch_token, load_userinfo, GoogleOAuthError


class LogoutResponse(BaseModel):
    detail: Literal["logged out"] = "logged out"


class AuthenticationRequest(BaseModel):
    username: str
    password: str


account_ctrl = APIRouter(prefix="/api/v1/account")


@account_ctrl.get("/me", tags=["account"], name="Check current user authentication data")
async def me(auth_data: AuthData = Depends(user_and_permissions)) -> AccountMeResponse:
    """
    Get user info of a currently authenticated user

    Mostly used as a startup call for the UI SPA to figure out if the browser user
    is authenticated and either fill in the user's data like username and avatar in header
    or redirect the user to an authentication page
    """
    dct = await auth_data.user.to_dict_ext(fields=AccountMeResponse.__fields__.keys(), include_restricted=True)
    dct["current_permissions"] = auth_data.permissions
    return AccountMeResponse(**dct)


@account_ctrl.get("/oauth", tags=["account"], name="OAuth2 callback handler")
async def oauth(code: str,
                session: Session = Depends(current_session),
                error: Optional[str] = None) -> RedirectResponse:
    if error:
        raise BadRequest(f"error authenticating with OAuth provider: {error}")

    try:
        token = await fetch_token(code)
    except GoogleOAuthError as e:
        raise BadRequest(e.response["error"])

    data = await load_userinfo(token)
    user = await User.find_one({"email": data["email"]})
    if user is None:
        role = await Role.default()
        username = data["email"].split("@")[0]
        user = User.create(
            username=username,
            email=data["email"],
            first_name=data.get("given_name"),
            last_name=data.get("family_name"),
            avatar_url=data.get("picture"),
            role_id=role.id
        )
        await user.save()
    session.user_id = user.id
    return RedirectResponse("/")


@account_ctrl.patch("/me", tags=["account"], name="Update current user profile data")
async def update(body: AccountMeUpdateRequest,
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[AccountMeResponse]:
    """
    Update user data
    """
    if not auth_data.permissions.users.myself.update:
        raise Forbidden("no permissions to update profile fields")

    await auth_data.user.update_from_pydantic(body)

    dct = await auth_data.user.to_dict_ext(fields=AccountMeResponse.__fields__.keys(), include_restricted=True)
    dct["current_permissions"] = auth_data.permissions
    return StatusResponse(
        status="profile updated",
        data=AccountMeResponse(**dct)
    )


@account_ctrl.patch("/settings", tags=["account"], name="Update current user settings")
async def settings(body: SettingsRequest,
                   auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[AccountMeResponse]:
    """
    Update user data
    """
    if not auth_data.permissions.users.myself.update:
        raise Forbidden("no permissions to update profile fields")

    auth_data.user.settings = body.settings.dict()
    await auth_data.user.save()

    dct = await auth_data.user.to_dict_ext(fields=AccountMeResponse.__fields__.keys(), include_restricted=True)
    dct["current_permissions"] = auth_data.permissions
    return StatusResponse(
        status="user settings updated",
        data=AccountMeResponse(**dct)
    )


@account_ctrl.post("/password", tags=["account"], name="Set password for current user")
async def set_password(body: SetPasswordRequest,
                       auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[AccountMeResponse]:

    if not auth_data.permissions.users.myself.update:
        raise Forbidden("no permissions to set current user's password")

    if not body.password:
        raise InputDataError("empty password")
    if body.password != body.password_confirm:
        raise InputDataError("passwords don't match")

    auth_data.user.set_password(body.password)
    await auth_data.user.save()

    dct = await auth_data.user.to_dict_ext(fields=AccountMeResponse.__fields__.keys(), include_restricted=True)
    dct["current_permissions"] = auth_data.permissions
    return StatusResponse(
        status="password set",
        data=AccountMeResponse(**dct)
    )


@account_ctrl.post("/authenticate", tags=["account"])
async def authenticate(
        auth_request: AuthenticationRequest = Body(),
        session: Session = Depends(current_session)) -> AccountMeResponse:
    if not ctx.cfg.april.enable_passwords:
        raise AuthenticationError()

    user = await session.user()
    if user:
        raise BadRequest("already authenticated")

    user = await User.get(auth_request.username)
    if user is None or not user.check_password(auth_request.password):
        raise AuthenticationError("invalid credentials")
    session.user_id = user.id

    dct = await user.to_dict_ext(fields=AccountMeResponse.__fields__.keys(), include_restricted=True)
    role = await user.role()
    dct["current_permissions"] = role.permissions()
    return AccountMeResponse(**dct)


@account_ctrl.post("/logout", tags=["account"], name="Reset session-based auth data and logout")
async def logout(session: Session = Depends(current_session)) -> LogoutResponse:
    session.user_id = None
    return LogoutResponse(detail="logged out")
