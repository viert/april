import asyncio

from bson import ObjectId
from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon.errors import NotFound, Forbidden

from app.models import Group, Host, Rack, State, OsImage
from app.models.rack import InsufficientUnitSpace
from app.models.api.host import (
    HostResponse,
    HostUpdateRequest,
    HostCreateRequest,
    HostMoveGroupRequest,
    HostMoveGroupResponse,
    HostMoveRackRequest,
    HostDeleteManyRequest,
    HostTagsRequest,
    HostMassTagsRequest,
    HostMassAliasesRequest,
    HostStateRequest,
    HostMassStateRequest,
    HostMassOsImageRequest,
)
from app.models.api.group import GroupBaseResponse
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    ObjectIdListParam,
    ObjectIdParam,
    StringListParam,
    user_and_permissions,
    pagination_params,
    name_filter,
    resolve_group_ids,
    resolve_location_ids
)
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.pagination import PaginatedList, ItemsList, paginated, PaginationParams
from app.lib.pattern import expand_with_matches, apply_matches
from app.lib.glpi.types import Inventory
from app.lib.glpi import glpi_agent_sync
from app.lib.query import HostQueryBuilder


hosts_ctrl = APIRouter(prefix="/api/v1/hosts")


def hosts_transform(fields: Optional[List[str]], auth_data: AuthData) -> Callable[[Host], Awaitable[HostResponse]]:
    need_rel = fields and "rel" in fields

    async def transform(h: Host) -> HostResponse:
        dct = await h.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = h.relation(auth_data.user).value
        return HostResponse(**dct)
    return transform


@hosts_ctrl.get("/", tags=["hosts"], name="Hosts index and search", response_model_exclude_unset=True)
async def index(auth_data: AuthData = Depends(user_and_permissions),
                pagination: Optional[PaginationParams] = Depends(pagination_params),
                mine_only: bool = Query(False),
                fields: Optional[StringListParam] = Query(default=None),
                name: NameFilter = Depends(name_filter),
                ids: Optional[ObjectIdListParam] = Query(default=None),
                location_ids: Optional[List[ObjectId | None]] = Depends(resolve_location_ids),
                state_id: Optional[ObjectIdParam] = Query(default=None),
                team_id: Optional[ObjectIdParam] = Query(default=None),
                group_ids: Optional[List[ObjectId | None]] = Depends(resolve_group_ids),
                local_tags: Optional[StringListParam] = Query(default=None),
                tags: Optional[StringListParam] = Query(default=None),
                ip: Optional[str] = Query(default=None),
                mac: Optional[str] = Query(default=None),
                sort: Optional[str] = Query(default=None),
                vm_type: Optional[str] = Query(default=None),
                vm_host_id: Optional[ObjectIdParam] = Query(default=None),
                ) -> PaginatedList[HostResponse] | ItemsList[HostResponse]:

    qb = HostQueryBuilder(auth_data)
    qb.permissions(mine_only=mine_only)
    qb.name_filter(name)
    qb.ids(ids)
    qb.local_tags(local_tags)
    qb.groups(group_ids)
    qb.state(state_id)
    qb.locations(location_ids)
    qb.ip(ip)
    qb.mac(mac)
    qb.vm_type(vm_type)
    qb.vm_host(vm_host_id)
    await qb.team(team_id)
    await qb.tags(tags)
    qb.sort(sort)

    hosts = qb.find()
    return await paginated(hosts, transform=hosts_transform(fields, auth_data), params=pagination)


@hosts_ctrl.get("/{host_id}", tags=["hosts"], name="Get a host", response_model_exclude_unset=True)
async def show(host_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> HostResponse:
    host = await Host.get(host_id, NotFound("host not found"))
    check_read(auth_data, host, NotFound("host not found"))
    return await hosts_transform(fields, auth_data)(host)


@hosts_ctrl.patch("/{host_id}", tags=["hosts"], name="Update a host")
async def update(host_id: str,
                 body: HostUpdateRequest,
                 auth_data: AuthData = Depends(user_and_permissions),
                 fields: Optional[StringListParam] = Query(default=None)) -> StatusResponse[HostResponse]:
    host = await Host.get(host_id, NotFound("host not found"))
    check_read_and_update(auth_data, host, NotFound("host not found"),
                          Forbidden("no permissions to modify this host"))

    if body.group_id:
        group = await Group.get(body.group_id)
        check_read_and_update(auth_data, group, NotFound("group not found"),
                              Forbidden(f"no permissions to move this host to group {group.name}"))

    if body.rack_id is not None:
        rack = await Rack.get(body.rack_id, NotFound("rack not found"))
        if rack.id != host.rack_id and not body.rack_position:
            positions = await rack.available_units(count=1)
            body.rack_position = positions.pop()

    if body.os_image_id:
        os_image = await OsImage.get(body.os_image_id, NotFound("os image not found"))
        check_read(auth_data, os_image, NotFound("os image not found"))

    if body.state_id:
        state = await State.get(body.state_id, NotFound("state not found"))
        check_read(auth_data, state, NotFound("state not found"))

    await host.update_from_pydantic(body)

    return StatusResponse(
        status=f"host {host.fqdn} updated",
        data=await hosts_transform(fields, auth_data)(host)
    )


@hosts_ctrl.post("/{host_id}/state", tags=["hosts"], name="Set host's state")
async def set_state(host_id: str,
                    body: HostStateRequest,
                    fields: Optional[StringListParam] = Query(default=None),
                    auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    host = await Host.get(host_id, NotFound("host not found"))
    rel = host.relation(auth_data.user)

    state = None
    if body.state_id is not None:
        state = await State.get(body.state_id, NotFound("state not found"))
        check_read(auth_data, state, NotFound("state not found"))

    check_read(auth_data, host, NotFound("host not found"))

    if not auth_data.permissions.hosts.cani("set_state").get(rel.value):
        raise Forbidden("no permissions to set this host's state")

    if state:
        host.state_id = state.id
        status = f"host {host.fqdn} state set to \"{state.name}\""
    else:
        host.state_id = None
        status = f"host {host.fqdn} state reset"

    await host.save()

    return StatusResponse(
        status=status,
        data=await hosts_transform(fields, auth_data)(host)
    )


@hosts_ctrl.post("/{host_id}/add_tags", tags=["hosts"], name="Add tags to host")
async def add_tags(host_id: str,
                   body: HostTagsRequest,
                   fields: Optional[StringListParam] = Query(default=None),
                   auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    host = await Host.get(host_id, NotFound("host not found"))
    check_read_and_update(auth_data, host, NotFound("host not found"),
                          Forbidden("no permissions to modify this host"))
    for tag in body.tags:
        host.add_local_tag(tag)

    await host.save()

    return StatusResponse(
        status=f"host {host.fqdn} tags set",
        data=await hosts_transform(fields, auth_data)(host)
    )


@hosts_ctrl.post("/{host_id}/remove_tags", tags=["hosts"], name="Remove tags from host")
async def remove_tags(host_id: str,
                      body: HostTagsRequest,
                      fields: Optional[StringListParam] = Query(default=None),
                      auth_data: AuthData = Depends(user_and_permissions)
                      ) -> StatusResponse[HostResponse]:
    host = await Host.get(host_id, NotFound("host not found"))
    check_read_and_update(auth_data, host, NotFound("host not found"),
                          Forbidden("no permissions to modify this host"))
    for tag in body.tags:
        host.remove_local_tag(tag)

    await host.save()

    return StatusResponse(
        status=f"host {host.fqdn} tags removed",
        data=await hosts_transform(fields, auth_data)(host)
    )


@hosts_ctrl.delete("/{host_id}", tags=["hosts"], name="Delete a host", response_model_exclude_unset=True)
async def delete(host_id: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    host = await Host.get(host_id, NotFound("host not found"))
    check_read_and_destroy(auth_data, host, NotFound("host not found"),
                           Forbidden("no permissions to delete this host"))

    await host.destroy()

    return StatusResponse(
        status=f"host {host.fqdn} deleted",
        data=await hosts_transform(fields, auth_data)(host)
    )


@hosts_ctrl.post("/", tags=["hosts"], name="Create host(s)", status_code=201)
async def create(body: HostCreateRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:

    if not auth_data.permissions.hosts.unowned.create:
        raise Forbidden("you don't have permissions to create hosts")

    if body.group_id:
        group = await Group.get(body.group_id, NotFound("group not found"))
        check_read_and_update(auth_data, group, NotFound("group not found"),
                              Forbidden("no permissions to add new hosts to this group"))

    if body.multiple:
        fqdns_data = list(expand_with_matches(body.fqdn))
        aliases_map = {}
        fqdns = []
        for fqdn, matches in fqdns_data:
            fqdns.append(fqdn)
            aliases_map[fqdn] = [apply_matches(alias, matches) for alias in body.aliases]
    else:
        fqdns = [body.fqdn]
        aliases_map = {
            body.fqdn: body.aliases
        }

    tasks = []
    for fqdn in fqdns:
        aliases = aliases_map.get(fqdn, [])
        host = Host.create(
            fqdn=fqdn,
            aliases=aliases,
            description=body.description,
            group_id=body.group_id,
            rack_id=body.rack_id,
            rack_position=body.rack_position,
            local_tags=body.local_tags,
            local_attrs=body.local_attrs
        )
        tasks.append(host.save())

    hosts = await asyncio.gather(*tasks)

    return StatusResponse(
        status="host(s) created",
        data=await asyncio.gather(*[hosts_transform(fields, auth_data)(host) for host in hosts])
    )


@hosts_ctrl.post("/group", tags=["hosts"], name="Move multiple hosts to a given group")
async def move_group(body: HostMoveGroupRequest,
                     fields: Optional[StringListParam] = Query(default=None),
                     auth_data: AuthData = Depends(user_and_permissions)) -> HostMoveGroupResponse:
    user = auth_data.user
    perm = auth_data.permissions

    group = None
    if body.group_id:
        group = await Group.get(body.group_id, NotFound("group not found"))
        check_read_and_update(auth_data, group, NotFound("group not found"),
                              Forbidden("no permissions to move hosts to this group"))
        group_id = group.id
    else:
        group_id = None

    read_check = perm.hosts.cani("read")
    update_check = perm.hosts.cani("update")
    hosts = await Host.find({"_id": {"$in": body.host_ids}}).all()
    hosts = filter(lambda h: h.group_id != group_id, hosts)
    hosts = filter(lambda h: read_check[h.relation(user).value], hosts)
    hosts = list(hosts)

    if not hosts:
        raise NotFound("no hosts found")

    forbidden = []
    for host in hosts:
        if not update_check[host.relation(user).value]:
            forbidden.append(host.fqdn)

    if forbidden:
        raise Forbidden(f"no permissions to move hosts {', '.join(forbidden)}")

    for host in hosts:
        host.group_id = group_id
        await host.save()

    status = (f"{len(hosts)} hosts moved to group {group.name}" if group
              else f"{len(hosts)} hosts detached from their groups")

    return HostMoveGroupResponse(
        status=status,
        group=GroupBaseResponse(**group.to_dict()) if group else None,
        hosts=await asyncio.gather(*[hosts_transform(fields, auth_data)(host) for host in hosts])
    )


@hosts_ctrl.post("/rack", tags=["hosts"], name="Move multiple hosts to a given rack")
async def move_rack(body: HostMoveRackRequest,
                    fields: Optional[StringListParam] = Query(default=None),
                    auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    hosts_to_detach = []
    hosts_to_move_manually = []
    hosts_to_move_automatically = []

    for item in body.host_rack_positions:
        if item.rack_id is None:
            hosts_to_detach.append(item)
        elif item.rack_position > 0:
            hosts_to_move_manually.append(item)
        else:
            hosts_to_move_automatically.append(item)

    rack_ids = list(set([item.rack_id for item in body.host_rack_positions]))
    host_ids = list(set([item.host_id for item in body.host_rack_positions]))

    rack_map = await Rack.find({"_id": {"$in": rack_ids}}).as_map()
    host_map = await Host.find({"_id": {"$in": host_ids}}).as_map()

    for rack_id in rack_ids:
        if rack_id:
            if rack_id not in rack_map:
                raise NotFound(f"rack {rack_id} not found")
            rack = rack_map[rack_id]
            check_read(auth_data, rack, NotFound(f"rack {rack_id} not found"))
    for host_id in host_ids:
        if host_id not in host_map:
            raise NotFound(f"host {host_id} not found")
        host = host_map[host_id]
        check_read_and_update(auth_data, host, NotFound(f"host {host_id} not found"),
                              Forbidden(f"no permissions to update host {host.fqdn}"))

    tasks = []

    for item in hosts_to_detach:
        host = host_map[item.host_id]
        host.rack_id = None
        host.rack_position = None
        tasks.append(host.save())

    for item in hosts_to_move_manually:
        host = host_map[item.host_id]
        host.rack_id = item.rack_id
        host.rack_position = item.rack_position
        tasks.append(host.save())

    rack_alloc = {}

    for item in hosts_to_move_automatically:
        host = host_map[item.host_id]
        rack = rack_map[item.rack_id]
        host.rack_id = item.rack_id
        if item.rack_id not in rack_alloc:
            rack_alloc[item.rack_id] = await rack.available_units()
        try:
            host.rack_position = rack_alloc[item.rack_id].pop()
        except IndexError:
            raise InsufficientUnitSpace(f"rack {rack.full_name} does not have enough space")
        tasks.append(host.save())

    await asyncio.gather(*tasks)

    status = []
    if hosts_to_detach:
        status.append(f"{len(hosts_to_detach)} hosts detached from their racks")
    if hosts_to_move_manually:
        status.append(f"{len(hosts_to_move_manually)} hosts moved to designated rack units")
    if hosts_to_move_automatically:
        status.append(f"{len(hosts_to_move_automatically)} hosts moved to random units within rack")

    status = ", ".join(status)
    if not status:
        status = "no changes were made"

    hosts = list(host_map.values())
    transform = hosts_transform(fields, auth_data)
    data = await asyncio.gather(*[transform(host) for host in hosts])

    return StatusResponse(
        status=status,
        data=data
    )


@hosts_ctrl.delete("/", tags=["hosts"], name="Delete multiple hosts")
async def delete_many(body: HostDeleteManyRequest,
                      fields: Optional[StringListParam] = Query(default=None),
                      auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    user = auth_data.user
    perm = auth_data.permissions

    read_check = perm.hosts.cani("read")
    destroy_check = perm.hosts.cani("destroy")
    hosts = await Host.find({"_id": {"$in": body.host_ids}}).all()
    hosts = filter(lambda h: read_check[h.relation(user).value], hosts)
    hosts = list(hosts)

    if not hosts:
        raise NotFound("no hosts found")

    forbidden = []
    for host in hosts:
        if not destroy_check[host.relation(user).value]:
            forbidden.append(host.fqdn)

    if forbidden:
        raise Forbidden(f"no permissions to delete hosts {', '.join(forbidden)}")

    for host in hosts:
        await host.destroy()

    transform = hosts_transform(fields, auth_data)

    return StatusResponse(
        status=f"{len(hosts)} hosts deleted",
        data=await asyncio.gather(*[transform(host) for host in hosts])
    )


@hosts_ctrl.post("/tags", tags=["hosts"], name="Add or overwrite local tags on multiple hosts")
async def set_tags(body: HostMassTagsRequest,
                   fields: Optional[StringListParam] = Query(default=None),
                   auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    user = auth_data.user
    perm = auth_data.permissions

    read_check = perm.hosts.cani("read")
    update_check = perm.hosts.cani("update")
    hosts = await Host.find({"_id": {"$in": body.host_ids}}).all()
    hosts = filter(lambda h: read_check[h.relation(user).value], hosts)
    hosts = list(hosts)

    if not hosts:
        raise NotFound("no hosts found")

    forbidden = []
    for host in hosts:
        if not update_check[host.relation(user).value]:
            forbidden.append(host.fqdn)

    if forbidden:
        raise Forbidden(f"no permissions to update hosts {', '.join(forbidden)}")

    for host in hosts:
        if body.overwrite:
            host.local_tags = body.tags
        else:
            host.local_tags.extend(body.tags)
        await host.save()

    transform = hosts_transform(fields, auth_data)

    return StatusResponse(
        status=f"{len(hosts)} hosts updated",
        data=await asyncio.gather(*[transform(host) for host in hosts])
    )


@hosts_ctrl.post("/aliases", tags=["hosts"], name="Add or overwrite aliases on multiple hosts")
async def set_aliases(body: HostMassAliasesRequest,
                      fields: Optional[StringListParam] = Query(default=None),
                      auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    user = auth_data.user
    perm = auth_data.permissions

    read_check = perm.hosts.cani("read")
    update_check = perm.hosts.cani("update")
    hosts = await Host.find({"_id": {"$in": body.host_ids}}).all()
    hosts = filter(lambda h: read_check[h.relation(user).value], hosts)
    hosts = list(hosts)

    if not hosts:
        raise NotFound("no hosts found")

    forbidden = []
    for host in hosts:
        if not update_check[host.relation(user).value]:
            forbidden.append(host.fqdn)

    if forbidden:
        raise Forbidden(f"no permissions to update hosts {', '.join(forbidden)}")

    for host in hosts:
        aliases = [host.render_alias(pattern) for pattern in body.aliases]
        if body.overwrite:
            host.aliases = aliases
        else:
            host.aliases.extend(aliases)
        await host.save()

    transform = hosts_transform(fields, auth_data)
    return StatusResponse(
        status=f"{len(hosts)} hosts updated",
        data=await asyncio.gather(*[transform(host) for host in hosts])
    )


@hosts_ctrl.post("/state", tags=["hosts"], name="Set state of multiple hosts")
async def mass_set_state(body: HostMassStateRequest,
                         fields: Optional[StringListParam] = Query(default=None),
                         auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    user = auth_data.user
    perm = auth_data.permissions

    state = None
    if body.state_id is not None:
        state = await State.get(body.state_id, NotFound("state not found"))
        check_read(auth_data, state, NotFound("state not found"))

    read_check = perm.hosts.cani("read")
    update_check = perm.hosts.cani("set_state")
    hosts = await Host.find({"_id": {"$in": body.host_ids}}).all()
    hosts = filter(lambda h: read_check[h.relation(user).value], hosts)
    hosts = list(hosts)

    if not hosts:
        raise NotFound("no hosts found")

    forbidden = []
    for host in hosts:
        if not update_check[host.relation(user).value]:
            forbidden.append(host.fqdn)

    if forbidden:
        raise Forbidden(f"no permissions to update hosts {', '.join(forbidden)}")

    if state:
        for host in hosts:
            host.state_id = body.state_id
            await host.save()
        status = f"{len(hosts)} hosts changed state to \"{state.name}\""
    else:
        for host in hosts:
            host.state_id = None
            await host.save()
        status = f"{len(hosts)} hosts reset state"

    transform = hosts_transform(fields, auth_data)
    return StatusResponse(
        status=status,
        data=await asyncio.gather(*[transform(host) for host in hosts])
    )


@hosts_ctrl.post("/os_image", tags=["hosts"], name="Set os image of multiple hosts")
async def mass_set_os_image(body: HostMassOsImageRequest,
                            fields: Optional[StringListParam] = Query(default=None),
                            auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[HostResponse]:
    user = auth_data.user
    perm = auth_data.permissions

    os_image = None
    if body.os_image_id is not None:
        os_image = await OsImage.get(body.os_image_id, NotFound("state not found"))
        check_read(auth_data, os_image, NotFound("state not found"))

    read_check = perm.hosts.cani("read")
    update_check = perm.hosts.cani("update")
    hosts = await Host.find({"_id": {"$in": body.host_ids}}).all()
    hosts = filter(lambda h: read_check[h.relation(user).value], hosts)
    hosts = list(hosts)

    if not hosts:
        raise NotFound("no hosts found")

    forbidden = []
    for host in hosts:
        if not update_check[host.relation(user).value]:
            forbidden.append(host.fqdn)

    if forbidden:
        raise Forbidden(f"no permissions to update hosts {', '.join(forbidden)}")

    if os_image:
        for host in hosts:
            host.os_image_id = os_image.id
            await host.save()
        status = f"{len(hosts)} hosts changed os image to \"{os_image.name}\""
    else:
        for host in hosts:
            host.state_id = None
            await host.save()
        status = f"{len(hosts)} hosts reset os image"

    transform = hosts_transform(fields, auth_data)
    return StatusResponse(
        status=status,
        data=await asyncio.gather(*[transform(host) for host in hosts])
    )


@hosts_ctrl.post("/glpi", tags=["hosts"], name="Import a host from GLPI agent inventory")
async def glpi_import(body: Inventory,
                      fields: Optional[StringListParam] = Query(default=None),
                      auth_data: AuthData = Depends(user_and_permissions),) -> StatusResponse[HostResponse]:

    perm = auth_data.permissions
    read_permissions = perm.hosts.cani("read").values()
    update_permissions = perm.hosts.cani("update").values()
    create_permissions = perm.hosts.unowned.create

    components_permissions = (perm.components.all.create and perm.components.all.update and
                              perm.components.all.read and perm.components.all.destroy)

    if not all([*read_permissions, *update_permissions, create_permissions, components_permissions]):
        raise Forbidden("insufficient permissions to import hosts and/or components")

    host = await glpi_agent_sync(body)

    if host is None:
        return StatusResponse(
            status=f"host is ignored being a vm",
            data=None
        )

    return StatusResponse(
        status=f"host {host.fqdn} imported",
        data=await hosts_transform(fields, auth_data)(host)
    )
