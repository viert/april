from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon.errors import NotFound, Forbidden

from app.models import DBInstance, Host, Team, VirtualMachine
from app.models.api.db_instance import DBInstanceResponse, DBInstanceRequest
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    ObjectIdListParam,
    ObjectIdParam,
    StringListParam,
    user_and_permissions,
    pagination_params,
    name_filter,
)
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.query import DBInstanceQueryBuilder


def db_instances_transform(
        fields: Optional[List[str]], auth_data: AuthData) -> Callable[[DBInstance], Awaitable[DBInstanceResponse]]:
    need_rel = fields and "rel" in fields

    async def transform(r: DBInstance) -> DBInstanceResponse:
        dct = await r.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = r.relation(auth_data.user).value
        return DBInstanceResponse(**dct)
    return transform


db_instances_ctrl = APIRouter(prefix="/api/v1/db_instances")


@db_instances_ctrl.get("/", tags=["db_instances"], name="DBInstances index and search",
                       response_model_exclude_unset=True)
async def index(auth_data: AuthData = Depends(user_and_permissions),
                pagination: Optional[PaginationParams] = Depends(pagination_params),
                fields: Optional[StringListParam] = Query(default=None),
                name: NameFilter = Depends(name_filter),
                host_id: Optional[ObjectIdParam] = Query(default=None),
                team_id: Optional[ObjectIdParam] = Query(default=None),
                ids: Optional[ObjectIdListParam] = Query(default=None),
                port: Optional[int] = Query(default=None),
                mine_only: bool = Query(default=False),
                sort: Optional[str] = Query(default=None),
                ) -> PaginatedList[DBInstanceResponse] | ItemsList[DBInstanceResponse]:

    qb = DBInstanceQueryBuilder(auth_data)
    qb.permissions(mine_only=mine_only)
    qb.name_filter(name)
    qb.ids(ids)
    qb.port(port)

    qb.team(team_id)
    qb.host(host_id)
    qb.sort(sort)

    db_instances = qb.find()
    return await paginated(
        db_instances,
        transform=db_instances_transform(fields, auth_data),
        params=pagination
    )


@db_instances_ctrl.get("/{db_instance_id}", tags=["db_instances"], name="Get a db_instance",
                       response_model_exclude_unset=True)
async def show(db_instance_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> DBInstanceResponse:
    db_instance = await DBInstance.get(db_instance_id, NotFound("db_instance not found"))
    check_read(auth_data, db_instance, NotFound("db_instance not found"))
    return await db_instances_transform(fields, auth_data)(db_instance)


@db_instances_ctrl.post("/", tags=["db_instances"], name="Create a db_instance", status_code=201)
async def create(body: DBInstanceRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[DBInstanceResponse]:
    perm = auth_data.permissions
    if not perm.db_instances.unowned.create:
        raise Forbidden("no permissions to create new db_instances")

    host = await Host.get(body.host_id, NotFound("host not found"))
    check_read(auth_data, host, NotFound("host not found"))

    team = await Team.get(body.team_id, NotFound("team not found"))
    check_read(auth_data, team, NotFound("team not found"))

    db_instance = DBInstance.from_pydantic(body)
    await db_instance.save()

    return StatusResponse(
        status="db instance created",
        data=await db_instances_transform(fields, auth_data)(db_instance)
    )


@db_instances_ctrl.patch("/{db_instance_id}", tags=["db_instances"], name="Update a db_instance")
async def update(db_instance_id: str,
                 body: DBInstanceRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[DBInstanceResponse]:
    db_instance = await DBInstance.get(db_instance_id, NotFound("db_instance not found"))
    check_read_and_update(auth_data, db_instance, NotFound("db_instance not found"),
                          Forbidden("no permissions to update this db_instance"))

    payload = body.dict(exclude_unset=True)
    if body.team_id:
        team = await Team.get(body.team_id, NotFound("team not found"))
        if team.id == db_instance.team_id:
            del payload["team_id"]
    if body.host_id:
        host = await Host.get(body.host_id, NotFound("host not found"))
        if host.id == db_instance.host_id:
            del payload["host_id"]
    if body.vm_id:
        vm = await VirtualMachine.get(body.vm_id, NotFound("virtual machine not found"))
        if vm.id == db_instance.vm_id:
            del payload["vm_id"]

    await db_instance.update(payload)
    return StatusResponse(
        status="db instance updated",
        data=await db_instances_transform(fields, auth_data)(db_instance)
    )


@db_instances_ctrl.delete("/{db_instance_id}", tags=["db_instances"], name="Delete a db_instance")
async def delete(db_instance_id: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[DBInstanceResponse]:
    db_instance = await DBInstance.get(db_instance_id, NotFound("db_instance not found"))
    check_read_and_destroy(auth_data, db_instance, NotFound("db_instance not found"),
                           Forbidden("no permissions to update this db_instance"))
    await db_instance.destroy()
    return StatusResponse(
        status="db instance deleted",
        data=await db_instances_transform(fields, auth_data)(db_instance)
    )
