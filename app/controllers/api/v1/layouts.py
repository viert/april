from bson import ObjectId
from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon import ctx
from croydon.util import resolve_id
from croydon.errors import NotFound, Forbidden

from app.models import Team
from app.models.links import Link, Layout
from app.models.api import StatusResponse
from app.models.api.layout import LayoutCreateRequest, LayoutUpdateRequest, LayoutSetLinksRequest
from app.models.api.layout import LayoutResponse
from app.extractors import (
    AuthData,
    NameFilter,
    StringListParam,
    user_and_permissions,
    pagination_params,
    name_filter
)
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.query import LayoutQueryBuilder

layouts_ctrl = APIRouter(prefix="/api/v1/layouts")


def layout_transform(fields: Optional[List[str]],
                     auth_data: AuthData) -> Callable[[Layout], Awaitable[LayoutResponse]]:
    need_rel = fields and "rel" in fields

    async def transform(layout: Layout) -> LayoutResponse:
        dct = await layout.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = layout.relation(auth_data.user).value
        return LayoutResponse(**dct)

    return transform


@layouts_ctrl.get("/", tags=["layouts"], name="Layouts index and search", response_model_exclude_unset=True)
async def index(auth_data: AuthData = Depends(user_and_permissions),
                pagination: Optional[PaginationParams] = Depends(pagination_params),
                fields: Optional[StringListParam] = Query(default=None),
                mine_only: bool = False,
                sort: Optional[str] = Query(default=None),
                name: NameFilter = Depends(name_filter)
                ) -> PaginatedList[LayoutResponse] | ItemsList[LayoutResponse]:

    qb = LayoutQueryBuilder(auth_data)
    qb.permissions(mine_only=mine_only)
    qb.name_filter(name)
    qb.sort(sort)

    layouts = qb.find()
    return await paginated(layouts, transform=layout_transform(fields, auth_data), params=pagination)


@layouts_ctrl.get("/{layout_id}", tags=["layouts"], name="Get a layout", response_model_exclude_unset=True)
async def show(layout_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> LayoutResponse:
    layout = await Layout.get(layout_id, NotFound("layout not found"))
    check_read(auth_data, layout, NotFound("layout not found"))
    return await layout_transform(fields, auth_data)(layout)


@layouts_ctrl.post("/", tags=["layouts"], name="Create a layout", response_model_exclude_unset=True)
async def create(body: LayoutCreateRequest,
                 auth_data: AuthData = Depends(user_and_permissions),
                 fields: Optional[StringListParam] = Query(default=None)) -> StatusResponse[LayoutResponse]:
    if not auth_data.permissions.layouts.others.create:
        raise Forbidden("no permissions to create new layouts")

    team = await Team.get(body.team_id, NotFound("team not found"))
    check_read_and_update(auth_data,
                          team,
                          NotFound("team not found"),
                          Forbidden("no permissions to create layouts in this team"))

    layout = Layout.from_pydantic(body)
    await layout.save()
    return StatusResponse(
        status="layout created",
        data=await layout_transform(fields, auth_data)(layout)
    )


@layouts_ctrl.patch("/{layout_id}", tags=["layouts"], name="Update a layout", response_model_exclude_unset=True)
async def update(layout_id: str,
                 body: LayoutUpdateRequest,
                 auth_data: AuthData = Depends(user_and_permissions),
                 fields: Optional[StringListParam] = Query(default=None)) -> StatusResponse[LayoutResponse]:
    layout = await Layout.get(layout_id, NotFound("layout not found"))
    check_read_and_update(auth_data,
                          layout,
                          NotFound("layout not found"),
                          Forbidden("no permissions to update this layout"))

    if body.team_id:
        team = await Team.get(body.team_id, NotFound("team not found"))
        if team.id != layout.team_id:
            check_read_and_update(auth_data, team, NotFound("team not found"),
                                  Forbidden("no permissions to move layout to this team"))
    await layout.update_from_pydantic(body)

    return StatusResponse(
        status="layout updated",
        data=await layout_transform(fields, auth_data)(layout)
    )


@layouts_ctrl.delete("/{layout_id}", tags=["layouts"], name="Delete a layout", response_model_exclude_unset=True)
async def delete(layout_id: str,
                 auth_data: AuthData = Depends(user_and_permissions),
                 fields: Optional[StringListParam] = Query(default=None)) -> StatusResponse[LayoutResponse]:
    layout = await Layout.get(layout_id, NotFound("layout not found"))
    check_read_and_destroy(auth_data,
                           layout,
                           NotFound("layout not found"),
                           Forbidden("no permissions to delete this layout"))
    await layout.destroy()
    return StatusResponse(
        status="layout updated",
        data=await layout_transform(fields, auth_data)(layout)
    )


@layouts_ctrl.post("/{layout_id}/links", tags=["layouts"],
                   name="Create or update layout links", response_model_exclude_unset=True)
async def set_links(layout_id: str,
                    body: LayoutSetLinksRequest,
                    auth_data: AuthData = Depends(user_and_permissions),
                    fields: Optional[StringListParam] = Query(default=None)) -> StatusResponse[LayoutResponse]:

    layout = await Layout.get(layout_id, NotFound("layout not found"))
    check_read_and_update(auth_data,
                          layout,
                          NotFound("layout not found"),
                          Forbidden("no permissions to update this layout"))

    body.check_and_update_nodes()

    node_map = {}
    update_link_map = {}

    for node_id, node_def in body.nodes.items():
        node = await node_def.get_object(node_id, NotFound(f"{node_def.node_type} {node_id} not found"))
        check_read(auth_data, node, NotFound(f"{node_def.node_type} {node_id} not found"))
        node_map[node_id] = node

    links_to_update = []
    links_to_create = []

    for link in body.links:
        link_id = resolve_id(link.id)
        if isinstance(link_id, ObjectId):
            lnk = await Link.get(link_id, NotFound(f"link {link_id} not found"))
            if lnk.layout_id != layout.id:
                raise NotFound(f"link {link_id} not found")
            links_to_update.append(link)
            update_link_map[link.id] = lnk
        else:
            links_to_create.append(link)

    ctx.log.debug(f"{len(links_to_update)} links to update, {len(links_to_create)} links to create")
    link_ids_to_update = [resolve_id(link.id) for link in links_to_update]

    ctx.log.debug(f"destroying links not in {link_ids_to_update} if any")
    await Link.destroy_many({"layout_id": layout.id, "_id": {"$nin": link_ids_to_update}})

    ctx.log.debug(f"updating links {link_ids_to_update}")
    for link in links_to_update:
        await update_link_map[link.id].update_from_pydantic(link)

    ctx.log.debug(f"creating links")
    for link in links_to_create:
        link.id = None
        lnk = Link.from_pydantic(link)
        lnk.layout_id = layout.id
        await lnk.save()

    # Collect node positions from each node of body.nodes if the node position exists.
    # If it does not, check if it already exists in layout itself
    # If the resulting dict is empty, it assigns None instead
    node_positions = {}
    for k, v in body.nodes.items():
        if v.node_position:
            node_positions[k] = v.node_position
        elif layout.node_positions and k in layout.node_positions:
            node_positions[k] = layout.node_positions[k]

    layout.node_positions = node_positions

    await layout.save()

    if not fields:
        fields = ["links"]
    elif "links" not in fields:
        fields.append("links")

    return StatusResponse(
        status="links set",
        data=await layout_transform(fields, auth_data)(layout)
    )
