from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon.errors import NotFound, Forbidden

from app.models import Location
from app.models.api.location import LocationResponse, LocationRequest, LocationTreeResponse
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    ObjectIdListParam,
    ObjectIdParam,
    StringListParam,
    user_and_permissions,
    pagination_params,
    name_filter,
)
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.query import LocationQueryBuilder


def locations_transform(
        fields: Optional[List[str]], auth_data: AuthData) -> Callable[[Location], Awaitable[LocationResponse]]:
    need_rel = fields and "rel" in fields

    async def transform(loc: Location) -> LocationResponse:
        dct = await loc.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = loc.relation(auth_data.user).value
        return LocationResponse(**dct)
    return transform


locations_ctrl = APIRouter(prefix="/api/v1/locations")


@locations_ctrl.get("/", tags=["locations"], name="Locations index and search",
                    response_model_exclude_unset=True)
async def index(auth_data: AuthData = Depends(user_and_permissions),
                pagination: Optional[PaginationParams] = Depends(pagination_params),
                fields: Optional[StringListParam] = Query(default=None),
                name: NameFilter = Depends(name_filter),
                is_root: bool = False,
                parent_id: Optional[ObjectIdParam] = None,
                sort: Optional[str] = Query(default=None),
                ids: Optional[ObjectIdListParam] = Query(default=None)
                ) -> PaginatedList[LocationResponse] | ItemsList[LocationResponse]:

    qb = LocationQueryBuilder(auth_data)
    qb.name_filter(name)
    qb.ids(ids)
    if is_root:
        qb.root_only()
    await qb.parent(parent_id)
    qb.sort(sort)

    locations = qb.find()
    return await paginated(
        locations,
        transform=locations_transform(fields, auth_data),
        params=pagination
    )


@locations_ctrl.get("/tree", tags=["locations"], name="Recursively fetch a full location tree")
async def tree(auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> LocationTreeResponse:
    if not auth_data.permissions.locations.all.read:
        raise NotFound("locations not found")
    locations = await Location.full_tree(fields=fields)
    return LocationTreeResponse(
        data=locations
    )


@locations_ctrl.get("/{loc_id}", tags=["locations"], name="Locations index and search",
                    response_model_exclude_unset=True)
async def show(loc_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> LocationResponse:

    location = await Location.get(loc_id, NotFound("location not found"))
    check_read(auth_data, location, NotFound("location not found"))
    return await locations_transform(fields, auth_data)(location)


@locations_ctrl.post("/", tags=["locations"], name="Create a location", status_code=201)
async def create(body: LocationRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[LocationResponse]:
    perm = auth_data.permissions
    if not perm.locations.all.create:
        raise Forbidden("no permissions to create new locations")

    parent = None
    if body.parent_id:
        parent = await Location.get(body.parent_id, NotFound("parent not found"))
        body.parent_id = None

    location = Location.from_pydantic(body)
    await location.save()

    if parent:
        await parent.add_child(location)

    return StatusResponse(
        status="location created",
        data=await locations_transform(fields, auth_data)(location)
    )


@locations_ctrl.patch("/{loc_id}", tags=["locations"], name="Update a location")
async def update(loc_id: str,
                 body: LocationRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[LocationResponse]:
    location = await Location.get(loc_id, NotFound("location not found"))
    check_read_and_update(auth_data, location, NotFound("location not found"),
                          Forbidden("no permissions to update this location"))

    parent = None
    if body.parent_id:
        parent = await Location.get(body.parent_id, NotFound("parent not found"))
        body.parent_id = None
        if parent.id == location.parent_id:
            # the same parent, do not set
            parent = None

    await location.update_from_pydantic(body)

    if parent:
        await parent.add_child(location)

    return StatusResponse(
        status="location updated",
        data=await locations_transform(fields, auth_data)(location)
    )


@locations_ctrl.post("/{loc_id}/detach", tags=["locations"], name="Detach a location from its parent")
async def detach(loc_id: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[LocationResponse]:
    location = await Location.get(loc_id, NotFound("location not found"))
    check_read_and_update(auth_data, location, NotFound("location not found"),
                          Forbidden("no permissions to update this location"))

    await location.detach()
    return StatusResponse(
        status="location detached from its parent",
        data=await locations_transform(fields, auth_data)(location)
    )


@locations_ctrl.delete("/{loc_id}", tags=["locations"], name="Delete a location")
async def delete(loc_id: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[LocationResponse]:
    location = await Location.get(loc_id, NotFound("location not found"))
    check_read_and_destroy(auth_data, location, NotFound("location not found"),
                           Forbidden("no permissions to update this location"))
    await location.destroy()
    return StatusResponse(
        status="location deleted",
        data=await locations_transform(fields, auth_data)(location)
    )
