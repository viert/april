from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query
from pydantic import BaseModel

from croydon.errors import NotFound, Forbidden, InputDataError, Conflict

from app.models import User, Role, Token
from app.models.api.user import UserResponse, UserRequest, SetPasswordRequest
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    user_and_permissions,
    pagination_params,
    name_filter,
    ObjectIdListParam,
    StringListParam
)
from app.lib.rbac import Permissions
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.query import UserQueryBuilder


users_ctrl = APIRouter(prefix="/api/v1/users")


class CreateTokenRequest(BaseModel):
    permissions: Permissions
    description: Optional[str]
    auto_prolonged: Optional[bool]


class CreateTokenResponse(BaseModel):
    token: str
    status: str


def users_transform(fields: Optional[List[str]], auth_data: AuthData) -> Callable[[User], Awaitable[UserResponse]]:
    tokens_requested = fields is not None and "tokens" in fields
    need_rel = fields and "rel" in fields

    async def transform(u: User) -> UserResponse:
        rel = u.relation(auth_data.user)
        dct = await u.to_dict_ext(fields=fields)
        if tokens_requested and not auth_data.permissions.users.get(rel).read_tokens:
            dct["tokens"] = []
        if need_rel:
            dct["rel"] = rel.value
        return UserResponse(**dct)

    return transform


@users_ctrl.get("/", tags=["users"], name="Users index and search", response_model_exclude_unset=True)
async def index(
        auth_data: AuthData = Depends(user_and_permissions),
        pagination: Optional[PaginationParams] = Depends(pagination_params),
        ids: Optional[ObjectIdListParam] = Query(default=None),
        fields: Optional[StringListParam] = Query(default=None),
        sort: Optional[str] = Query(default=None),
        name: NameFilter = Depends(name_filter)) -> PaginatedList[UserResponse] | ItemsList[UserResponse]:

    qb = UserQueryBuilder(auth_data)
    qb.permissions()
    qb.name_filter(name)
    qb.ids(ids)
    qb.sort(sort)

    users = qb.find()
    return await paginated(users, transform=users_transform(fields, auth_data), params=pagination)


@users_ctrl.get("/{user_id}", tags=["users"], name="Get a user", response_model_exclude_unset=True)
async def show(user_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> UserResponse:
    user = await User.get(user_id, NotFound("user not found"))
    check_read(auth_data, user, NotFound("user not found"))
    return await users_transform(fields, auth_data)(user)


@users_ctrl.post("/", tags=["users"], name="Create a new user", status_code=201)
async def create(body: UserRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[UserResponse]:
    perm = auth_data.permissions
    if not perm.users.others.create:
        raise Forbidden("no permissions to create new users")

    passwd: Optional[str] = None
    if body.password:
        if body.password != body.password_confirm:
            raise InputDataError("passwords don't match")
        passwd = body.password
        body.password = None
        body.password_confirm = None

    role = await Role.get(body.role_id, NotFound("role not found"))
    if role.permissions().exceeds(perm) and not perm.users.others.assign_exceeding_roles:
        raise Forbidden("you can't create a user with permissions exceeding yours")

    body.role_id = role.id

    existing = await User.get(body.username)
    if existing:
        raise Conflict("user already exists")

    new_user = User.from_pydantic(body)

    if passwd:
        new_user.set_password(passwd)

    await new_user.save()

    return StatusResponse(
        status="user created",
        data=await users_transform(fields, auth_data)(new_user)
    )


@users_ctrl.patch("/{user_id}", tags=["users"], name="Update a user")
async def update(user_id: str,
                 body: UserRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[UserResponse]:
    user = await User.get(user_id, NotFound("user not found"))
    check_read_and_update(
        auth_data,
        user,
        NotFound("user not found"),
        Forbidden("no permissions to modify this user")
    )

    if body.role_id and body.role_id != str(user.role_id):
        new_role = await Role.get(body.role_id, NotFound("role not found"))
        perm = auth_data.permissions
        rel = user.relation(auth_data.user)

        can_assign_role = perm.users.get(rel).assign_exceeding_roles \
            if new_role.permissions().exceeds(perm) else perm.users.get(rel).assign_roles
        if not can_assign_role:
            raise Forbidden("you can't assign the new role to this user")

    # update does not set the password
    body.password = None
    body.password_confirm = None

    await user.update_from_pydantic(body)
    return StatusResponse(
        status="user updated",
        data=await users_transform(fields, auth_data)(user)
    )


@users_ctrl.post("/{user_id}/set_password", tags=["users"], name="Set user's password")
async def set_password(user_id: str,
                       body: SetPasswordRequest,
                       fields: Optional[StringListParam] = Query(default=None),
                       auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[UserResponse]:
    user = await User.get(user_id, NotFound("user not found"))
    check_read_and_update(
        auth_data,
        user,
        NotFound("user not found"),
        Forbidden("no permissions to modify this user")
    )

    if not body.password:
        raise InputDataError("empty password")
    if body.password != body.password_confirm:
        raise InputDataError("passwords don't match")

    user.set_password(body.password)
    await user.save()

    return StatusResponse(
        status="password successfully set",
        data=await users_transform(fields, auth_data)(user)
    )


@users_ctrl.delete("/{user_id}/tokens/{token}", tags=["users"], name="Revoke user's token")
async def revoke_token(user_id: str,
                       token: str,
                       fields: Optional[StringListParam] = Query(default=None),
                       auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[UserResponse]:
    user = await User.get(user_id, NotFound("user not found"))

    rel = user.relation(auth_data.user)
    perm = auth_data.permissions

    if not perm.users.get(rel).read:
        raise NotFound("user not found")
    if not perm.users.get(rel).revoke_tokens:
        raise Forbidden("no permissions to revoke this user's tokens")

    token = await Token.find_one({"token": token, "user_id": user.id})
    if token is None:
        raise NotFound("token not found")

    await token.destroy()

    return StatusResponse(
        status="token revoked",
        data=await users_transform(fields, auth_data)(user)
    )


@users_ctrl.delete("/{user_id}/tokens", tags=["users"], name="Revoke all user's tokens")
async def revoke_all_tokens(user_id: str,
                            fields: Optional[StringListParam] = Query(default=None),
                            auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[UserResponse]:
    user = await User.get(user_id, NotFound("user not found"))
    rel = user.relation(auth_data.user)
    perm = auth_data.permissions

    if not perm.users.get(rel).read:
        raise NotFound("user not found")
    if not perm.users.get(rel).revoke_tokens:
        raise Forbidden("no permissions to revoke this user's tokens")

    await user.revoke_all_tokens()

    return StatusResponse(
        status="all tokens revoked",
        data=await users_transform(fields, auth_data)(user)
    )


@users_ctrl.post("/{user_id}/tokens", tags=["users"], name="Create a new user's token", status_code=201)
async def create_token(user_id: str,
                       body: CreateTokenRequest,
                       auth_data: AuthData = Depends(user_and_permissions)) -> CreateTokenResponse:
    user = await User.get(user_id, NotFound("user not found"))
    rel = user.relation(auth_data.user)
    perm = auth_data.permissions

    if not perm.users.get(rel).read:
        raise NotFound("user not found")
    if not perm.users.get(rel).create_tokens:
        raise Forbidden("no permissions to create tokens for this user")

    user_role = await user.role()
    if body.permissions.exceeds(user_role.permissions()):
        raise Forbidden("token permissions cannot exceed user role's permissions")
    if not perm.users.get(rel).assign_exceeding_roles and body.permissions.exceeds(perm):
        raise Forbidden("token permissions cannot exceed yours")

    token = Token.create(
        user_id=user.id,
        description=body.description,
        stored_permissions=body.permissions.dict(),
        auto_prolonged=body.auto_prolonged
    )
    await token.save()

    return CreateTokenResponse(
        token=token.token,
        status="token created"
    )


@users_ctrl.delete("/{user_id}", tags=["users"], name="Delete a user")
async def delete(user_id: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[UserResponse]:
    user = await User.get(user_id, NotFound("user not found"))

    check_read_and_destroy(
        auth_data,
        user,
        NotFound("user not found"),
        Forbidden("no permissions to delete this user")
    )

    await user.destroy()
    return StatusResponse(
        status="user deleted",
        data=await users_transform(fields, auth_data)(user)
    )
