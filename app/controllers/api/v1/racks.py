from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon.errors import NotFound, Forbidden

from app.models import Rack, Location
from app.models.api.rack import RackResponse, RackRequest
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    ObjectIdListParam,
    ObjectIdParam,
    StringListParam,
    user_and_permissions,
    pagination_params,
    name_filter,
    resolve_location_ids,
)
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.query import RackQueryBuilder


def racks_transform(
        fields: Optional[List[str]], auth_data: AuthData) -> Callable[[Rack], Awaitable[RackResponse]]:
    need_rel = fields and "rel" in fields

    async def transform(r: Rack) -> RackResponse:
        dct = await r.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = r.relation(auth_data.user).value
        return RackResponse(**dct)
    return transform


racks_ctrl = APIRouter(prefix="/api/v1/racks")


@racks_ctrl.get("/", tags=["racks"], name="Racks index and search",
                response_model_exclude_unset=True)
async def index(auth_data: AuthData = Depends(user_and_permissions),
                pagination: Optional[PaginationParams] = Depends(pagination_params),
                fields: Optional[StringListParam] = Query(default=None),
                name: NameFilter = Depends(name_filter),
                location_ids: Optional[ObjectIdParam] = Depends(resolve_location_ids),
                sort: Optional[str] = Query(default=None),
                ids: Optional[ObjectIdListParam] = Query(default=None)
                ) -> PaginatedList[RackResponse] | ItemsList[RackResponse]:

    qb = RackQueryBuilder(auth_data)
    qb.permissions()
    qb.name_filter(name)
    qb.ids(ids)
    qb.locations(location_ids)
    qb.sort(sort)

    racks = qb.find()
    return await paginated(
        racks,
        transform=racks_transform(fields, auth_data),
        params=pagination
    )


@racks_ctrl.get("/{rack_id}", tags=["racks"], name="Get a rack",
                response_model_exclude_unset=True)
async def show(rack_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> RackResponse:
    rack = await Rack.get(rack_id, NotFound("rack not found"))
    check_read(auth_data, rack, NotFound("rack not found"))
    return await racks_transform(fields, auth_data)(rack)


@racks_ctrl.post("/", tags=["racks"], name="Create a rack", status_code=201)
async def create(body: RackRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[RackResponse]:
    perm = auth_data.permissions
    if not perm.racks.all.create:
        raise Forbidden("no permissions to create new racks")

    location = await Location.get(body.location_id, NotFound("location not found"))
    check_read_and_update(auth_data, location, NotFound("location not found"),
                          Forbidden("no permissions to add racks to this location"))

    rack = Rack.from_pydantic(body)
    await rack.save()

    return StatusResponse(
        status="rack created",
        data=await racks_transform(fields, auth_data)(rack)
    )


@racks_ctrl.patch("/{rack_id}", tags=["racks"], name="Update a rack")
async def update(rack_id: str,
                 body: RackRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[RackResponse]:
    rack = await Rack.get(rack_id, NotFound("rack not found"))
    check_read_and_update(auth_data, rack, NotFound("rack not found"),
                          Forbidden("no permissions to update this rack"))

    if body.location_id:
        location = await Location.get(body.location_id, NotFound("location not found"))
        if location.id == rack.location_id:
            # the same parent, do not set
            body.location_id = None

    await rack.update_from_pydantic(body)
    await rack.save()

    return StatusResponse(
        status="rack updated",
        data=await racks_transform(fields, auth_data)(rack)
    )


@racks_ctrl.delete("/{rack_id}", tags=["racks"], name="Delete a rack")
async def delete(rack_id: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)
                 ) -> StatusResponse[RackResponse]:
    rack = await Rack.get(rack_id, NotFound("rack not found"))
    check_read_and_destroy(auth_data, rack, NotFound("rack not found"),
                           Forbidden("no permissions to update this rack"))
    await rack.destroy()
    return StatusResponse(
        status="rack deleted",
        data=await racks_transform(fields, auth_data)(rack)
    )
