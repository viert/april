from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon.errors import NotFound, Forbidden, InputDataError

from app.models import Role
from app.models.api.role import RoleRequest, RoleResponse
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    StringListParam,
    user_and_permissions,
    pagination_params,
    name_filter
)
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.query import RoleQueryBuilder

roles_ctrl = APIRouter(prefix="/api/v1/roles")


def roles_transform(fields: Optional[List[str]], auth_data: AuthData) -> Callable[[Role], Awaitable[RoleResponse]]:
    need_rel = fields and "rel" in fields
    need_users = False
    if fields and "users" in fields:
        need_users = True
        fields.remove("users")

    async def transform(r: Role) -> RoleResponse:
        dct = await r.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = r.relation(auth_data.user).value
        if need_users:
            users = []
            for user in await r.users().all():
                rel = user.relation(auth_data.user)
                if auth_data.permissions.check("users", rel.value, "read"):
                    users.append(user.to_dict())
            dct["users"] = users
        return RoleResponse(**dct)
    return transform


@roles_ctrl.get("/", tags=["roles"], name="Roles index and search", response_model_exclude_unset=True)
async def index(
        auth_data: AuthData = Depends(user_and_permissions),
        pagination: Optional[PaginationParams] = Depends(pagination_params),
        sort: Optional[str] = Query(default=None),
        fields: Optional[StringListParam] = Query(default=None),
        name: NameFilter = Depends(name_filter)) -> PaginatedList[RoleResponse] | ItemsList[RoleResponse]:

    qb = RoleQueryBuilder(auth_data)
    qb.permissions()
    qb.name_filter(name)
    qb.sort(sort)

    roles = qb.find()
    return await paginated(roles, transform=roles_transform(fields, auth_data), params=pagination)


@roles_ctrl.get("/{role_id}", tags=["roles"], name="Get a role", response_model_exclude_unset=True)
async def show(role_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[StringListParam] = Query(default=None)) -> RoleResponse:
    role = await Role.get(role_id, NotFound("role not found"))
    check_read(auth_data, role, NotFound("role not found"))
    return await roles_transform(fields, auth_data)(role)


@roles_ctrl.post("/", tags=["roles"], name="Create a role", status_code=201)
async def create(body: RoleRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[RoleResponse]:
    perm = auth_data.permissions
    if not perm.roles.all.create:
        raise Forbidden("no permissions to create new roles")

    if body.stored_permissions is None:
        raise InputDataError("stored_permissions field is mandatory")

    if body.stored_permissions.exceeds(perm):
        raise Forbidden("you can't create roles that exceed your permissions")

    role = Role.from_pydantic(body)
    await role.save()

    return StatusResponse(
        status="role created",
        data=await roles_transform(fields, auth_data)(role)
    )


@roles_ctrl.patch("/{role_id}", tags=["roles"], name="Update a role")
async def update(role_id: str,
                 body: RoleRequest,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[RoleResponse]:
    role = await Role.get(role_id, NotFound("role not found"))
    perm = auth_data.permissions
    check_read_and_update(
        auth_data,
        role,
        NotFound("role not found"),
        Forbidden("no permissions to modify this role")
    )

    if body.stored_permissions is not None and body.stored_permissions.exceeds(perm):
        raise Forbidden("you can't update role with permissions that exceed yours")

    await role.update_from_pydantic(body)
    return StatusResponse(
        status="role updated",
        data=await roles_transform(fields, auth_data)(role)
    )


@roles_ctrl.delete("/{role_id}", tags=["roles"], name="Delete a role")
async def delete(role_id: str,
                 fields: Optional[StringListParam] = Query(default=None),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[RoleResponse]:
    role = await Role.get(role_id, NotFound("role not found"))
    check_read_and_destroy(
        auth_data,
        role,
        NotFound("role not found"),
        Forbidden("no permissions to delete this role")
    )

    await role.destroy()
    return StatusResponse(
        status="role destroyed",
        data=await roles_transform(fields, auth_data)(role)
    )
