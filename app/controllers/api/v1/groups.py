import asyncio
from typing import Optional, List, Callable, Awaitable
from fastapi import APIRouter, Depends, Query

from croydon.errors import NotFound, Forbidden, InputDataError

from app.models import Group, Host, Team
from app.models.api.group import (
    GroupResponse,
    GroupCreateRequest,
    GroupUpdateRequest,
    GroupDeleteManyRequest,
    GroupSetParentRequest,
    GroupSetChildrenRequest,
    GroupSetHostsRequest,
    GroupMassTagsRequest,
    GroupMassSetParentRequest
)
from app.models.api import StatusResponse
from app.extractors import (
    AuthData,
    NameFilter,
    user_and_permissions,
    pagination_params,
    model_fields,
    name_filter,
    ObjectIdListParam,
    ObjectIdParam,
    StringListParam
)
from app.lib.rbac.check import check_read, check_read_and_update, check_read_and_destroy
from app.lib.pagination import PaginatedList, paginated, PaginationParams, ItemsList
from app.lib.query import GroupQueryBuilder


groups_ctrl = APIRouter(prefix="/api/v1/groups")


def groups_transform(fields: Optional[List[str]],
                     auth_data: AuthData) -> Callable[[Group], Awaitable[GroupResponse]]:
    need_rel = fields and "rel" in fields

    async def transform(g: Group) -> GroupResponse:
        dct = await g.to_dict_ext(fields=fields)
        if need_rel:
            dct["rel"] = g.relation(auth_data.user).value
        return GroupResponse(**dct)
    return transform


@groups_ctrl.get("/", tags=["groups"], name="Groups index and search", response_model_exclude_unset=True)
async def index(auth_data: AuthData = Depends(user_and_permissions),
                pagination: Optional[PaginationParams] = Depends(pagination_params),
                mine_only: bool = Query(False),
                ids: Optional[ObjectIdListParam] = Query(default=None),
                fields: Optional[List[str]] = Depends(model_fields),
                team_id: Optional[ObjectIdParam] = Query(default=None),
                local_tags: Optional[StringListParam] = Query(default=None),
                tags: Optional[StringListParam] = Query(default=None),
                sort: Optional[str] = Query(default=None),
                name: NameFilter = Depends(name_filter)) -> PaginatedList[GroupResponse] | ItemsList[GroupResponse]:

    qb = GroupQueryBuilder(auth_data)
    qb.permissions(mine_only=mine_only)
    qb.name_filter(name)
    qb.ids(ids)
    qb.team(team_id)
    qb.local_tags(local_tags)
    qb.sort(sort)
    await qb.tags(tags)

    groups = qb.find()
    return await paginated(groups, transform=groups_transform(fields, auth_data), params=pagination)


@groups_ctrl.get("/{group_id}", tags=["groups"], name="Get a group", response_model_exclude_unset=True)
async def show(group_id: str,
               auth_data: AuthData = Depends(user_and_permissions),
               fields: Optional[List[str]] = Depends(model_fields)) -> GroupResponse:
    group = await Group.get(group_id, NotFound("group not found"))
    check_read(auth_data, group, NotFound("group not found"))
    return await groups_transform(fields, auth_data)(group)


@groups_ctrl.post("/", tags=["groups"], name="Create a group", status_code=201)
async def create(body: GroupCreateRequest,
                 fields: Optional[List[str]] = Depends(model_fields),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[GroupResponse]:
    perm = auth_data.permissions
    if not perm.groups.others.create:
        raise Forbidden("no permissions to create new groups")

    team = await Team.get(body.team_id, NotFound("team not found"))
    check_read_and_update(auth_data,
                          team,
                          NotFound("team not found"),
                          Forbidden("no permissions to create groups in this team"))

    parent = None
    if body.parent_id:
        parent = await Group.get(body.parent_id, NotFound("parent not found"))
        check_read_and_update(auth_data,
                              parent,
                              NotFound("parent not found"),
                              Forbidden("no permissions to set this parent"))
        body.parent_id = None

    group = Group.from_pydantic(body)
    await group.save()

    if parent:
        await parent.add_child(group)

    return StatusResponse(
        status="group created",
        data=await groups_transform(fields, auth_data)(group)
    )


@groups_ctrl.delete("/{group_id}", tags=["groups"], name="Delete a group")
async def delete(group_id: str,
                 fields: Optional[List[str]] = Depends(model_fields),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[GroupResponse]:
    group = await Group.get(group_id, NotFound("group not found"))
    check_read_and_destroy(
        auth_data,
        group,
        NotFound("group not found"),
        Forbidden("no permissions to delete this group")
    )

    await group.destroy()

    return StatusResponse(
        status="group destroyed",
        data=await groups_transform(fields, auth_data)(group)
    )


@groups_ctrl.patch("/{group_id}", tags=["groups"], name="Update a group")
async def update(group_id: str,
                 body: GroupUpdateRequest,
                 fields: Optional[List[str]] = Depends(model_fields),
                 auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[GroupResponse]:
    group = await Group.get(group_id, NotFound("group not found"))
    check_read_and_update(
        auth_data,
        group,
        NotFound("group not found"),
        Forbidden("no permissions to update this group")
    )

    await group.update_from_pydantic(body)

    return StatusResponse(
        status="group updated",
        data=await groups_transform(fields, auth_data)(group)
    )


@groups_ctrl.post("/{group_id}/parent", tags=["groups"], name="Set a group's parent")
async def set_parent(group_id: str,
                     body: GroupSetParentRequest,
                     fields: Optional[List[str]] = Depends(model_fields),
                     auth_data: AuthData = Depends(user_and_permissions)
                     ) -> StatusResponse[GroupResponse]:
    group = await Group.get(group_id, NotFound("group not found"))
    check_read_and_update(
        auth_data,
        group,
        NotFound("group not found"),
        Forbidden("no permissions to update this group")
    )

    old_parent = await group.parent()
    if old_parent:
        check_read_and_update(
            auth_data,
            old_parent,
            NotFound("group not found"),
            Forbidden("no permissions to modify the old parent of this group")
        )

    if body.parent_id:
        parent = await Group.get(body.parent_id, NotFound("parent not found"))
        check_read_and_update(
            auth_data,
            parent,
            NotFound("parent not found"),
            Forbidden("no permissions to update the parent")
        )

        if group.parent_id == parent.id:
            raise InputDataError("group is already a child of this parent")

        await parent.add_child(group)
    else:
        await group.detach()

    return StatusResponse(
        status="group's parent set",
        data=await groups_transform(fields, auth_data)(group)
    )


@groups_ctrl.post("/{group_id}/children", tags=["groups"], name="Set a group's children")
async def set_children(group_id: str,
                       body: GroupSetChildrenRequest,
                       fields: Optional[List[str]] = Depends(model_fields),
                       auth_data: AuthData = Depends(user_and_permissions)
                       ) -> StatusResponse[GroupResponse]:
    group = await Group.get(group_id, NotFound("group not found"))
    check_read_and_update(
        auth_data,
        group,
        NotFound("group not found"),
        Forbidden("no permissions to update this group")
    )

    new_children = set(await Group.find({"_id": {"$in": body.child_ids}}).all())
    for child in new_children:
        check_read_and_update(auth_data,
                              child,
                              NotFound("one of the children wasn't found"),
                              Forbidden("no permissions to update one of the children")
                              )

    current_children = set(await group.children().all())

    children_to_remove = current_children.difference(new_children)
    children_to_add = new_children.difference(current_children)

    for child in children_to_add:
        child_curr_parent = await child.parent()
        check_read_and_update(auth_data,
                              child_curr_parent,
                              NotFound("one of the children wasn't found"),
                              Forbidden("no permissions to update one of the children's old parent")
                              )
    for child in children_to_remove:
        await child.detach()
    for child in children_to_add:
        await group.add_child(child)

    return StatusResponse(
        status="group's children set",
        data=await groups_transform(fields, auth_data)(group)
    )


@groups_ctrl.post("/{group_id}/hosts", tags=["groups"], name="Set group's hosts")
async def set_hosts(group_id: str,
                    body: GroupSetHostsRequest,
                    fields: Optional[List[str]] = Depends(model_fields),
                    auth_data: AuthData = Depends(user_and_permissions)
                    ) -> StatusResponse[GroupResponse]:
    group = await Group.get(group_id, NotFound("group not found"))
    check_read_and_update(
        auth_data,
        group,
        NotFound("group not found"),
        Forbidden("no permissions to update this group")
    )

    new_hosts = set(await Host.find({"_id": {"$in": body.host_ids}}).all())
    prev_group_ids = [x.group_id for x in new_hosts if x.group_id is not None]
    prev_groups = await Group.find({"_id": {"$in": prev_group_ids}}).all()

    for prev_group in prev_groups:
        check_read_and_update(auth_data,
                              prev_group,
                              NotFound("one of the hosts was not found"),
                              Forbidden("no permissions to detach current group from one of the hosts")
                              )

    for host in new_hosts:
        check_read_and_update(auth_data,
                              host,
                              NotFound("one of the hosts was not found"),
                              Forbidden("no permissions to move one of the hosts")
                              )

    current_hosts = set(await group.hosts().all())

    hosts_to_remove = current_hosts.difference(new_hosts)
    hosts_to_add = new_hosts.difference(current_hosts)

    hosts_to_remove_ids = [x.id for x in hosts_to_remove]
    if hosts_to_remove_ids:
        await Host.detach_many(hosts_to_remove_ids)

    for host in hosts_to_add:
        host.group_id = group.id
        await host.save()

    return StatusResponse(
        status="group's hosts set",
        data=await groups_transform(fields, auth_data)(group)
    )


@groups_ctrl.delete("/", tags=["groups"], name="Delete multiple groups")
async def delete_many(body: GroupDeleteManyRequest,
                      fields: Optional[StringListParam] = Query(default=None),
                      auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[GroupResponse]:
    user = auth_data.user
    perm = auth_data.permissions

    read_check = perm.hosts.cani("read")
    destroy_check = perm.hosts.cani("destroy")
    groups = await Group.find({"_id": {"$in": body.group_ids}}).all()
    groups = filter(lambda g: read_check[g.relation(user).value], groups)
    groups = list(groups)

    if not groups:
        raise NotFound("no groups found")

    forbidden = []
    for group in groups:
        if not destroy_check[group.relation(user).value]:
            forbidden.append(group.name)

    if forbidden:
        raise Forbidden(f"no permissions to delete groups {', '.join(forbidden)}")

    for group in groups:
        await group.destroy()

    transform = groups_transform(fields, auth_data)

    return StatusResponse(
        status=f"{len(groups)} groups deleted",
        data=await asyncio.gather(*[transform(group) for group in groups])
    )


@groups_ctrl.post("/tags", tags=["groups"], name="Add or overwrite local tags on multiple groups")
async def set_tags(body: GroupMassTagsRequest,
                   fields: Optional[StringListParam] = Query(default=None),
                   auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[GroupResponse]:
    user = auth_data.user
    perm = auth_data.permissions

    read_check = perm.groups.cani("read")
    update_check = perm.groups.cani("update")
    groups = await Group.find({"_id": {"$in": body.group_ids}}).all()
    groups = filter(lambda h: read_check[h.relation(user).value], groups)
    groups = list(groups)

    if not groups:
        raise NotFound("no groups found")

    forbidden = []
    for group in groups:
        if not update_check[group.relation(user).value]:
            forbidden.append(group.name)

    if forbidden:
        raise Forbidden(f"no permissions to update groups {', '.join(forbidden)}")

    for group in groups:
        if body.overwrite:
            group.local_tags = body.tags
        else:
            group.local_tags.extend(body.tags)
        await group.save()

    transform = groups_transform(fields, auth_data)

    return StatusResponse(
        status=f"{len(groups)} groups updated",
        data=await asyncio.gather(*[transform(group) for group in groups])
    )


@groups_ctrl.post("/parent", tags=["groups"], name="Move groups to a new parent")
async def mass_set_parent(body: GroupMassSetParentRequest,
                          fields: Optional[StringListParam] = Query(default=None),
                          auth_data: AuthData = Depends(user_and_permissions)) -> StatusResponse[GroupResponse]:
    user = auth_data.user
    perm = auth_data.permissions

    read_check = perm.groups.cani("read")
    update_check = perm.groups.cani("update")
    groups = await Group.find({"_id": {"$in": body.group_ids}}).all()
    groups = filter(lambda h: read_check[h.relation(user).value], groups)
    groups = list(groups)

    if not groups:
        raise NotFound("no groups found")

    forbidden = []
    for group in groups:
        if not update_check[group.relation(user).value]:
            forbidden.append(group.name)

    if forbidden:
        raise Forbidden(f"no permissions to update groups {', '.join(forbidden)}")

    parent = None
    if body.parent_id:
        parent = await Group.get(body.parent_id, NotFound("parent not found"))
        check_read_and_update(auth_data, parent, NotFound("parent not found"),
                              Forbidden("no permissions to move groups to this parent"))

    for group in groups:
        if parent:
            await parent.add_child(group)
        else:
            await group.detach()

    transform = groups_transform(fields, auth_data)
    status = f"{len(groups)} groups moved to parent {parent.name}" if parent \
        else f"{len(groups)} groups detached from their parents"

    return StatusResponse(
        status=status,
        data=await asyncio.gather(*[transform(group) for group in groups])
    )
