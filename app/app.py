import os.path
from croydon.baseapp import BaseApp
from app.middleware import SessionMiddleware, TimingMiddleware

from app.models.api.host import HostBaseResponse
from app.models.api.group import GroupResponse
from app.models.api.user import UserBaseResponse
from app.models.api.team import TeamResponse
from app.models.api.location import LocationResponse, LocationTreeItemResponse
from app.models.api.rack import RackBaseResponse, RackResponse
from app.models.api.role import RoleResponse
from app.models.api.layout import LayoutBaseResponse
from app.models.api.link import LinkBaseResponse

GroupResponse.update_forward_refs(
    HostBaseResponse=HostBaseResponse,
)
TeamResponse.update_forward_refs(
    UserBaseResponse=UserBaseResponse,
    LayoutBaseResponse=LayoutBaseResponse,
    LinkBaseResponse=LinkBaseResponse,
)
LocationResponse.update_forward_refs(RackBaseResponse=RackBaseResponse)
LocationTreeItemResponse.update_forward_refs(RackBaseResponse=RackBaseResponse)
RoleResponse.update_forward_refs(UserBaseResponse=UserBaseResponse)
RackResponse.update_forward_refs(HostBaseResponse=HostBaseResponse)


TAGS_METADATA = [
    {
        "name": "account",
        "description": "Various authentication handlers such as authentication itself, "
                       "checking the current user profile, logging out and so on"
    },
    {
        "name": "teams",
        "description": "Operations with teams"
    },
    {
        "name": "groups",
        "description": "Operations with groups"
    },
    {
        "name": "hosts",
        "description": "Operations with hosts"
    },
    {
        "name": "virtual_machines",
        "description": "Operations with virtual machines"
    },
    {
        "name": "locations",
        "description": "Operations with locations"
    },
    {
        "name": "racks",
        "description": "Operations with racks"
    },
    {
        "name": "layouts",
        "description": "Operations with layouts and object links"
    },
    {
        "name": "users",
        "description": "Operations with users"
    },
    {
        "name": "roles",
        "description": "Operations with user roles and permissions"
    },
    {
        "name": "selectors",
        "description": "Operations with various selectors"
    },
    {
        "name": "db_instances",
        "description": "Operations with database instances"
    }
]


class App(BaseApp):

    def __init__(self):
        app_dir = os.path.abspath(os.path.dirname(__file__))
        project_dir = os.path.abspath(os.path.join(app_dir, ".."))
        super().__init__(
            project_dir=project_dir,
            openapi_tags=TAGS_METADATA,
            title="April API",
            version="1.0.0",
        )

    def setup_routes(self):
        from .controllers.api.v1.account import account_ctrl
        from .controllers.api.v1.users import users_ctrl
        from .controllers.api.v1.roles import roles_ctrl
        from .controllers.api.v1.teams import teams_ctrl
        from .controllers.api.v1.groups import groups_ctrl
        from .controllers.api.v1.hosts import hosts_ctrl
        from .controllers.api.v1.locations import locations_ctrl
        from .controllers.api.v1.racks import racks_ctrl
        from .controllers.api.v1.layouts import layouts_ctrl
        from .controllers.api.v1.selectors import selectors_ctrl
        from .controllers.api.v1.db_instances import db_instances_ctrl

        self.include_router(account_ctrl)
        self.include_router(users_ctrl)
        self.include_router(roles_ctrl)
        self.include_router(teams_ctrl)
        self.include_router(hosts_ctrl)
        self.include_router(groups_ctrl)
        self.include_router(locations_ctrl)
        self.include_router(racks_ctrl)
        self.include_router(layouts_ctrl)
        self.include_router(selectors_ctrl)
        self.include_router(db_instances_ctrl)

    def setup_middleware(self) -> None:
        super().setup_middleware()
        self.add_middleware(SessionMiddleware)
        self.add_middleware(TimingMiddleware)


app = App()
