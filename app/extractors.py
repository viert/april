import asyncio
import re
from bson.objectid import ObjectId, InvalidId
from typing import Optional, Callable, Awaitable, List, Pattern, Dict
from dataclasses import dataclass
from fastapi import Depends, Request, Security, Query
from fastapi.security import APIKeyHeader

from croydon import ctx
from croydon.errors import NotFound

from app.models import Token, User, Session, Group, Location
from app.lib.rbac import Permissions
from app.lib.pagination import PaginationParams
from app.errors import AuthenticationError

NameFilter = Optional[Pattern | Dict[str, List[str]] | str]

api_key_header = APIKeyHeader(name="Authorization", auto_error=False)


@dataclass
class AuthData:
    user: User
    permissions: Permissions


class ObjectIdParam(str):

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v: str) -> ObjectId:
        try:
            return ObjectId(v)
        except InvalidId:
            raise ValueError("invalid object id")


class ObjectIdListParam(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v: str) -> List[ObjectId]:
        try:
            return [ObjectId(token.strip()) for token in v.split(",")]
        except InvalidId:
            raise ValueError("invalid object id list")


class StringListParam(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v: str) -> List[str]:
        return [token.strip() for token in v.split(",")]


async def auth_token(authorization: Optional[str] = Security(api_key_header)) -> Optional[Token]:
    if authorization is None:
        return None
    parts = authorization.split(" ")
    if len(parts) != 2:
        return None
    token_type, token = parts
    if token_type != "Token":
        return None
    token = await Token.cache_get(token)
    if token:
        if token.is_expired():
            ctx.log.debug(f"token {token.token} is expired")
            return None
        await token.auto_prolong()
    return token


async def current_session(request: Request) -> Session:
    return request.state.session


def authenticated_user(required: bool = True) -> Callable[[Optional[Token]], Awaitable[Optional[User]]]:
    async def inner(token: Optional[Token] = Depends(auth_token),
                    session: Optional[Session] = Depends(current_session)) -> Optional[User]:
        if token is not None:
            user = await token.user()
            if user:
                ctx.log.debug("authenticated_user found by token")
                return user

        if session is not None:
            user = await session.user()
            if user:
                ctx.log.debug("authenticated_user found by session")
                return user

        if required:
            ctx.log.debug("authenticated_user was not found but auth is required")
            raise AuthenticationError()

        ctx.log.debug("authenticated_user was not found")
        return None

    return inner


async def user_and_permissions(
        token: Optional[Token] = Depends(auth_token),
        user: User = Depends(authenticated_user(True))) -> AuthData:
    if token is None:
        role = await user.role()
        assert role is not None
        permissions = role.permissions()
    else:
        permissions = token.permissions()

    return AuthData(user=user, permissions=permissions)


async def pagination_params(page: int = 1,
                            limit: Optional[int] = None,
                            user: Optional[User] = Depends(authenticated_user(False)),
                            no_paging: bool = False) -> Optional[PaginationParams]:
    if no_paging:
        return None
    if limit is None:
        if user is not None:
            limit = user.user_settings.documents_per_page
        else:
            limit = ctx.cfg.general.documents_per_page

    return PaginationParams(page=page, limit=limit)


async def model_fields(fields: Optional[str] = None) -> Optional[List[str]]:
    if fields is None:
        return None
    return [field.strip() for field in fields.split(",")]


async def name_filter(name: Optional[str] = None) -> NameFilter:
    if name is None:
        return None
    if name.find(" ") >= 0:
        tokens = name.split()
        return {"$in": tokens}
    try:
        return re.compile(name, flags=re.IGNORECASE)
    except Exception:
        return ""


async def resolve_group_ids(group_ids: Optional[ObjectIdListParam] = Query(default=None),
                            group_lookup_recursive: Optional[bool] = Query(default=False),
                            unowned_only: Optional[bool] = Query(default=False)) -> Optional[List[ObjectId | None]]:
    if unowned_only:
        return [None]
    if not group_ids:
        return None
    if group_lookup_recursive and group_ids:
        groups = [Group.get(group_id, NotFound("group not found")) for group_id in group_ids]
        groups = await asyncio.gather(*groups)
        group_ids = set([group.id for group in groups if group])
        for group in groups:
            group_ids = group_ids.union(await group.all_child_ids())
        group_ids = list(group_ids)
    return group_ids


async def resolve_location_ids(location_id: Optional[ObjectIdParam] = Query(default=None),
                               location_lookup_recursive: Optional[bool] = Query(default=False),
                               no_location: Optional[bool] = Query(default=False)) -> Optional[List[ObjectId | None]]:
    if no_location:
        return [None]
    if not location_id:
        return None
    location = await Location.get(location_id, NotFound("location not found"))
    if location_lookup_recursive:
        return await location.tree_ids()
    return [location.id]
