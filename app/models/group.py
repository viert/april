import asyncio
from bson import ObjectId
from time import time
from typing import List, Self, Optional, TYPE_CHECKING

from croydon import ctx
from croydon.models.fields import StringField, ReferenceField, ListField
from croydon.decorators import api_field, save_required
from croydon.errors import NotFound, IntegrityError, ValidationError
from croydon.db import ObjectsCursor
from croydon.util import now

from app.lib.rbac import PermissionsNamespace
from app.lib.rbac.group import GroupRelation
from app.lib.rbac.rbac_model_mixin import RBACModel

from app.lib import NAME_EXPR
from .user import User
from .team import Team
from .timestamped_model import TimestampedModel
from .tree_model import TreeModel
from app.models.linkable_model import LinkableModel

if TYPE_CHECKING:
    from .host import Host


class Group(TimestampedModel, TreeModel, LinkableModel, RBACModel):

    RBAC_NAMESPACE = PermissionsNamespace.GROUPS
    COLLECTION = "groups"
    KEY_FIELD = "name"
    LINK_MODEL_TYPE = "group"

    name = StringField(required=True, unique=True, re_match=NAME_EXPR)
    description = StringField(required=True, default="")
    team_id: ReferenceField[Team] = ReferenceField(reference_model=Team, required=True)

    # auto-assigned fields, do not assign manually
    team_name = StringField(required=True, rejected=True, index=True)
    owner_ids: ListField[ObjectId] = ListField(required=True, rejected=True,
                                               restricted=True, default=list, index=True)

    def __hash__(self) -> Optional[int]:
        if self.id is None:
            return None
        return hash(f"Group.{self.id}")

    def relation(self, user: "User") -> GroupRelation:
        if user.id in self.owner_ids:
            return GroupRelation.OWNED
        return GroupRelation.OTHERS

    @api_field
    async def team(self) -> Team:
        return await Team.get(self.team_id)

    async def _assign_team_cache(self) -> None:
        """
        This must be run in _before_validation callback.
        Team cache fields are required but not meant to be set manually.
        This function derives all the required values from the associated team
        :return:
        """
        if self.team_id is None:
            raise ValidationError("team_id is required")
        t = await Team.cache_get(self.team_id)
        if t is None:
            raise NotFound("team not found")
        self.team_name = t.name
        self.owner_ids = t.all_user_ids()

    # region computed
    @api_field
    def hosts(self) -> ObjectsCursor["Host"]:
        from .host import Host
        return Host.find({"group_id": self.id})

    @api_field
    async def all_hosts(self) -> ObjectsCursor["Host"]:
        # TODO reimplement with aggregate() for better performance
        from .host import Host
        all_child_ids = await self.tree_ids()
        return Host.find({"group_id": {"$in": all_child_ids}})

    async def all_host_ids(self) -> List[ObjectId]:
        from .host import Host
        all_child_ids = await self.tree_ids()
        return await Host.find_ids({"group_id": {"$in": all_child_ids}})

    # endregion

    # region callbacks

    async def validate(self) -> None:
        if self.is_new:
            if self.parent_id is not None:
                raise IntegrityError("new groups can't have a parent. use add_child()")
            if self.child_ids:
                raise IntegrityError("new groups can't have children. use add_child()")
        else:
            if self.parent_id is not None:
                parent = await self.parent()
                if parent is None:
                    raise NotFound("parent not found")
                if parent.team_id != self.team_id:
                    raise IntegrityError("team and parent belong to different teams")
                children = await self.children().all()
                if children:
                    team_ids = set([child.team_id for child in children])
                    if len(team_ids) > 1 or self.team_id not in team_ids:
                        raise IntegrityError("team and children belong to different teams")

    async def _before_validation(self) -> None:
        await super()._before_validation()
        await self._assign_team_cache()

    # endregion

    async def drop_metadata_cache(self, drop_tags: bool, drop_attrs: bool,
                                  child_ids: Optional[List[ObjectId]] = None):
        if child_ids is None:
            child_ids = await self.tree_ids()
        await super().drop_metadata_cache(drop_tags, drop_attrs, child_ids)

        from .host import Host

        t1 = time()
        all_host_ids = await Host.find_ids({"group_id": {"$in": child_ids}})

        cache_keys = []

        if drop_tags:
            ctx.log.debug(f"invalidating tags cache for all hosts in group {self.name}")
            cache_keys += [f"hosts.{host_id}._tags" for host_id in all_host_ids]

        if drop_attrs:
            ctx.log.debug(f"invalidating attrs cache for all hosts in group {self.name}")
            cache_keys += [f"hosts.{host_id}._attrs" for host_id in all_host_ids]

        tasks = [self._invalidate(cache_key) for cache_key in cache_keys]
        await asyncio.gather(*tasks)
        t2 = time()

        items = []
        if drop_tags:
            items.append("tags")
        if drop_attrs:
            items.append("attrs")
        if items:
            ctx.log.debug(f"dropped {'/'.join(items)} cache ({len(cache_keys)} keys) in %.3f sec", t2-t1)

    @save_required
    async def add_child(self, child: Self) -> None:
        if self.team_id != child.team_id:
            raise IntegrityError("child's and parent's team must match")
        await super().add_child(child)

    @save_required
    async def move_team(self, team: Team):
        from .host import Host

        if self.team_id == team.id:
            raise IntegrityError(f"group already belongs to team {team.name}")
        await self.detach()

        owner_ids = team.all_user_ids()

        child_ids = await self.tree_ids()

        host_ids = await self.all_host_ids()

        await Group.update_many(
            {"_id": {"$in": child_ids}},
            {
                "$set": {
                    "team_id": team.id,
                    "team_name": team.name,
                    "owner_ids": owner_ids,
                    "updated_at": now()
                }
            }
        )

        await Host.update_many(
            {"_id": {"$in": host_ids}},
            {
                "$set": {
                    "owner_ids": owner_ids,
                    "updated_at": now()
                }
            }
        )
