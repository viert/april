from croydon.models.submodel import StorableSubmodel
from croydon.models.fields import ReferenceField, OnDestroy
from app.lib.rbac.rbac_model_mixin import RBACModel
from app.lib.rbac.component import ComponentRelation
from app.lib.rbac import PermissionsNamespace
from ..host import Host


class Component(StorableSubmodel, RBACModel):

    RBAC_NAMESPACE = PermissionsNamespace.COMPONENTS
    COLLECTION = "components"

    host_id: ReferenceField[Host] = ReferenceField(reference_model=Host, on_destroy=OnDestroy.DETACH)

    def relation(self, user: "User") -> ComponentRelation:
        return ComponentRelation.ALL
