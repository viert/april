from croydon.models.fields import StringField, IntField
from .component import Component


class Memory(Component):

    SUBMODEL = "memory"

    serial = StringField(required=True, unique=True)
    capacity = IntField(required=True, min_value=0)
    description = StringField(required=True)
    manufacturer = StringField(required=True)
    model = StringField(required=True)
    speed = IntField(required=True, min_value=0)
    type = StringField(required=True)


Component.register_submodel("memory", Memory)
