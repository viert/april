from croydon.models.fields import StringField, IntField
from .component import Component


class CPU(Component):

    SUBMODEL = "cpu"

    arch = StringField()
    name = StringField()
    cores = IntField(min_value=0)
    threads = IntField(min_value=0)
    family_name = StringField()
    family_number = IntField(min_value=0)
    manufacturer = StringField()
    model = StringField()
    cpu_id = StringField()
    speed = IntField(min_value=0)
    stepping = IntField(min_value=0)


Component.register_submodel("cpu", CPU)
