from typing import Optional, Dict, Any, Set, List, Self, TYPE_CHECKING
from pydantic import BaseModel
from random import choice

from croydon.models.fields import (
    StringField,
    ListField,
    ReferenceField,
    SelfReferenceField,
    IntField,
    ObjectIdField,
)
from croydon.decorators import api_field, model_cached_method, save_required
from croydon.db import ObjectsCursor
from croydon.errors import InputDataError
from croydon.models.index import Index, IndexKey, IndexDirection
from croydon.util import uuid4_string

from app.lib.rbac import PermissionsNamespace
from app.lib.rbac.host import HostRelation
from app.lib.attrs import merge
from .user import User
from .rack import Rack
from .location import Location
from .base_host import BaseHost
from app.models.selectors.state import State
from app.models.selectors.os_image import OsImage

if TYPE_CHECKING:
    from .components import Component
    from .db_instance import DBInstance


class InterfaceNetwork(BaseModel):
    family: int = 4
    ip: str
    gw: Optional[str] = None
    mask: str
    subnet: Optional[str]


class Interface(BaseModel):
    name: str
    mac: str
    speed: Optional[str]
    mgmt: bool = False
    virtual: bool = False
    networks: List[InterfaceNetwork]


def generate_serial() -> str:
    rand_syms = [choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") for _ in range(5)]
    rand_str = "".join(rand_syms)
    return f"AUTO{rand_str}"


class Host(BaseHost):

    RBAC_NAMESPACE = PermissionsNamespace.HOSTS
    COLLECTION = "hosts"
    KEY_FIELD = "fqdn"
    LINK_MODEL_TYPE = "host"

    INDEXES = [
        Index(keys=[IndexKey("interfaces.networks.ip", IndexDirection.ASCENDING)]),
        Index(keys=[IndexKey("interfaces.mac", IndexDirection.ASCENDING)]),
    ]

    serial = StringField(default=generate_serial, index=True)
    manufacturer = StringField(default="")
    model = StringField(default="")
    rack_id: ReferenceField[Rack] = ReferenceField(reference_model=Rack)
    rack_position = IntField(min_value=1, max_value=100)
    state_id: ReferenceField[State] = ReferenceField(reference_model=State)
    os_image_id: ReferenceField[OsImage] = ReferenceField(reference_model=OsImage)
    interfaces: ListField[Dict[str, Any]] = ListField(required=True, default=list)
    aliases: ListField[str] = ListField(required=True, index=True, default=list)

    vm_type = StringField(default="host", choices=["kvm", "host"])
    vm_host_id = SelfReferenceField(required=False)
    vcpu = IntField(default=0)
    memory = IntField(default=0)
    status = StringField()
    uuid = StringField(default=uuid4_string)

    # auto-assigned field
    location_id = ObjectIdField(rejected=True, index=True)

    def relation(self, user: "User") -> HostRelation:
        if self.group_id is None:
            return HostRelation.UNOWNED
        if user.id in self.owner_ids:
            return HostRelation.OWNED
        return HostRelation.OTHERS

    @classmethod
    def _default_get_query(cls, expr: str) -> Dict[str, Any]:
        return {"$or": [
            {"fqdn": expr},
            {"serial": expr},
            {"aliases": expr}
        ]}

    def is_vm(self):
        return self.vm_type != "host"

    @model_cached_method
    async def _tags(self) -> Set[str]:
        tags = set()
        if not self.is_vm():
            rack = await self.rack()
            if rack:
                tags = tags.union(await rack.tags())
        group = await self.group()
        if group:
            tags = tags.union(await group.tags())
        return tags.union(self.local_tags)

    @model_cached_method
    async def _attrs(self) -> Dict[str, Any]:
        attrs = {}
        if not self.is_vm():
            rack = await self.rack()
            if rack:
                attrs = merge(attrs, await rack.attrs())
        group = await self.group()
        if group:
            attrs = merge(attrs, await group.attrs())

        attrs = merge(attrs, self.local_attrs)
        return attrs

    @api_field
    def components(self, submodel: Optional[str] = None) -> ObjectsCursor["Component"]:
        from .components import Component
        query = {"host_id": self.id}
        if submodel:
            query["submodel"] = submodel
        return Component.find(query)

    @api_field
    def virtual_machines(self) -> ObjectsCursor[Self]:
        return Host.find({"vm_host_id": self.id})

    def db_instances(self) -> ObjectsCursor["DBInstance"]:
        from .db_instance import DBInstance
        return DBInstance.find({"host_id": self.id})

    async def _auto_assign(self) -> None:
        group = await self.group()
        if group:
            self.owner_ids = group.owner_ids
        else:
            self.owner_ids = []

        rack = await self.rack()
        if rack:
            dc = await rack.location()
            if dc:
                self.location_id = dc.id
            else:
                self.location_id = None
        else:
            self.location_id = None

    async def _before_validation(self) -> None:
        await super()._before_validation()

        if self.fqdn and self.fqdn.endswith("."):
            # drop a terminating dot
            self.fqdn = self.fqdn[:-1]

        if self.rack_id is None:
            self.rack_position = None

    def render_alias(self, pattern: str) -> str:
        tokens = self.fqdn.split(".")
        pattern = pattern.replace(f"$0", self.fqdn)
        for idx, token in enumerate(tokens):
            pattern = pattern.replace(f"${idx+1}", token)
        return pattern

    @api_field
    async def rack(self) -> Rack:
        return await Rack.get(self.rack_id)

    @api_field
    async def location(self) -> Location:
        return await Location.get(self.location_id)

    @api_field
    async def rack_full_name(self) -> Optional[str]:
        rack = await self.rack()
        if rack:
            return rack.full_name
        return None

    @api_field
    async def vm_host(self) -> Optional[Self]:
        return await Host.get(self.vm_host_id)

    @api_field
    async def vm_host_fqdn(self) -> Optional[str]:
        vm_host = await self.vm_host()
        if vm_host is None:
            return None
        return vm_host.fqdn

    async def _validate_vm_fields(self) -> None:
        host = await self.vm_host()
        if host is None:
            raise InputDataError("vm host not found")
        if host.id == self.id:
            raise InputDataError("vm can't reference itself as a host")
        if self.rack_id is not None:
            raise InputDataError("vm can't be attached to a rack")

    async def validate(self) -> None:
        await super().validate()

        if self.vm_type != "host":
            await self._validate_vm_fields()

        rack = await self.rack()
        if rack:
            if self.rack_position is None:
                raise InputDataError("rack_position must be set if a rack is assigned")
            if self.rack_position > rack.num_units:
                raise InputDataError(f"invalid rack_position, the rack has only {rack.num_units} units")
        for iface in self.interfaces:
            _ = Interface(**iface)

    @api_field
    async def state(self) -> Optional[State]:
        if self.state_id is None:
            return None
        return await State.cache_get(self.state_id)

    @api_field
    async def state_name(self) -> Optional[str]:
        state = await self.state()
        if state:
            return state.name
        return None

    @api_field
    async def os_image(self) -> Optional[OsImage]:
        if self.os_image_id is None:
            return None
        return await OsImage.cache_get(self.os_image_id)

    @api_field
    async def os_image_name(self) -> Optional[str]:
        os_image = await self.os_image()
        if os_image:
            return os_image.name
        return None

    @save_required
    async def absorb(self, other: Self):
        self.aliases.append(other.fqdn)
        self.local_tags += other.local_tags

        l_attrs = other.local_attrs
        l_attrs.update(self.local_attrs)
        self.local_attrs = l_attrs
        for model_ref in other._references:
            await model_ref.ref_class.update_many(
                {model_ref.ref_field: other.id},
                {
                    "$set": {
                        model_ref.ref_field: self.id
                    }
                }
            )
        await self.save()
        await other.destroy()
