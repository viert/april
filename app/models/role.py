from typing import List, Self, TYPE_CHECKING
from pydantic import ValidationError as PydanticValidationError
from croydon.models.fields import StringField, DictField
from croydon.errors import ValidationError, ObjectHasReferences
from croydon.db import ObjectsCursor
from croydon.decorators import api_field

from app.lib.rbac import (
    PermissionsNamespace, Permissions, DEFAULT_PERMISSIONS, SUPERUSER_PERMISSIONS, INVENTORY_PERMISSIONS
)
from app.lib.rbac.rbac_model_mixin import RBACModel
from app.lib.rbac.role import RoleRelation
from app.lib import NAME_EXPR
from .timestamped_model import TimestampedModel

if TYPE_CHECKING:
    from .user import User


class Role(TimestampedModel, RBACModel):

    RBAC_NAMESPACE = PermissionsNamespace.ROLES
    COLLECTION = "roles"
    KEY_FIELD = "name"

    name = StringField(required=True, unique=True, re_match=NAME_EXPR)
    stored_permissions = DictField(required=True)

    def permissions(self) -> Permissions:
        return Permissions(**self.stored_permissions)

    def relation(self, user: "User") -> RoleRelation:
        return RoleRelation.ALL

    @classmethod
    async def get_or_create(cls, name: str, permissions: Permissions) -> Self:
        role = await Role.cache_get(name)
        if not role:
            role = Role({"name": name, "stored_permissions": permissions.dict()})
            await role.save()
        return role

    @classmethod
    async def default(cls) -> Self:
        return await cls.get_or_create("default", DEFAULT_PERMISSIONS)

    @classmethod
    async def superuser(cls) -> Self:
        return await cls.get_or_create("superuser", SUPERUSER_PERMISSIONS)

    @classmethod
    async def restore_default(cls):
        """
        This is useful during migrations. When there are changes to permission settings
        this will recreate the default roles (id remains unchanged) with a new set of
        permissions
        """
        r = await cls.default()
        r.stored_permissions = DEFAULT_PERMISSIONS.dict()
        await r.save()

        r = await cls.superuser()
        r.stored_permissions = SUPERUSER_PERMISSIONS.dict()
        await r.save()

        r = await cls.inventory()
        r.stored_permissions = INVENTORY_PERMISSIONS.dict()
        await r.save()

    @classmethod
    async def inventory(cls) -> Self:
        return await cls.get_or_create("inventory", INVENTORY_PERMISSIONS)

    async def validate(self) -> None:
        try:
            _ = self.permissions()
        except PydanticValidationError as e:
            raise ValidationError(f"stored_permissions are invalid: {e}")

    @api_field
    def users(self) -> ObjectsCursor["User"]:
        from .user import User
        return User.find({"role_id": self.id})

    @api_field
    async def usernames(self) -> List[str]:
        return [u.username for u in await self.users().all()]

    async def _after_save(self, is_new: bool) -> None:
        users = await self.users().all()
        for user in users:
            await user.invalidate()
