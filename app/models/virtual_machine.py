from typing import Set, Dict, Any

from croydon.models.fields import StringField, ReferenceField, IntField
from croydon.decorators import api_field, model_cached_method

from app.lib.attrs import merge
from app.lib.rbac import PermissionsNamespace
from app.lib.rbac.virtual_machine import VirtualMachineRelation

from .base_host import BaseHost
from .host import Host


class VirtualMachine(BaseHost):

    RBAC_NAMESPACE = PermissionsNamespace.VIRTUAL_MACHINES
    KEY_FIELD = "fqdn"
    COLLECTION = "virtual_machines"
    LINK_MODEL_TYPE = "virtual_machine"

    vm_type = StringField(required=True, default="kvm", choices=["kvm", "docker"])
    host_id: ReferenceField[Host] = ReferenceField(reference_model=Host, rejected=True, required=True)
    vcpu = IntField(default=0, required=True)
    memory = IntField(default=0)
    image = StringField()
    status = StringField()
    uuid = StringField()

    def relation(self, user: "User") -> VirtualMachineRelation:
        if self.group_id is None:
            return VirtualMachineRelation.UNOWNED
        if user.id in self.owner_ids:
            return VirtualMachineRelation.OWNED
        return VirtualMachineRelation.OTHERS

    @model_cached_method
    async def _tags(self) -> Set[str]:
        tags = set()
        group = await self.group()
        if group:
            tags = tags.union(await group.tags())
        return tags.union(self.local_tags)

    @model_cached_method
    async def _attrs(self) -> Dict[str, Any]:
        attrs = {}
        group = await self.group()
        if group:
            attrs = merge(attrs, await group.attrs())

        attrs = merge(attrs, self.local_attrs)
        return attrs

    @api_field
    async def host(self) -> Host:
        return await Host.get(self.host_id)

    @api_field
    async def host_fqdn(self) -> str:
        host = await self.host()
        return host.fqdn

    async def _auto_assign(self) -> None:
        group = await self.group()
        if group:
            self.owner_ids = group.owner_ids
        else:
            self.owner_ids = []

        host = await self.host()
        if host:
            self.location_id = host.location_id
            self.rack_id = host.rack_id
            self.rack_position = host.rack_position
        else:
            self.location_id = None
            self.rack_id = None
            self.rack_position = None
