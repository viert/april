from abc import ABC
from typing import TYPE_CHECKING
from croydon.models.submodel import StorableSubmodel
from croydon.models.fields import StringField

from app.lib.rbac.rbac_model_mixin import RBACModel
from app.lib.rbac import PermissionsNamespace
from app.lib.rbac.selector import SelectorRelation

if TYPE_CHECKING:
    from .user import User


class Selector(StorableSubmodel, RBACModel, ABC):

    COLLECTION = "selectors"
    KEY_FIELD = "name"
    RBAC_NAMESPACE = PermissionsNamespace.SELECTORS

    name = StringField()  # it is required and unique due to being the KEY_FIELD

    def relation(self, user: "User") -> SelectorRelation:
        return SelectorRelation.ALL
