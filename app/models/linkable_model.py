from abc import ABC
from typing import TypeVar, TYPE_CHECKING, Dict
from croydon.models import StorableModel
from croydon.db import ObjectsCursor
from croydon.decorators import api_field
if TYPE_CHECKING:
    from app.models.links.layout import Layout
    from app.models.links.link import Link


class LinkableModel(StorableModel, ABC):

    LINK_MODEL_TYPE: str
    CTOR_MAP: Dict[str, "TLinkableModel"] = {}

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        if hasattr(cls, "LINK_MODEL_TYPE"):
            LinkableModel.CTOR_MAP[cls.LINK_MODEL_TYPE] = cls

    async def _before_delete(self) -> None:
        await super()._before_delete()
        from .links import Link
        await Link.destroy_many({"$or": [
            {"source_id": self.id},
            {"target_id": self.id}
        ]})

    @api_field
    def links(self) -> ObjectsCursor["Link"]:
        from .links import Link
        return Link.find_by_object_id(self.id)

    @api_field
    async def layouts(self) -> ObjectsCursor["Layout"]:
        from .links import Layout
        links = await self.links().all()
        layout_ids = list(set([link.layout_id for link in links]))
        return Layout.find({"_id": {"$in": layout_ids}})


TLinkableModel = TypeVar("TLinkableModel", bound=LinkableModel)
