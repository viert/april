import asyncio
import itertools
from abc import ABC
from time import time
from motor.motor_asyncio import AsyncIOMotorCursor
from typing import Self, Optional, List, Dict, Any, TypeVar, Set
from bson import ObjectId

from croydon import ctx
from croydon.db import ObjectsCursor
from croydon.models import StorableModel
from croydon.models.fields import ObjectIdField, ListField, DictField
from croydon.decorators import api_field, save_required, model_cached_method
from croydon.errors import ObjectSaveRequired, ObjectHasReferences

from app.errors import ChildExists, ChildDoesNotExist, CyclicChain
from app.lib.attrs import merge


TTreeModel = TypeVar("TTreeModel", bound="TreeModel")


class TreeModel(StorableModel, ABC):

    # use add_child() and remove_child() methods to keep integrity,
    # avoid setting these fields up manually
    parent_id = ObjectIdField(rejected=True)
    child_ids: ListField[ObjectId] = ListField(required=True, rejected=True, index=True, default=list)
    local_tags: ListField[str] = ListField(required=True, default=list, index=True)
    local_attrs: ListField[str] = DictField(required=True, default=dict)

    _invalidate_tags_cache: bool = False
    _invalidate_attrs_cache: bool = False

    def has_child(self, child: TTreeModel) -> bool:
        return child.id in self.child_ids

    @api_field
    def children(self) -> ObjectsCursor[Self]:
        return self.__class__.find({"_id": {"$in": self.child_ids}})

    @api_field
    async def parent(self) -> Optional[Self]:
        if self.parent_id is None:
            return None
        return await self.__class__.get(self.parent_id)

    def _aggregate_children(self, project: Optional[Dict[str, Any]] = None) -> AsyncIOMotorCursor:

        graph_lookup = {
            "from": self.collection,
            "as": "all_children",
            "startWith": "$child_ids",
            "connectFromField": "child_ids",
            "connectToField": "_id"
        }

        pipeline = [{"$graphLookup": graph_lookup}]

        if project:
            pipeline.append({"$project": project})

        return self.__class__.aggregate(pipeline, {"_id": self.id})

    async def all_child_ids(self) -> List[ObjectId]:
        cur = self._aggregate_children({"all_child_ids": "$all_children._id"})
        results = await cur.to_list(None)
        if not results:
            return []
        return results[0]["all_child_ids"]

    async def tree_ids(self) -> List[ObjectId]:
        ids = await self.all_child_ids()
        ids.append(self.id)
        return ids

    async def all_children(self) -> List[Self]:
        cur = self._aggregate_children()
        results = await cur.to_list(None)
        if not results:
            return []
        return [self.__class__(attrs) for attrs in results[0]["all_children"]]

    @save_required
    async def remove_child(self, child: TTreeModel) -> None:
        if not self.has_child(child):
            raise ChildDoesNotExist()
        child.parent_id = None
        self.child_ids.remove(child.id)
        await child.save()
        await self.save()

    @save_required
    async def detach(self) -> None:
        parent = await self.parent()
        if parent is None:
            return None
        await parent.remove_child(self)

    @save_required
    async def remove_all_children(self) -> None:
        tasks = []
        for child in await self.children().all():
            child.parent_id = None
            tasks.append(child.save())
        await asyncio.gather(*tasks)
        self.child_ids = []
        await self.save()

    @save_required
    async def add_child(self, child: TTreeModel) -> None:
        if child.is_new:
            raise ObjectSaveRequired("child must be saved first")
        if self.has_child(child):
            raise ChildExists()
        if self.id == child.id:
            raise CyclicChain(f"can't add {self.__class__.__name__} as its own child")
        if self.id in await child.all_child_ids():
            raise CyclicChain("the child contains the parent in its descendants")

        old_parent = await child.parent()
        if old_parent:
            await old_parent.remove_child(child)

        child.parent_id = self.id
        self.child_ids.append(child.id)
        await child.save()
        await self.save()

    async def _before_delete(self) -> None:
        await super()._before_delete()
        if self.child_ids:
            raise ObjectHasReferences(f"{self.__class__.__name__} has children")
        if self.parent_id:
            parent = await self.parent()
            await parent.remove_child(self)

    async def _before_save(self) -> None:
        await super()._before_save()
        if not self.is_new:
            # if local tags have changed, drop tags cache
            if self.local_tags != self._initial_state["local_tags"]:
                self._invalidate_tags_cache = True
            # if local attrs have changed, drop tags cache
            if self.local_attrs != self._initial_state["local_attrs"]:
                self._invalidate_attrs_cache = True

            # if the chain has been modified, drop all the inheritance caches
            if self.parent_id != self._initial_state["parent_id"]:
                self._invalidate_tags_cache = True
                self._invalidate_attrs_cache = True

    async def _before_validation(self) -> None:
        await super()._before_validation()
        await self._unify_tags()

    async def _after_save(self, is_new: bool) -> None:
        await super()._after_save(is_new)
        if self._invalidate_tags_cache or self._invalidate_attrs_cache:
            await self.drop_metadata_cache(self._invalidate_tags_cache, self._invalidate_attrs_cache)
            self._invalidate_tags_cache = False
            self._invalidate_attrs_cache = False

    @model_cached_method
    async def _tags(self) -> Set[str]:
        if self.parent_id is None:
            return set(self.local_tags)
        parent: Self = await self.parent()
        parent_tags = await parent._tags()
        tags = parent_tags.union(self.local_tags)
        return tags

    @model_cached_method
    async def _attrs(self) -> Dict[str, Any]:
        if self.parent_id is None:
            return self.local_attrs
        parent: Self = await self.parent()
        parent_attrs = await parent._attrs()
        attrs = merge(parent_attrs, self.local_attrs)
        return attrs

    @api_field
    async def tags(self) -> List[str]:
        return list(await self._tags())

    @api_field
    async def attrs(self) -> Dict[str, Any]:
        return await self._attrs()

    async def _unify_tags(self) -> None:
        """
        This must be run in _before_validation callback.
        The method makes local_tags list behave like a set
        :return:
        """
        self.local_tags = list(set(self.local_tags))

    @api_field
    async def chain(self) -> List[Self]:
        if self.parent_id is None:
            return [self]
        parent = await self.parent()
        chain = await parent.chain()
        chain.append(self)
        return chain

    async def drop_metadata_cache(self, drop_tags: bool, drop_attrs: bool,
                                  child_ids: Optional[List[ObjectId]] = None):
        t1 = time()
        if child_ids is None:
            child_ids = await self.tree_ids()

        cache_keys = []

        ident = f"{self.__class__.__name__}:{getattr(self, self.KEY_FIELD)}"

        if drop_tags:
            ctx.log.debug(f"either local_tags or parent have changed for {ident}, "
                          f"invalidating tags cache for self and all children")
            cache_keys += [f"{self.collection}.{child_id}._tags" for child_id in child_ids]

        if drop_attrs:
            ctx.log.debug(f"either local_attrs or parent have changed for {ident}, "
                          f"invalidating attrs cache for self and all children")
            cache_keys += [f"{self.collection}.{child_id}._attrs" for child_id in child_ids]

        tasks = [self._invalidate(cache_key) for cache_key in cache_keys]
        await asyncio.gather(*tasks)
        t2 = time()

        items = []
        if drop_tags:
            items.append("tags")
        if drop_attrs:
            items.append("attrs")
        if items:
            ctx.log.debug(f"dropped {'/'.join(items)} cache ({len(cache_keys)} keys) in %.3f sec", t2-t1)

    def add_local_tag(self, tag: str):
        if tag in self.local_tags:
            return
        self.local_tags.append(tag)

    def remove_local_tag(self, tag: str):
        if tag not in self.local_tags:
            return
        self.local_tags.remove(tag)

    @classmethod
    async def find_ids_by_tags_recursive(cls, tags: List[str]) -> List[str]:
        top_level = await cls.find({"local_tags": {"$in": tags}}).all()
        tasks = [item.tree_ids() for item in top_level]
        ids = set(itertools.chain(*(await asyncio.gather(*tasks))))
        return list(ids)

    @classmethod
    async def find_by_tags_recursive(cls, tags: List[str]) -> ObjectsCursor[Self]:
        ids = await cls.find_ids_by_tags_recursive(tags)
        return cls.find({"_id": {"$in": ids}})
