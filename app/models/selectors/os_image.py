from typing import TYPE_CHECKING
from croydon.db import ObjectsCursor
from ..selector import Selector

if TYPE_CHECKING:
    from app.models.host import Host


class OsImage(Selector):

    SUBMODEL = "os_image"

    def hosts(self) -> ObjectsCursor["Host"]:
        from ..host import Host
        return Host.find({"os_image_id": self.id})


Selector.register_submodel("os_image", OsImage)
