from typing import TYPE_CHECKING
from croydon.db import ObjectsCursor
from ..selector import Selector

if TYPE_CHECKING:
    from app.models.host import Host


class State(Selector):

    SUBMODEL = "state"

    def hosts(self) -> ObjectsCursor["Host"]:
        from ..host import Host
        return Host.find({"state_id": self.id})


Selector.register_submodel("state", State)
