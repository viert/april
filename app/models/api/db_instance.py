from typing import Optional, List
from pydantic import BaseModel, validator
from datetime import datetime

from .host import HostBaseResponse
from .team import TeamBaseResponse
from .layout import LayoutBaseResponse
from .link import LinkableModelResponse
from . import convert_to_object_id, convert_from_object_id


class DBInstanceBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]
    host_id: Optional[str]
    team_id: Optional[str]
    db_type: Optional[str]
    port: Optional[int]
    path: Optional[str]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    _convert_object_ids = validator("id", "team_id", "host_id",
                                    pre=True, allow_reuse=True)(convert_from_object_id)


class DBInstanceResponse(DBInstanceBaseResponse, LinkableModelResponse):

    host: Optional[HostBaseResponse]
    team: Optional[TeamBaseResponse]
    layouts: Optional[List[LayoutBaseResponse]]
    rel: Optional[str]

    @validator("host", pre=True)
    def convert_host(cls, v):
        if hasattr(v, "to_dict"):
            return HostBaseResponse(**v.to_dict())
        return v

    @validator("team", pre=True)
    def convert_team(cls, v):
        if hasattr(v, "to_dict"):
            return TeamBaseResponse(**v.to_dict())
        return v


class DBInstanceRequest(BaseModel):
    host_id: Optional[str]
    team_id: Optional[str]
    db_type: Optional[str]
    name: Optional[str]
    port: Optional[int]
    path: Optional[str]

    _convert_object_ids = validator("host_id", "team_id", allow_reuse=True)(convert_to_object_id)
