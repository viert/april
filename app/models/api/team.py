from typing import Optional, List, TYPE_CHECKING
from pydantic import BaseModel, validator
from datetime import datetime
from . import convert_from_object_id, convert_to_object_id
if TYPE_CHECKING:
    from .user import UserBaseResponse
    from .layout import LayoutBaseResponse
    from .link import LinkBaseResponse


class TeamBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]
    description: Optional[str]
    email: Optional[str]
    owner_id: Optional[str]
    member_ids: Optional[List[str]]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    _convert_object_ids = validator(
        "id", "owner_id", pre=True, allow_reuse=True)(convert_from_object_id)
    _convert_object_id_lists = validator(
        "member_ids", each_item=True, pre=True, allow_reuse=True)(convert_from_object_id)


class TeamResponse(TeamBaseResponse):
    rel: Optional[str]

    owner: Optional["UserBaseResponse"]
    owner_username: Optional[str]
    members: Optional[List["UserBaseResponse"]]
    member_usernames: Optional[List[str]]
    layouts: Optional[List["LayoutBaseResponse"]]
    links: Optional[List["LinkBaseResponse"]]

    @validator("owner", pre=True)
    def convert_owner(cls, v):
        from .user import UserBaseResponse
        if hasattr(v, "to_dict"):
            return UserBaseResponse(**v.to_dict())
        return v

    @validator("members", pre=True, each_item=True)
    def convert_members(cls, v):
        from .user import UserBaseResponse
        if hasattr(v, "to_dict"):
            return UserBaseResponse(**v.to_dict())
        return v


class TeamRequest(BaseModel):
    name: Optional[str]
    description: Optional[str]
    email: Optional[str]


class TeamSetOwnerRequest(BaseModel):
    owner_id: str

    _convert_object_ids = validator("owner_id", allow_reuse=True)(convert_to_object_id)


class TeamSetMembersRequest(BaseModel):
    member_ids: List[str]

    _convert_object_ids = validator("member_ids", each_item=True, allow_reuse=True)(convert_to_object_id)
