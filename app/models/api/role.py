from typing import Optional, List, TYPE_CHECKING
from pydantic import BaseModel, validator
from datetime import datetime
from app.lib.rbac import Permissions
from . import convert_from_object_id
if TYPE_CHECKING:
    from .user import UserBaseResponse


class RoleBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]
    stored_permissions: Optional[Permissions]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    _convert_object_ids = validator("id", pre=True, allow_reuse=True)(convert_from_object_id)


class RoleResponse(RoleBaseResponse):
    rel: Optional[str]
    usernames: Optional[List[str]]
    users: Optional[List["UserBaseResponse"]]


class RoleRequest(BaseModel):
    name: Optional[str]
    stored_permissions: Optional[Permissions]
