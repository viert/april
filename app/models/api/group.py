from typing import Optional, List, Dict, Any, TYPE_CHECKING
from pydantic import BaseModel, validator
from datetime import datetime
from .team import TeamBaseResponse
from .layout import LayoutBaseResponse
from .link import LinkableModelResponse
from . import convert_to_object_id, convert_from_object_id
if TYPE_CHECKING:
    from .host import HostBaseResponse


class GroupBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]
    description: Optional[str]
    team_id: Optional[str]
    team_name: Optional[str]
    local_tags: Optional[List[str]]
    local_attrs: Optional[Dict[str, Any]]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    parent_id: Optional[str]
    child_ids: Optional[List[str]]

    _convert_object_ids = validator(
        "id", "team_id", "parent_id", pre=True, allow_reuse=True)(convert_from_object_id)
    _convert_object_id_lists = validator(
        "child_ids", each_item=True, pre=True, allow_reuse=True)(convert_from_object_id)


class GroupResponse(GroupBaseResponse, LinkableModelResponse):
    rel: Optional[str]

    team: Optional[TeamBaseResponse]
    tags: Optional[List[str]]
    attrs: Optional[Dict[str, Any]]
    parent: Optional[GroupBaseResponse]
    children: Optional[List[GroupBaseResponse]]
    chain: Optional[List[GroupBaseResponse]]
    hosts: Optional[List["HostBaseResponse"]]
    all_hosts: Optional[List["HostBaseResponse"]]

    layouts: Optional[List[LayoutBaseResponse]]

    @validator("team", pre=True)
    def convert_team(cls, v: Any) -> Any:
        if hasattr(v, "to_dict"):
            return TeamBaseResponse(**v.to_dict())
        return v

    @validator("parent", pre=True)
    def convert_parent(cls, v: Any) -> Any:
        if hasattr(v, "to_dict"):
            return GroupBaseResponse(**v.to_dict())
        return v

    @validator("children", "chain", pre=True, each_item=True)
    def convert_children(cls, v: Any) -> Any:
        if hasattr(v, "to_dict"):
            return GroupBaseResponse(**v.to_dict())
        return v

    @validator("hosts", "all_hosts", pre=True, each_item=True)
    def convert_hosts(cls, v: Any) -> Any:
        from .host import HostBaseResponse
        if hasattr(v, "to_dict"):
            return HostBaseResponse(**v.to_dict())
        return v


class GroupUpdateRequest(BaseModel):
    name: Optional[str]
    description: Optional[str]
    local_tags: Optional[List[str]]
    local_attrs: Optional[Dict[str, Any]]


class GroupCreateRequest(GroupUpdateRequest):
    team_id: str
    parent_id: Optional[str]

    _convert_object_ids = validator("team_id", "parent_id", allow_reuse=True)(convert_to_object_id)


class GroupSetParentRequest(BaseModel):
    parent_id: Optional[str]

    _convert_object_ids = validator("parent_id", allow_reuse=True)(convert_to_object_id)


class GroupSetChildrenRequest(BaseModel):
    child_ids: List[str]

    _convert_object_ids = validator("child_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class GroupSetHostsRequest(BaseModel):
    host_ids: List[str]

    _convert_object_ids = validator("host_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class GroupDeleteManyRequest(BaseModel):
    group_ids: List[str]
    _convert_object_id_list = validator("group_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class GroupMassTagsRequest(BaseModel):
    group_ids: List[str]
    tags: List[str]
    overwrite: bool = False
    _convert_object_id_list = validator("group_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class GroupMassSetParentRequest(BaseModel):
    group_ids: List[str]
    parent_id: Optional[str]

    _convert_object_id_list = validator("group_ids", each_item=True, allow_reuse=True)(convert_to_object_id)
    _convert_object_id = validator("parent_id", allow_reuse=True)(convert_to_object_id)
