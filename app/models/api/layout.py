from pydantic import BaseModel, validator, ValidationError, Field
from typing import Optional, List, Dict, Any
from datetime import datetime
from .team import TeamBaseResponse
from .link import LinkableModelResponse
from ..linkable_model import LinkableModel
from . import convert_from_object_id, convert_to_object_id


class NodePosition(BaseModel):
    x: float
    y: float


class LayoutBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]
    team_id: Optional[str]
    node_positions: Optional[Dict[str, NodePosition]]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    _convert_object_ids = validator("id", "team_id", pre=True, allow_reuse=True)(convert_from_object_id)


class LayoutResponse(LayoutBaseResponse, LinkableModelResponse):
    team: Optional[TeamBaseResponse]
    num_links: Optional[int]
    rel: Optional[str]

    @validator("team", pre=True)
    def convert_team(cls, v: Any) -> Any:
        if hasattr(v, "to_dict"):
            return TeamBaseResponse(**v.to_dict())
        return v


class LayoutUpdateRequest(BaseModel):
    name: Optional[str]
    team_id: Optional[str]
    node_positions: Optional[Dict[str, NodePosition]]

    _convert_object_ids = validator("team_id", allow_reuse=True)(convert_to_object_id)


class LayoutCreateRequest(LayoutUpdateRequest):
    pass


class Node(BaseModel):
    node_type: str = Field(regex="|".join(LinkableModel.CTOR_MAP.keys()))
    node_position: Optional[NodePosition] = None

    @validator("node_position")
    def convert_node_position(cls, v: Any) -> Any:
        return v.dict() if v is not None else None

    async def get_object(self, obj_id: str, raise_if_none: Exception):
        from app.models.links import Link
        if self.node_type not in LinkableModel.CTOR_MAP:
            raise ValidationError(f"invalid node type {self.node_type}")
        ctor = LinkableModel.CTOR_MAP.get(self.node_type)
        return await ctor.get(obj_id, raise_if_none)


class LinkSet(BaseModel):
    id: Optional[str]
    source_id: str
    source_type: str = Field(regex="|".join(LinkableModel.CTOR_MAP.keys()))
    target_id: str
    target_type: str = Field(regex="|".join(LinkableModel.CTOR_MAP.keys()))
    meta: Optional[Dict[str, Any]]

    _convert_object_ids = validator("source_id", "target_id", allow_reuse=True)(convert_to_object_id)


class LayoutSetLinksRequest(BaseModel):
    nodes: Optional[Dict[str, Node]]
    links: List[LinkSet]

    def check_and_update_nodes(self):
        """
        checks if the nodes property have all ids mentioned in the links
        """
        if self.nodes is None:
            self.nodes = {}
        for link in self.links:
            if link.source_id not in self.nodes:
                self.nodes[link.source_id] = Node(node_type=link.source_type)
            if link.target_id not in self.nodes:
                self.nodes[link.target_id] = Node(node_type=link.target_type)
