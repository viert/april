from bson import ObjectId
from typing import TypeVar, Generic, Any, List
from pydantic.generics import GenericModel, BaseModel

TBaseModel = TypeVar("TBaseModel", bound=BaseModel)


class StatusResponse(GenericModel, Generic[TBaseModel]):
    status: str
    data: TBaseModel | List[TBaseModel]


def convert_to_object_id(v: Any) -> Any:
    if isinstance(v, str):
        return ObjectId(v)
    return v


def convert_from_object_id(v: Any) -> Any:
    if isinstance(v, ObjectId):
        return str(v)
    return v
