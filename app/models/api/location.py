from typing import Optional, List, Dict, Any, TYPE_CHECKING
from pydantic import BaseModel, validator
from datetime import datetime
from . import convert_to_object_id, convert_from_object_id
from .layout import LayoutBaseResponse
from .link import LinkableModelResponse
if TYPE_CHECKING:
    from .rack import RackBaseResponse


class LocationBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]
    full_name: Optional[str]
    description: Optional[str]
    root_id: Optional[str]
    parent_id: Optional[str]
    child_ids: Optional[List[str]]
    local_tags: Optional[List[str]]
    type: Optional[str]
    local_attrs: Optional[Dict[str, Any]]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    _convert_object_ids = validator("id", "root_id", "parent_id", pre=True, allow_reuse=True)(convert_from_object_id)
    _convert_object_id_lists = validator("child_ids", pre=True, allow_reuse=True, each_item=True)(convert_from_object_id)


class LocationResponse(LocationBaseResponse, LinkableModelResponse):
    rel: Optional[str]

    root: Optional[LocationBaseResponse]
    parent: Optional[LocationBaseResponse]
    racks: Optional[List["RackBaseResponse"]]
    tags: Optional[List[str]]
    attrs: Optional[Dict[str, Any]]
    children: Optional[List[LocationBaseResponse]]
    chain: Optional[List[LocationBaseResponse]]
    layouts: Optional[List[LayoutBaseResponse]]

    @validator("root", "parent", pre=True)
    def convert_location(cls, v):
        if hasattr(v, "to_dict"):
            return LocationResponse(**v.to_dict())
        return v

    @validator("children", "chain", pre=True, each_item=True)
    def convert_children(cls, v: Any) -> Any:
        if hasattr(v, "to_dict"):
            return LocationBaseResponse(**v.to_dict())
        return v


class LocationTreeItemResponse(LocationResponse):
    children: List["LocationTreeItemResponse"]

    @validator("root", "parent", pre=True)
    def convert_location(cls, v):
        if hasattr(v, "to_dict"):
            return LocationResponse(**v.to_dict())
        return v

    @validator("children", pre=True, each_item=True)
    def convert_children(cls, v):
        if isinstance(v, dict):
            return LocationTreeItemResponse(**v)
        return v


class LocationTreeResponse(BaseModel):
    data: List[LocationTreeItemResponse]

    @validator("data", pre=True, each_item=True)
    def convert_data(cls, v):
        if isinstance(v, dict):
            return LocationTreeItemResponse(**v)
        return v


class LocationRequest(BaseModel):
    name: Optional[str]
    description: Optional[str]
    local_tags: Optional[List[str]]
    local_attrs: Optional[Dict[str, Any]]
    parent_id: Optional[str]
    type: Optional[str]

    _convert_object_id = validator("parent_id", allow_reuse=True)(convert_to_object_id)
