from typing import Optional, List, Dict, Any, TYPE_CHECKING
from pydantic import BaseModel, validator
from datetime import datetime
from .location import LocationBaseResponse
from .layout import LayoutBaseResponse
from .link import LinkableModelResponse
from . import convert_to_object_id, convert_from_object_id
if TYPE_CHECKING:
    from .host import HostBaseResponse


class RackBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]
    num_units: Optional[int]
    location_id: Optional[str]
    full_name: Optional[str]
    local_tags: Optional[List[str]]
    local_attrs: Optional[Dict[str, Any]]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    _convert_object_ids = validator("id", "location_id", pre=True, allow_reuse=True)(convert_from_object_id)


class RackResponse(RackBaseResponse, LinkableModelResponse):
    rel: Optional[str]

    location: Optional[LocationBaseResponse]
    tags: Optional[List[str]]
    attrs: Optional[Dict[str, Any]]
    hosts: Optional[List["HostBaseResponse"]]
    layouts: Optional[List[LayoutBaseResponse]]

    @validator("location", pre=True)
    def convert_location(cls, v):
        if hasattr(v, "to_dict"):
            return LocationBaseResponse(**v.to_dict())
        return v


class RackRequest(BaseModel):
    name: Optional[str]
    num_units: Optional[int]
    local_tags: Optional[List[str]]
    local_attrs: Optional[Dict[str, Any]]
    location_id: Optional[str]

    _convert_object_id = validator("location_id", allow_reuse=True)(convert_to_object_id)
