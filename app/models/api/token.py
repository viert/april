from typing import Optional
from pydantic import BaseModel, validator
from datetime import datetime
from app.lib.rbac import Permissions
from . import convert_from_object_id


class TokenBaseResponse(BaseModel):
    token_type: Optional[str]
    token: Optional[str]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    expires_at: Optional[datetime]
    description: Optional[str]
    user_id: Optional[str]
    stored_permissions: Optional[Permissions]

    _convert_object_ids = validator("user_id", pre=True, allow_reuse=True)(convert_from_object_id)
