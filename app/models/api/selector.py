from typing import Optional
from pydantic import BaseModel, validator, Field
from . import convert_from_object_id
from ..selector import Selector


class SelectorBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]
    submodel: Optional[str] = Field(regex="|".join(Selector.__submodel_loaders__.keys()))

    _convert_object_ids = validator("id", pre=True, allow_reuse=True)(convert_from_object_id)


class SelectorResponse(SelectorBaseResponse):
    rel: Optional[str]


class SelectorRequest(BaseModel):
    name: Optional[str]
