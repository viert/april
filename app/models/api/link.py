from pydantic import BaseModel, validator
from typing import Optional, Dict, Any, List
from . import convert_from_object_id


class LinkBaseResponse(BaseModel):
    id: Optional[str]
    layout_id: Optional[str]
    source_id: Optional[str]
    source_type: Optional[str]
    target_id: Optional[str]
    target_type: Optional[str]
    meta: Optional[Dict[str, Any]]

    _convert_object_ids = validator(
        "id", "source_id", "target_id", "layout_id", pre=True, allow_reuse=True)(convert_from_object_id)


class LinkResponse(LinkBaseResponse):
    pass


class LinkableModelResponse(BaseModel):

    links: Optional[List[LinkBaseResponse]]

    @validator("links", each_item=True, pre=True)
    def convert_links(cls, v: Any) -> Any:
        if hasattr(v, "to_dict"):
            return LinkBaseResponse(**v.to_dict())
        return v
