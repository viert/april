from typing import Optional
from pydantic import BaseModel, validator
from . import convert_to_object_id, convert_from_object_id


class ComponentResponse(BaseModel):
    id: Optional[str]
    host_id: Optional[str]

    _convert_object_ids = validator("id", "host_id", pre=True, allow_reuse=True)(convert_from_object_id)


class CPUResponse(ComponentResponse):
    arch: Optional[str]
    name: Optional[str]
    cores: Optional[int]
    threads: Optional[int]
    family_name: Optional[str]
    family_number: Optional[int]
    manufacturer: Optional[str]
    model: Optional[str]
    cpu_id: Optional[str]
    speed: Optional[int]
    stepping: Optional[int]


class MemoryResponse(ComponentResponse):
    serial: Optional[str]
    capacity: Optional[int]
    description: Optional[str]
    manufacturer: Optional[str]
    model: Optional[str]
    speed: Optional[int]
    type: Optional[str]
