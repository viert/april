from typing import Optional
from pydantic import BaseModel, validator
from . import convert_from_object_id


class StateBaseResponse(BaseModel):
    id: Optional[str]
    name: Optional[str]

    _convert_object_ids = validator("id", pre=True, allow_reuse=True)(convert_from_object_id)


class StateResponse(StateBaseResponse):
    rel: Optional[str]


class StateRequest(BaseModel):
    name: Optional[str]
