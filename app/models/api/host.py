from typing import Optional, List, Dict, Any
from pydantic import BaseModel, validator, Field
from datetime import datetime
from .group import GroupBaseResponse
from .rack import RackBaseResponse
from .location import LocationBaseResponse
from .component import MemoryResponse, CPUResponse
from .selector import SelectorBaseResponse
from .layout import LayoutBaseResponse
from .link import LinkableModelResponse
from ..components import CPU, Memory
from . import convert_to_object_id, convert_from_object_id
from app.models.host import Interface


class HostBaseResponse(BaseModel):
    id: Optional[str]
    fqdn: Optional[str]
    description: Optional[str]
    uuid: Optional[str]
    serial: Optional[str]
    manufacturer: Optional[str]
    model: Optional[str]
    group_id: Optional[str]
    rack_id: Optional[str]
    state_id: Optional[str]
    os_image_id: Optional[str]
    vm_host_id: Optional[str]
    rack_position: Optional[int]
    local_tags: Optional[List[str]]
    local_attrs: Optional[Dict[str, Any]]
    location_id: Optional[str]
    location: Optional[LocationBaseResponse]
    aliases: Optional[List[str]]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    last_reported_at: Optional[datetime]
    interfaces: Optional[List[Interface]]
    vm_type: Optional[str]
    memory: Optional[int]
    vcpu: Optional[int]
    status: Optional[str]

    _convert_object_ids = validator(
        "id", "group_id", "rack_id", "location_id", "state_id", "os_image_id", "vm_host_id",
        pre=True, allow_reuse=True)(convert_from_object_id)


class HostResponse(HostBaseResponse, LinkableModelResponse):
    rel: Optional[str]

    rack: Optional[RackBaseResponse]
    rack_full_name: Optional[str]

    location: Optional[LocationBaseResponse]

    group: Optional[GroupBaseResponse]
    group_name: Optional[str]

    state: Optional[SelectorBaseResponse]
    state_name: Optional[str]

    os_image: Optional[SelectorBaseResponse]
    os_image_name: Optional[str]

    vm_host: Optional[HostBaseResponse]
    vm_host_fqdn: Optional[str]

    virtual_machines: Optional[List[HostBaseResponse]]

    tags: Optional[List[str]]
    attrs: Optional[Dict[str, Any]]

    components: Optional[List[CPUResponse | MemoryResponse]]

    layouts: Optional[List[LayoutBaseResponse]]

    @validator("group", pre=True)
    def convert_group(cls, v):
        if hasattr(v, "to_dict"):
            return GroupBaseResponse(**v.to_dict())
        return v

    @validator("rack", pre=True)
    def convert_rack(cls, v):
        if hasattr(v, "to_dict"):
            return RackBaseResponse(**v.to_dict())
        return v

    @validator("location", pre=True)
    def convert_location(cls, v):
        if hasattr(v, "to_dict"):
            return LocationBaseResponse(**v.to_dict())
        return v

    @validator("components", pre=True, each_item=True)
    def convert_components(cls, v):
        if isinstance(v, CPU):
            return CPUResponse(**v.to_dict())
        elif isinstance(v, Memory):
            return MemoryResponse(**v.to_dict())
        return v

    @validator("state", "os_image", pre=True)
    def convert_selectors(cls, v):
        if hasattr(v, "to_dict"):
            return SelectorBaseResponse(**v.to_dict())
        return v

    @validator("vm_host", pre=True)
    def convert_vm_host(cls, v):
        if hasattr(v, "to_dict"):
            return HostBaseResponse(**v.to_dict())
        return v


class HostUpdateRequest(BaseModel):
    fqdn: Optional[str]
    description: Optional[str]
    uuid: Optional[str]
    serial: Optional[str]
    rack_id: Optional[str]
    rack_position: Optional[int]
    group_id: Optional[str]
    state_id: Optional[str]
    os_image_id: Optional[str]
    vm_host_id: Optional[str]
    vm_type: Optional[str]
    vcpu: Optional[int]
    memory: Optional[int]
    local_tags: Optional[List[str]]
    local_attrs: Optional[Dict[str, Any]]
    aliases: Optional[List[str]]

    _convert_object_ids = validator(
        "rack_id", "group_id", "os_image_id", "state_id", "vm_host_id", allow_reuse=True)(convert_to_object_id)


class HostCreateRequest(BaseModel):
    fqdn: str
    description: Optional[str]
    rack_id: Optional[str]
    rack_position: Optional[int]
    group_id: Optional[str]
    state_id: Optional[str]
    os_image_id: Optional[str]
    vm_host_id: Optional[str]
    vm_type: Optional[str]
    vcpu: Optional[int]
    memory: Optional[int]
    local_tags: Optional[List[str]] = Field(default_factory=list)
    local_attrs: Optional[Dict[str, Any]] = Field(default_factory=dict)
    aliases: Optional[List[str]] = Field(default_factory=list)

    multiple: bool = False

    _convert_object_ids = validator(
        "group_id", "rack_id", "state_id", "os_image_id", "vm_host_id", allow_reuse=True)(convert_to_object_id)


class HostMoveGroupRequest(BaseModel):
    host_ids: List[str]
    group_id: Optional[str]

    _convert_object_id = validator("group_id", allow_reuse=True)(convert_to_object_id)
    _convert_object_id_list = validator("host_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class HostMoveGroupResponse(BaseModel):
    status: str
    hosts: List[HostBaseResponse]
    group: Optional[GroupBaseResponse]


class HostMoveRackRequestItem(BaseModel):
    host_id: str
    rack_id: Optional[str]
    rack_position: Optional[int]

    _convert_object_id = validator("host_id", "rack_id", allow_reuse=True)(convert_to_object_id)


class HostMoveRackRequest(BaseModel):
    host_rack_positions: List[HostMoveRackRequestItem]


class HostDeleteManyRequest(BaseModel):
    host_ids: List[str]
    _convert_object_id_list = validator("host_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class HostTagsRequest(BaseModel):
    tags: List[str]


class HostStateRequest(BaseModel):
    state_id: Optional[str]
    _convert_object_id = validator("state_id", allow_reuse=True)(convert_to_object_id)


class HostMassStateRequest(BaseModel):
    host_ids: List[str]
    state_id: Optional[str]
    _convert_object_id = validator("state_id", allow_reuse=True)(convert_to_object_id)
    _convert_object_id_list = validator("host_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class HostMassOsImageRequest(BaseModel):
    host_ids: List[str]
    os_image_id: Optional[str]
    _convert_object_id = validator("os_image_id", allow_reuse=True)(convert_to_object_id)
    _convert_object_id_list = validator("host_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class HostMassTagsRequest(HostTagsRequest):
    host_ids: List[str]
    overwrite: bool = False
    _convert_object_id_list = validator("host_ids", each_item=True, allow_reuse=True)(convert_to_object_id)


class HostMassAliasesRequest(BaseModel):
    aliases: List[str]
    host_ids: List[str]
    overwrite: bool = False
    _convert_object_id_list = validator("host_ids", each_item=True, allow_reuse=True)(convert_to_object_id)
