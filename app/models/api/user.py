from typing import Optional, List
from pydantic import BaseModel, validator
from datetime import datetime
from app.lib.rbac import Permissions
from .token import TokenBaseResponse
from .team import TeamBaseResponse
from .role import RoleBaseResponse
from .link import LinkableModelResponse
from .layout import LayoutBaseResponse
from . import convert_from_object_id, convert_to_object_id
from ..user import UserSettings


class UserBaseResponse(BaseModel):
    id: Optional[str]
    username: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    avatar_url: Optional[str]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    role_id: Optional[str]

    _convert_object_ids = validator("id", "role_id", pre=True, allow_reuse=True)(convert_from_object_id)


class AccountMeResponse(UserBaseResponse):
    settings: UserSettings
    role: RoleBaseResponse
    current_permissions: Optional[Permissions]
    tokens: Optional[List[TokenBaseResponse]]

    @validator("role", pre=True)
    def convert_role(cls, v):
        if hasattr(v, "to_dict"):
            return RoleBaseResponse(**v.to_dict())
        return v

    @validator("tokens", pre=True, each_item=True)
    def convert_tokens(cls, v):
        if hasattr(v, "to_dict"):
            return TokenBaseResponse(**v.to_dict())
        return v


class AccountMeUpdateRequest(BaseModel):
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    avatar_url: Optional[str]


class SettingsRequest(BaseModel):
    settings: UserSettings


class SetPasswordRequest(BaseModel):
    password: str
    password_confirm: str


class UserResponse(UserBaseResponse, LinkableModelResponse):
    rel: Optional[str]

    tokens: Optional[List[TokenBaseResponse]]
    role_name: Optional[str]
    teams_owned: Optional[List[TeamBaseResponse]]
    teams_member_of: Optional[List[TeamBaseResponse]]
    all_teams: Optional[List[TeamBaseResponse]]
    role: Optional[RoleBaseResponse]
    layouts: Optional[List[LayoutBaseResponse]]

    @validator("tokens", pre=True, each_item=True)
    def convert_tokens(cls, v):
        if hasattr(v, "to_dict"):
            return TokenBaseResponse(**v.to_dict())
        return v

    @validator("teams_owned", "teams_member_of", "all_teams", pre=True, each_item=True)
    def convert_teams(cls, v):
        if hasattr(v, "to_dict"):
            return TeamBaseResponse(**v.to_dict())
        return v

    @validator("role", pre=True)
    def convert_role(cls, v):
        if hasattr(v, "to_dict"):
            return RoleBaseResponse(**v.to_dict())
        return v


class UserRequest(BaseModel):
    username: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    avatar_url: Optional[str]
    password: Optional[str]
    password_confirm: Optional[str]
    role_id: Optional[str]

    _convert_object_ids = validator("role_id", allow_reuse=True)(convert_to_object_id)
