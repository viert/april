from bson import ObjectId
from typing import TYPE_CHECKING

from croydon.models.fields import ReferenceField, StringField, ListField, DictField
from croydon.db import ObjectsCursor
from croydon.decorators import api_field

from app.lib.rbac.rbac_model_mixin import RBACModel
from app.lib.rbac import PermissionsNamespace
from app.lib.rbac.layout import LayoutRelation
from ..timestamped_model import TimestampedModel
from ..team import Team

if TYPE_CHECKING:
    from .link import Link


class Layout(TimestampedModel, RBACModel):

    KEY_FIELD = "name"
    COLLECTION = "layouts"
    RBAC_NAMESPACE = PermissionsNamespace.LAYOUTS

    name = StringField(required=True)
    team_id: ReferenceField[Team] = ReferenceField(reference_model=Team, required=True)
    owner_ids: ListField[ObjectId] = ListField(default=list, rejected=True, restricted=True)
    node_positions = DictField(default=dict, required=True)
    
    @api_field
    async def team(self) -> Team:
        return await Team.get(self.team_id)

    def relation(self, user: "User") -> LayoutRelation:
        if user.id in self.owner_ids:
            return LayoutRelation.OWNED
        return LayoutRelation.OTHERS

    async def _before_save(self) -> None:
        await super()._before_save()
        if self.team_id != self._initial_state["team_id"]:
            team = await self.team()
            self.owner_ids = team.all_user_ids()

    @api_field
    def links(self) -> ObjectsCursor["Link"]:
        from .link import Link
        return Link.find({"layout_id": self.id})

    @api_field
    async def num_links(self) -> int:
        return await self.links().count()
