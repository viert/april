from typing import Type, Self
from bson import ObjectId

from croydon.models import StorableModel
from croydon.models.fields import StringField, DictField, ObjectIdField, ReferenceField, OnDestroy
from croydon.models.index import Index, IndexDirection, IndexKey
from croydon.errors import InputDataError
from croydon.db import ObjectsCursor

from .layout import Layout
from app.models.linkable_model import LinkableModel, TLinkableModel


class Link(StorableModel):

    COLLECTION = "links"

    INDEXES = [
        Index(keys=[
            IndexKey("source_id", IndexDirection.ASCENDING),
            IndexKey("source_type", IndexDirection.ASCENDING),
            IndexKey("target_id", IndexDirection.ASCENDING),
            IndexKey("target_type", IndexDirection.ASCENDING),
        ])
    ]

    source_id = ObjectIdField(required=True, index=True)
    source_type = StringField(required=True)
    target_id = ObjectIdField(required=True, index=True)
    target_type = StringField(required=True)
    layout_id: ReferenceField[Layout] = ReferenceField(reference_model=Layout, required=True,
                                                       on_destroy=OnDestroy.CASCADE)

    meta = DictField(required=True, default=dict)

    async def layout(self) -> Layout:
        return await Layout.get(self.layout_id)

    async def validate(self):
        await super().validate()
        if self.source_id == self.target_id:
            raise InputDataError("link can't connect an object with itself")
        source = await self.source()
        if source is None:
            raise InputDataError("source object not found")
        target = await self.target()
        if target is None:
            raise InputDataError("target object not found")

    @classmethod
    def find_by_source_id(cls, source_id: ObjectId) -> ObjectsCursor[Self]:
        return cls.find({"source_id": source_id})

    @classmethod
    def find_by_target_id(cls, target_id: ObjectId) -> ObjectsCursor[Self]:
        return cls.find({"target_id": target_id})

    @classmethod
    def find_by_object_id(cls, object_id: ObjectId) -> ObjectsCursor[Self]:
        return cls.find({"$or": [
            {"target_id": object_id},
            {"source_id": object_id},
        ]})

    @staticmethod
    def get_ctor(obj_type: str) -> Type[TLinkableModel]:
        from ..linkable_model import LinkableModel
        ctor = LinkableModel.CTOR_MAP.get(obj_type)
        if ctor is None:
            keys = list(LinkableModel.CTOR_MAP.keys())
            raise InputDataError(f"invalid object type {obj_type}, valid types are {keys}")
        return ctor

    async def source(self) -> [TLinkableModel]:
        ctor = self.get_ctor(self.source_type)
        return await ctor.get(self.source_id)

    async def target(self) -> [TLinkableModel]:
        ctor = self.get_ctor(self.target_type)
        return await ctor.get(self.target_id)
