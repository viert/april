from abc import ABC
from croydon.models import StorableModel
from croydon.models.fields import DatetimeField
from croydon.util import now


class TimestampedModel(StorableModel, ABC):

    created_at = DatetimeField(required=True, rejected=True, default=now)
    updated_at = DatetimeField(required=True, rejected=True, default=now)

    def touch(self) -> None:
        self.updated_at = now()

    async def _before_save(self) -> None:
        await super()._before_save()
        if not self.is_new:
            self.touch()
