from typing import Set, Dict, Any, List, Optional
from bson import ObjectId
from abc import ABC

from croydon.models.fields import StringField, ReferenceField, ListField, DictField, DatetimeField
from croydon.decorators import api_field
from croydon.util import now, uuid4_string

from app.lib import FQDN_EXPR
from app.lib.rbac.rbac_model_mixin import RBACModel

from .timestamped_model import TimestampedModel
from .group import Group
from .team import Team
from app.models.linkable_model import LinkableModel


class BaseHost(TimestampedModel, RBACModel, LinkableModel, ABC):

    fqdn = StringField(required=True, re_match=FQDN_EXPR)
    group_id: ReferenceField[Group] = ReferenceField(reference_model=Group)
    description = StringField(default="")
    local_tags: ListField[str] = ListField(required=True, default=list, index=True)
    local_attrs: ListField[str] = DictField(required=True, default=dict)
    uuid = StringField(required=True, unique=True, default=uuid4_string)
    last_reported_at = DatetimeField(required=False, default=None)

    # these fields are auto-assigned
    owner_ids: ListField[ObjectId] = ListField(rejected=True, restricted=True, default=list, index=True)

    def __hash__(self) -> int:
        return hash(f"{self.__class__.__name__}.{self.id}")

    async def _tags(self) -> Set[str]: ...

    async def _attrs(self) -> Dict[str, Any]: ...

    @api_field
    async def tags(self) -> List[str]:
        return list(await self._tags())

    @api_field
    async def attrs(self) -> Dict[str, Any]:
        return await self._attrs()

    @api_field
    async def group(self) -> Group:
        return await Group.get(self.group_id)

    @api_field
    async def group_name(self) -> Optional[str]:
        if self.group_id is None:
            return None
        group = await Group.cache_get(self.group_id)
        if group:
            return group.name
        return None

    async def team(self) -> Optional[Team]:
        group = await self.group()
        return group and await group.team()

    async def _auto_assign(self) -> None: ...

    async def _before_save(self) -> None:
        await super()._before_save()
        await self._auto_assign()

    async def _before_validation(self) -> None:
        await super()._before_validation()
        await self._unify_tags()

    @classmethod
    async def detach_many(cls, host_ids: List[ObjectId]):
        await cls.update_many(
            {"_id": {"$in": host_ids}},
            {
                "$set": {
                    "group_id": None,
                    "owner_ids": [],
                    "updated_at": now()
                }
            }
        )

    def add_local_tag(self, tag: str):
        if tag in self.local_tags:
            return
        self.local_tags.append(tag)

    def remove_local_tag(self, tag: str):
        if tag not in self.local_tags:
            return
        self.local_tags.remove(tag)

    async def _unify_tags(self) -> None:
        """
        This must be run in _before_validation callback.
        The method makes local_tags list behave like a set
        :return:
        """
        self.local_tags = list(set(self.local_tags))
