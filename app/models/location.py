import asyncio

from typing import Optional, Self, List, Dict, Any, TYPE_CHECKING
from bson import ObjectId
from time import time

from croydon import ctx
from croydon.models.fields import StringField, ObjectIdField
from croydon.decorators import api_field, save_required
from croydon.db import ObjectsCursor
from croydon.errors import ObjectHasReferences
from croydon.util import now

from app.lib import NAME_EXPR
from app.lib.rbac import PermissionsNamespace
from app.lib.rbac.location import LocationRelation
from app.lib.rbac.rbac_model_mixin import RBACModel
from .timestamped_model import TimestampedModel
from .tree_model import TreeModel
from .user import User
from app.models.linkable_model import LinkableModel

if TYPE_CHECKING:
    from .rack import Rack


class Location(TimestampedModel, TreeModel, LinkableModel, RBACModel):

    RBAC_NAMESPACE = PermissionsNamespace.LOCATIONS
    COLLECTION = "locations"
    KEY_FIELD = "full_name"
    LINK_MODEL_TYPE = "location"

    name = StringField(required=True, index=True, re_match=NAME_EXPR)
    description = StringField(required=True, default="")
    type = StringField(required=True, choices=["country", "city", "building", "room"], default="country")

    # root_id and full_name are auto-assigned, don't assign to it manually
    root_id = ObjectIdField(index=True, rejected=True)
    full_name = StringField(rejected=True, index=True)

    _assign_descendants_root: bool = False
    _assign_descendants_full_name: bool = False
    _assign_hosts_cache: bool = False

    def relation(self, user: "User") -> LocationRelation:
        return LocationRelation.ALL

    @api_field
    async def root(self) -> Optional[Self]:
        return await Location.get(self.root_id)

    @api_field
    def racks(self) -> ObjectsCursor["Rack"]:
        from .rack import Rack
        return Rack.find({"location_id": self.id})

    async def all_racks(self) -> ObjectsCursor["Rack"]:
        from .rack import Rack
        location_ids = await self.tree_ids()
        return Rack.find({"location_id": {"$in": location_ids}})

    async def all_rack_ids(self) -> List[ObjectId]:
        from .rack import Rack
        location_ids = await self.tree_ids()
        return await Rack.find_ids({"location_id": {"$in": location_ids}})

    @save_required
    async def find_root_id(self) -> ObjectId:
        if self.parent_id is None:
            return self.id
        parent = await self.parent()
        return await parent.find_root_id()

    async def find_full_name(self) -> str:
        if self.parent_id is None:
            return self.name
        parent = await self.parent()
        p_full_name = await parent.find_full_name()
        return f"{p_full_name}.{self.name}"

    async def assign_descendants_root(self):
        # recalculate all root_ids for all descendants and self
        root_id = await self.find_root_id()
        all_child_ids = await self.tree_ids()
        await Location.update_many(
            {"_id": {"$in": all_child_ids}},
            {
                "$set": {
                    "root_id": root_id,
                    "updated_at": now()
                }
            }
        )
        await self.reload()

    async def assign_descendants_full_name(self):
        all_locs = await self.all_children()
        all_locs.append(self)
        for loc in all_locs:
            loc.full_name = await loc.find_full_name()
            await loc.save(skip_callback=True)
        racks = await self.all_racks()
        async for rack in racks:
            await rack.save()

    async def assign_hosts_cache(self):
        """
        sets datacented_name field for directly (through direct racks) connected hosts
        :return:
        """
        from .host import Host
        from .rack import Rack
        rack_ids = await Rack.find_ids({"location_id": self.id})
        host_ids = await Host.find_ids({"rack_id": {"$in": rack_ids}})
        await Host.update_many(
            {"_id": {"$in": host_ids}},
            {
                "$set": {
                    "location_id": self.id,
                    "updated_at": now()
                }
            }
        )

    async def _before_save(self) -> None:
        await super()._before_save()
        self.full_name = await self.find_full_name()
        self._assign_descendants_root = self.parent_id != self._initial_state["parent_id"]
        self._assign_hosts_cache = self.name != self._initial_state["name"]
        self._assign_descendants_full_name = self._assign_descendants_root or self._assign_hosts_cache

    async def _after_save(self, is_new: bool) -> None:
        await super()._after_save(is_new)
        if self._assign_descendants_root:
            await self.assign_descendants_root()
            self._assign_descendants_root = False
        if self._assign_descendants_full_name:
            await self.assign_descendants_full_name()
            self._assign_descendants_full_name = False
        if self._assign_hosts_cache:
            await self.assign_hosts_cache()
            self._assign_hosts_cache = False

    async def drop_metadata_cache(self, drop_tags: bool, drop_attrs: bool,
                                  child_ids: Optional[List[ObjectId]] = None):
        if child_ids is None:
            child_ids = await self.tree_ids()
        await super().drop_metadata_cache(drop_tags, drop_attrs, child_ids)

        from .rack import Rack
        from .host import Host

        t1 = time()
        rack_ids = await Rack.find_ids({"location_id": {"$in": child_ids}})
        host_ids = await Host.find_ids({"rack_id": {"$in": rack_ids}})

        cache_keys = []
        if drop_tags:
            ctx.log.debug(f"invalidating tags cache for all racks and hosts in location {self.name}")
            cache_keys += [f"hosts.{host_id}._tags" for host_id in host_ids]
            cache_keys += [f"racks.{rack_id}._tags" for rack_id in rack_ids]

        if drop_attrs:
            ctx.log.debug(f"invalidating attrs cache for all racks and hosts in location {self.name}")
            cache_keys += [f"hosts.{host_id}._attrs" for host_id in host_ids]
            cache_keys += [f"racks.{rack_id}._attrs" for rack_id in rack_ids]

        tasks = [self._invalidate(cache_key) for cache_key in cache_keys]
        await asyncio.gather(*tasks)
        t2 = time()

        items = []
        if drop_tags:
            items.append("tags")
        if drop_attrs:
            items.append("attrs")
        if items:
            ctx.log.debug(f"dropped {'/'.join(items)} cache ({len(cache_keys)} keys) in %.3f sec", t2-t1)

    @classmethod
    async def full_tree(cls, parent_id: Optional[ObjectId] = None, *, fields: Optional[List[str]] = None) -> List[Dict[str, Any]]:
        query = {"parent_id": parent_id}
        locations = cls.find(query)
        dcts = []
        for loc in await locations.all():
            dct = await loc.to_dict_ext(fields)
            dct["children"] = await cls.full_tree(loc.id, fields=fields)
            dcts.append(dct)

        dcts.sort(key=lambda item: item["name"])
        return dcts

    @classmethod
    def _default_get_query(cls, expr: str) -> Dict[str, Any]:
        return {"$or": [
            {"name": expr},
            {"full_name": expr}
        ]}
