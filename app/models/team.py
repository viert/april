from bson import ObjectId
from typing import Optional, List, TYPE_CHECKING

from croydon.models.fields import StringField, ReferenceField, ListField
from croydon.util import now
from croydon.db import ObjectsCursor
from croydon.decorators import api_field, model_cached_method
from croydon.errors import ObjectHasReferences

from app.lib.rbac import PermissionsNamespace
from app.lib.rbac.team import TeamRelation
from app.lib.rbac.rbac_model_mixin import RBACModel
from app.lib import NAME_EXPR
from .user import User
from .timestamped_model import TimestampedModel
from app.models.linkable_model import LinkableModel

if TYPE_CHECKING:
    from .group import Group


class Team(TimestampedModel, LinkableModel, RBACModel):
    RBAC_NAMESPACE = PermissionsNamespace.TEAMS
    COLLECTION = "teams"
    KEY_FIELD = "name"
    LINK_MODEL_TYPE = "team"

    name = StringField(required=True, unique=True, re_match=NAME_EXPR)
    description = StringField(required=True, default="")
    email = StringField(required=True, default="")
    owner_id: ReferenceField[User] = ReferenceField(
        required=True, rejected=True, reference_model=User)
    member_ids: ListField[ObjectId] = ListField(
        required=True, default=list, rejected=True, index=True)

    _assign_descendants_cache: bool = False

    @api_field
    async def owner(self) -> Optional["User"]:
        from .user import User
        return await User.get(self.owner_id)

    @api_field
    async def owner_username(self) -> Optional[str]:
        owner: User = await self.owner()
        if owner is not None:
            return owner.username

    @api_field
    def members(self) -> ObjectsCursor["User"]:
        from .user import User
        return User.find({"_id": {"$in": self.member_ids}})

    @api_field
    async def member_usernames(self) -> List[str]:
        members = await self.members().all()
        return [user.username for user in members]

    @model_cached_method
    async def all_usernames(self) -> List[str]:
        member_usernames = await self.member_usernames()
        owner_username = await self.owner_username()
        return [owner_username, *member_usernames]

    def all_user_ids(self):
        return self.member_ids + [self.owner_id]

    def groups(self) -> ObjectsCursor["Group"]:
        from .group import Group
        return Group.find({"team_id": self.id})

    def relation(self, user: "User") -> TeamRelation:
        if user.id == self.owner_id:
            return TeamRelation.OWNED
        elif user.id in self.member_ids:
            return TeamRelation.MEMBER
        else:
            return TeamRelation.OTHERS

    async def _before_save(self) -> None:
        await super()._before_save()
        if not self.is_new:
            self._assign_descendants_cache = self.owner_id != self._initial_state.get("owner_id") or \
                                             self.member_ids != self._initial_state.get("member_ids") or \
                                             self.name != self._initial_state.get("name")

    async def assign_descendants_cache(self) -> None:
        # This method is called from a corresponding task asynchronously

        from .group import Group
        from .host import Host
        from .links import Layout
        from .virtual_machine import VirtualMachine
        from .db_instance import DBInstance

        all_user_ids = self.all_user_ids()
        await Group.update_many(
            {"team_id": self.id},
            {
                "$set": {
                    "team_name": self.name,
                    "owner_ids": all_user_ids,
                    "updated_at": now(),
                }
            }
        )

        group_ids = await Group.find_ids({"team_id": self.id})
        layout_ids = await Layout.find_ids({"team_id": self.id})
        all_host_ids = await Host.find_ids({"group_id": {"$in": group_ids}})
        all_vm_ids = await VirtualMachine.find_ids({"group_id": {"$in": group_ids}})

        await Host.update_many(
            {"_id": {"$in": all_host_ids}},
            {
                "$set": {
                    "owner_ids": all_user_ids,
                    "updated_at": now(),
                }
            }
        )

        await DBInstance.update_many(
            {"team_id": self.id},
            {
                "$set": {
                    "owner_ids": all_user_ids,
                    "updated_at": now(),
                }
            }
        )

        await Layout.update_many(
            {"_id": {"$in": layout_ids}},
            {
                "$set": {
                    "owner_ids": all_user_ids,
                    "updated_at": now(),
                }
            }
        )

        await VirtualMachine.update_many(
            {"_id": {"$in": all_vm_ids}},
            {
                "$set": {
                    "owner_ids": all_user_ids,
                    "updated_at": now(),
                }
            }
        )

    async def _after_save(self, is_new: bool) -> None:
        await super()._after_save(is_new)
        if not is_new and self._assign_descendants_cache:
            from app.tasks.team_cache_task import TeamCacheTask
            t = TeamCacheTask(data={"team_id": str(self.id)})
            await t.publish()
            self._assign_descendants_cache = False

    async def add_member(self, user: User) -> None:
        if user.id in self.member_ids or user.id == self.owner_id:
            return
        self.member_ids.append(user.id)
        await self.save()

    async def remove_member(self, user: User) -> None:
        self.member_ids.remove(user.id)
        await self.save()

    async def _before_delete(self) -> None:
        await super()._before_delete()
        member_count = await self.members().count()
        if member_count > 0:
            raise ObjectHasReferences(f"team has {member_count} users")
