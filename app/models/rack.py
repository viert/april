from typing import Optional, Dict, Any, Set, List, TYPE_CHECKING
from itertools import islice

from croydon.models.fields import ReferenceField, StringField, IntField, ListField, DictField
from croydon.decorators import api_field, model_cached_method
from croydon.errors import ObjectHasReferences, Forbidden
from croydon.db import ObjectsCursor
from croydon.util import now
from croydon import ctx

from app.lib.rbac.rbac_model_mixin import RBACModel
from app.lib.rbac.rack import RackRelation
from app.lib.rbac import PermissionsNamespace
from app.lib.attrs import merge

from .location import Location
from .timestamped_model import TimestampedModel
from app.models.linkable_model import LinkableModel

if TYPE_CHECKING:
    from .host import Host


class InsufficientUnitSpace(Forbidden):
    pass


class Rack(TimestampedModel, LinkableModel, RBACModel):

    RBAC_NAMESPACE = PermissionsNamespace.RACKS
    COLLECTION = "racks"
    KEY_FIELD = "full_name"
    LINK_MODEL_TYPE = "rack"

    name = StringField(required=True)
    location_id: ReferenceField[Location] = ReferenceField(reference_model=Location, required=True)
    num_units = IntField(required=True, min_value=1, max_value=100)
    local_tags: ListField[str] = ListField(required=True, default=list, index=True)
    local_attrs: ListField[str] = DictField(required=True, default=dict)

    # this is an automatically assigned field
    full_name = StringField(rejected=True, unique=True)

    _update_hosts: bool = False
    _invalidate_tags_cache: bool = False
    _invalidate_attrs_cache: bool = False

    def relation(self, user: "User") -> RackRelation:
        return RackRelation.ALL

    @api_field
    async def location(self) -> Optional[Location]:
        return await Location.cache_get(self.location_id)

    @api_field
    def hosts(self) -> ObjectsCursor["Host"]:
        from .host import Host
        return Host.find({"rack_id": self.id})

    async def _before_save(self) -> None:
        await super()._before_save()
        if not self.is_new:
            # if local tags have changed, drop tags cache
            if self.local_tags != self._initial_state["local_tags"]:
                self._invalidate_tags_cache = True
            # if local attrs have changed, drop tags cache
            if self.local_attrs != self._initial_state["local_attrs"]:
                self._invalidate_attrs_cache = True
            if self.location_id != self._initial_state["location_id"]:
                self._update_hosts = True
                self._invalidate_tags_cache = True
                self._invalidate_attrs_cache = True
        location = await self.location()
        self.full_name = f"{location.full_name}.{self.name}"

    async def _after_save(self, is_new: bool) -> None:
        await super()._after_save(is_new)
        if self._update_hosts:
            await self.update_cache()
            self._update_hosts = False
        if self._invalidate_tags_cache or self._invalidate_attrs_cache:
            await self.drop_metadata_cache()

    async def drop_metadata_cache(self):
        from .host import Host

        cache_keys = []
        host_ids = await Host.find_ids({"rack_id": self.id})

        if self._invalidate_tags_cache:
            ctx.log.debug(f"invalidating tags cache for the rack {self.name} and its hosts")
            cache_keys += [f"hosts.{host_id}._tags" for host_id in host_ids]
            cache_keys.append(f"racks.{self.id}._tags")
            self._invalidate_tags_cache = False

        if self._invalidate_attrs_cache:
            ctx.log.debug(f"invalidating attrs cache for the rack {self.name} and its hosts")
            cache_keys += [f"hosts.{host_id}._attrs" for host_id in host_ids]
            cache_keys.append(f"racks.{self.id}._attrs")
            self._invalidate_attrs_cache = False

    async def update_cache(self):
        from .host import Host
        await Host.update_many(
            {"rack_id": self.id},
            {"$set": {
                "location_id": self.location_id,
                "updated_at": now()
            }}
        )

    @model_cached_method
    async def _tags(self) -> Set[str]:
        if self.location_id is None:
            return set(self.local_tags)
        dc = await self.location()
        dc_tags = set(await dc.tags())
        tags = dc_tags.union(self.local_tags)
        return tags

    @model_cached_method
    async def _attrs(self) -> Dict[str, Any]:
        if self.location_id is None:
            return set(self.local_tags)
        dc = await self.location()
        dc_attrs = await dc.attrs()
        attrs = merge(dc_attrs, self.local_attrs)
        return attrs

    @api_field
    async def tags(self) -> List[str]:
        return list(await self._tags())

    @api_field
    async def attrs(self) -> Dict[str, Any]:
        return await self._attrs()

    async def available_units(self, *, count: int = -1, except_units: Optional[Set[int]] = None) -> List[int]:
        hosts = await self.hosts().all()
        occupied_units = {host.rack_position for host in hosts}
        if except_units:
            occupied_units = occupied_units.union(except_units)
        available = filter(lambda n: n not in occupied_units, range(1, self.num_units+1))
        units = list(islice(available, count))
        if count > 0 and len(units) < count:
            raise InsufficientUnitSpace(f"rack does not have {count} empty units")
        return units

    def add_local_tag(self, tag: str):
        if tag in self.local_tags:
            return
        self.local_tags.append(tag)

    def remove_local_tag(self, tag: str):
        if tag not in self.local_tags:
            return
        self.local_tags.remove(tag)
