import asyncio

import bcrypt
from typing import TYPE_CHECKING, Optional, Dict, Any
from pydantic import BaseModel, Field

from croydon import ctx
from croydon.decorators import api_field
from croydon.models.fields import StringField, ReferenceField, DictField
from croydon.db import ObjectsCursor

from app.lib.rbac import PermissionsNamespace
from app.lib.rbac.user import UserRelation
from app.lib.rbac.rbac_model_mixin import RBACModel
from .role import Role
from .timestamped_model import TimestampedModel
from app.models.linkable_model import LinkableModel

if TYPE_CHECKING:
    from .token import Token
    from .team import Team


class UserSettings(BaseModel):
    documents_per_page: int = Field(
        default_factory=lambda: ctx.cfg.general.documents_per_page,
        ge=1,
        le=200,
    )
    mine_only: bool = False


class User(TimestampedModel, LinkableModel, RBACModel):

    COLLECTION = "users"
    KEY_FIELD = "username"
    RBAC_NAMESPACE = PermissionsNamespace.USERS
    LINK_MODEL_TYPE = "user"

    username = StringField(required=True, unique=True)
    first_name = StringField(default="")
    last_name = StringField(default="")
    email = StringField(default="", required=True, unique=True)
    password_hash = StringField(default="-", rejected=True, restricted=True)
    avatar_url = StringField(default="")
    role_id: ReferenceField[Role] = ReferenceField(
        reference_model=Role,
        required=True,
        rejected=True,
    )
    settings = DictField(required=True, rejected=True, restricted=True, default=lambda: UserSettings().dict())

    _recalculate_cache: bool = False

    def set_password(self, password: str) -> None:
        salt = bcrypt.gensalt()
        self.password_hash = bcrypt.hashpw(password.encode("utf-8"), salt).decode("utf-8")

    def check_password(self, password: str) -> bool:
        try:
            return bcrypt.checkpw(
                password.encode("utf-8"),
                self.password_hash.encode("utf-8")
            )
        except ValueError as e:
            ctx.log.error(e)
            # password not set leads to bcrypt raising ValueError("invalid salt")
            return False

    async def create_token(self,
                           *,
                           description: Optional[str] = None,
                           permissions: Optional[Dict[str, Any]] = None) -> "Token":
        from .token import Token
        if permissions is None:
            role = await self.role()
            permissions = role.stored_permissions
        t = Token({"user_id": self.id, "description": description, "stored_permissions": permissions})
        await t.save()
        return t

    @api_field
    def tokens(self) -> ObjectsCursor["Token"]:
        from .token import Token
        return Token.find({"user_id": self.id})

    async def get_valid_token(self) -> "Token":
        async for token in self.tokens():
            if not token.is_expired():
                return token
        return await self.create_token()

    async def revoke_all_tokens(self) -> None:
        from .token import Token
        await Token.destroy_many({"user_id": self.id})

    def relation(self, user: "User") -> UserRelation:
        if user.id == self.id:
            return UserRelation.MYSELF
        return UserRelation.OTHERS

    @api_field
    async def role(self) -> Optional[Role]:
        return await Role.cache_get(self.role_id)

    @property
    def user_settings(self) -> UserSettings:
        return UserSettings(**self.settings)

    @api_field
    async def role_name(self) -> Optional[str]:
        role = await self.role()
        if role:
            return role.name
        return None

    @api_field
    def teams_owned(self) -> ObjectsCursor["Team"]:
        from .team import Team
        return Team.find({"owner_id": self.id})

    @api_field
    def teams_member_of(self) -> ObjectsCursor["Team"]:
        from .team import Team
        return Team.find({"member_ids": self.id})

    @api_field
    def all_teams(self) -> ObjectsCursor["Team"]:
        from .team import Team
        return Team.find({"$or": [
            {"owner_id": self.id},
            {"member_ids": self.id}
        ]})

    async def _before_save(self) -> None:
        await super()._before_save()
        if not self.is_new:
            if self.username != self._initial_state["username"]:
                self._recalculate_cache = True

    async def _after_save(self, is_new: bool) -> None:
        await super()._after_save(is_new)
        if self._recalculate_cache:
            await self.recalculate_team_cache()
            self._recalculate_cache = False

    async def _before_delete(self) -> None:
        await super()._before_delete()
        async for team in self.teams_member_of():
            await team.remove_member(self)

    async def recalculate_team_cache(self):
        teams = await self.all_teams().all()
        tasks = [team.assign_descendants_cache() for team in teams]
        await asyncio.gather(*tasks)
