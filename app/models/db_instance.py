from bson import ObjectId
from typing import Optional, TYPE_CHECKING
from croydon.models.fields import ReferenceField, StringField, IntField, ListField
from croydon.decorators import api_field
from croydon.errors import InputDataError

from app.lib.rbac.rbac_model_mixin import RBACModel
from app.lib.rbac.db import DBInstanceRelation
from app.lib.rbac import PermissionsNamespace

from .linkable_model import LinkableModel
from .timestamped_model import TimestampedModel
from .host import Host
from .team import Team

if TYPE_CHECKING:
    from .user import User


DB_TYPES = [
    "mysql",
    "postgres",
    "mssql",
    "mongodb",
    "clickhouse"
]


class DBInstance(TimestampedModel, LinkableModel, RBACModel):

    LINK_MODEL_TYPE = "db_instance"
    KEY_FIELD = "name"
    RBAC_NAMESPACE = PermissionsNamespace.DATABASE_INSTANCES

    name = StringField(required=True)
    host_id: ReferenceField[Host] = ReferenceField(reference_model=Host)
    team_id: ReferenceField[Team] = ReferenceField(reference_model=Team)
    db_type = StringField(default="mysql", choices=DB_TYPES)
    port = IntField()
    path = StringField(default="")

    # auto-assigned fields
    owner_ids: ListField[ObjectId] = ListField(rejected=True, restricted=True, default=list, index=True)

    def relation(self, user: "User") -> DBInstanceRelation:
        if self.team_id is None:
            return DBInstanceRelation.UNOWNED
        if user.id in self.owner_ids:
            return DBInstanceRelation.OWNED
        return DBInstanceRelation.OTHERS

    @api_field
    async def team(self) -> Optional[Team]:
        return await Team.get(self.team_id)

    @api_field
    async def host(self) -> Optional[Host]:
        return await Host.get(self.host_id)

    async def _before_save(self) -> None:
        await super()._before_save()
        team = await self.team()
        if team:
            self.owner_ids = team.all_user_ids()

    async def validate(self) -> None:
        await super().validate()
        host = await self.host()
        if host is None:
            raise InputDataError("db host not found")
