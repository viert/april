from typing import Optional
from datetime import timedelta, datetime
from pydantic import ValidationError as PydanticValidationError

from croydon import ctx
from croydon.models.fields import StringField, DatetimeField, ReferenceField, DictField, BoolField, OnDestroy
from croydon.models.index import Index, IndexDirection, IndexKey
from croydon.util import now, uuid4_string
from croydon.errors import ValidationError

from app.lib.rbac import Permissions
from .user import User
from .timestamped_model import TimestampedModel


class Token(TimestampedModel):

    COLLECTION = "tokens"
    KEY_FIELD = "token"
    INDEXES = [
        Index(
            keys=[
                IndexKey("user_id", IndexDirection.ASCENDING),
                IndexKey("token_type", IndexDirection.ASCENDING),
            ]
        )
    ]

    DEFAULT_TTL = timedelta(days=90)

    token_type = StringField(default="auth", required=True, rejected=True)
    token = StringField(default=uuid4_string, required=True, rejected=True, unique=True)
    expires_at = DatetimeField(default=lambda: Token.calculate_expires_at())
    description = StringField(default="")
    user_id: ReferenceField[User] = ReferenceField(
        reference_model=User,
        required=True,
        rejected=True,
        on_destroy=OnDestroy.CASCADE
    )
    stored_permissions = DictField(required=True)
    auto_prolonged = BoolField(default=True)

    async def user(self) -> Optional[User]:
        return await User.get(self.user_id)

    def is_expired(self) -> bool:
        return now() > self.expires_at

    def permissions(self) -> Permissions:
        return Permissions(**self.stored_permissions)

    async def validate(self) -> None:
        await super().validate()
        try:
            _ = self.permissions()
        except PydanticValidationError as e:
            raise ValidationError(f"stored_permissions are invalid: {e}")

    @classmethod
    def calculate_expires_at(cls) -> datetime:
        return now() + cls.DEFAULT_TTL

    async def auto_prolong(self):
        if not self.auto_prolonged:
            return
        ttl = self.expires_at - now()
        if ttl < timedelta(days=7):
            self.expires_at = self.calculate_expires_at()
            await self.save()
            ctx.log.debug(f"token {self.token} auto-prolonged")
