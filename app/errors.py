from croydon.errors import IntegrityError
from fastapi import HTTPException
from app.lib.oauth.google import get_oauth_authorize_url


class ChildExists(IntegrityError):
    def __init__(self):
        super().__init__("child already exists")


class ChildDoesNotExist(IntegrityError):
    def __init__(self):
        super().__init__("child does not exists")


class CyclicChain(IntegrityError):
    pass


class AuthenticationError(HTTPException):

    def __init__(self, msg: str = "you must be authenticated first"):

        detail = {"message": msg}
        auth_url = get_oauth_authorize_url()
        if auth_url:
            detail["oauth"] = {"url": auth_url}

        super().__init__(status_code=401, detail=detail)
