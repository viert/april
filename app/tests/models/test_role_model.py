from ..mongo_mock_test import MongoMockTest
from app.models import Role
from app.models.api.role import RoleResponse
from app.lib.rbac import PermissionsNamespace


class TestRoleModel(MongoMockTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await Role.destroy_all()

    async def test_vm_response_model(self):
        self.assertCountEqual(Role.exposed_fields(), RoleResponse.__fields__.keys())

    async def test_basics(self):
        self.assertEqual(Role.collection, "roles")
        self.assertEqual(Role.RBAC_NAMESPACE, PermissionsNamespace.ROLES)
        self.assertEqual(Role.KEY_FIELD, "name")
