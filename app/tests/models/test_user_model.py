from ..mongo_mock_test import MongoMockTest
from croydon.errors import ValidationError, NotFound, ObjectSaveRequired, IntegrityError, ObjectHasReferences
from app.models import User, Role, Team, Group
from pymongo.errors import DuplicateKeyError


class TestUserModel(MongoMockTest):

    user: User

    async def asyncSetUp(self) -> None:
        await Group.destroy_all()
        await Team.destroy_all()
        await User.destroy_all()
        await User.ensure_indexes()

        role = await Role.default()
        self.user = User.create(username="u", email="u@example.com", role_id=role.id)
        await self.user.save()
        self.tc1.reset()

    async def test_basics(self):
        self.assertEqual(User.collection, "users")

    async def test_invalid(self):
        u = User.create(username="test_user", email="test_user@example.com")
        with self.assertRaises(ValidationError):
            await u.save()

        u = User(self.user.to_dict())
        with self.assertRaises(DuplicateKeyError):
            await u.save()

    async def test_set_password(self):
        self.user.set_password("password1")
        self.assertTrue(self.user.check_password("password1"))
        self.user.set_password("password2")
        self.assertTrue(self.user.check_password("password2"))
        self.assertFalse(self.user.check_password("password1"))

    async def test_tokens(self):
        tc = await self.user.tokens().count()
        self.assertEqual(tc, 0)
        await self.user.create_token()
        tc = await self.user.tokens().count()
        self.assertEqual(tc, 1)
        await self.user.revoke_all_tokens()
        tc = await self.user.tokens().count()
        self.assertEqual(tc, 0)

        t = await self.user.get_valid_token()
        tc = await self.user.tokens().count()
        self.assertEqual(tc, 1)
        t2 = await self.user.get_valid_token()
        self.assertEqual(t, t2)

    async def test_team_cache(self):
        t = Team.create(name="t1", owner_id=self.user.id)
        await t.save()

        g = Group.create(name="g1", team_id=t.id)
        await g.save()

        self.assertEqual(g.owner_ids, [self.user.id])

        self.user.username = "new_username"
        await self.user.save()
        await g.reload()
        self.assertEqual(g.owner_ids, [self.user.id])

    async def test_computed_fields(self):
        role = await Role.default()
        u = User.create(username="test_user", email="test_user@example.com", role_id=role.id)
        await u.save()

        t1 = Team.create(name="owned", owner_id=u.id)
        await t1.save()
        t2 = Team.create(name="member", owner_id=self.user.id, member_ids=[u.id])
        await t2.save()

        teams_owned = await u.teams_owned().all()
        teams_member_of = await u.teams_member_of().all()
        all_teams = await u.all_teams().all()

        self.assertCountEqual(["owned"], [t.name for t in teams_owned])
        self.assertCountEqual(["member"], [t.name for t in teams_member_of])
        self.assertCountEqual(["owned", "member"], [t.name for t in all_teams])

    async def test_response_fields(self):
        from app.models.api.user import UserResponse

        response_fields = list(UserResponse.__fields__.keys())
        self.assertCountEqual(User.exposed_fields(), response_fields)
