from ..mongo_mock_test import MongoMockTest
from app.models import Team, Group, Host, User, Role, Rack, Location, DBInstance
from app.models.api.host import HostResponse
from app.lib.rbac import PermissionsNamespace


class TestHostModel(MongoMockTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await DBInstance.destroy_all()
        await Host.destroy_all()
        await Group.destroy_all()
        await Team.destroy_all()
        await User.destroy_all()
        await Rack.destroy_all()
        await Location.destroy_all()

    async def test_host_response_model(self):
        self.assertCountEqual(Host.exposed_fields(), HostResponse.__fields__.keys())

    async def test_basics(self):
        self.assertEqual(Host.collection, "hosts")
        self.assertEqual(Host.RBAC_NAMESPACE, PermissionsNamespace.HOSTS)
        self.assertEqual(Host.KEY_FIELD, "fqdn")

    async def test_cached_data(self):
        h = Host.create(fqdn="host1", local_tags=["host_tag"], local_attrs={"host_attr": "host_value"})
        await h.save()

        r = await Role.default()
        u = User.create(username="user", email="user@example.com", role_id=r.id)
        await u.save()

        t = Team.create(name="team", owner_id=u.id)
        await t.save()

        g = Group.create(name="group", team_id=t.id, local_tags=["group_tag"], local_attrs={"group_attr": "group_value"})
        await g.save()

        self.assertCountEqual(h.owner_ids, [])
        self.assertCountEqual(await h.tags(), ["host_tag"])
        self.assertDictEqual(await h.attrs(), {"host_attr": "host_value"})

        h.group_id = g.id
        await h.save()

        self.assertCountEqual(h.owner_ids, [u.id])
        self.assertCountEqual(await h.tags(), ["host_tag", "group_tag"])
        self.assertDictEqual(await h.attrs(), {"host_attr": "host_value", "group_attr": "group_value"})

        dc = Location.create(name="dc", local_tags=["dc_tag"], local_attrs={"dc_attr": "dc_value"})
        await dc.save()

        rack = Rack.create(
            name="1",
            local_tags=["rack_tag"],
            local_attrs={"rack_attr": "rack_value"},
            location_id=dc.id,
            num_units=50,
        )
        await rack.save()

        h.rack_id = rack.id
        h.rack_position = 1
        await h.save()

        self.assertCountEqual(h.owner_ids, [u.id])
        self.assertCountEqual(await h.tags(), ["host_tag", "group_tag", "rack_tag", "dc_tag"])
        self.assertDictEqual(await h.attrs(),
                             {
                                 "host_attr": "host_value",
                                 "group_attr": "group_value",
                                 "dc_attr": "dc_value",
                                 "rack_attr": "rack_value",
                             })

        h.group_id = None
        await h.save()
        self.assertCountEqual(h.owner_ids, [])
        self.assertCountEqual(await h.tags(), ["host_tag", "rack_tag", "dc_tag"])
        self.assertDictEqual(await h.attrs(),
                             {
                                 "host_attr": "host_value",
                                 "dc_attr": "dc_value",
                                 "rack_attr": "rack_value",
                             })

        h.rack_id = None
        h.rack_position = None
        await h.save()

        self.assertCountEqual(h.owner_ids, [])
        self.assertCountEqual(await h.tags(), ["host_tag"])
        self.assertDictEqual(await h.attrs(), {"host_attr": "host_value"})

    async def test_get_variations(self):
        h = Host.create(fqdn="host1", aliases=["host1.io"], serial="serial")
        await h.save()

        self.assertIsNone(await Host.get("random"))

        h1 = await Host.get(h.fqdn)
        self.assertEqual(h.id, h1.id)

        h1 = await Host.get(h.aliases[0])
        self.assertEqual(h.id, h1.id)

        h1 = await Host.get(h.serial)
        self.assertEqual(h.id, h1.id)

    async def test_absorb(self):
        h = Host.create(fqdn="host1", local_tags=["1", "2"], local_attrs={"host": "to_absorb", "foo": "bar"})
        await h.save()

        db = DBInstance.create(name="db", host_id=h.id, db_type="mysql", port=3306)
        await db.save()

        h1 = Host.create(fqdn="superhost", local_tags=["1", "3"], local_attrs={"blah": "minor", "host": "main"})
        await h1.save()
        await h1.absorb(h)

        self.assertIsNone(h.id)

        await db.reload()
        self.assertEqual(db.host_id, h1.id)
        self.assertCountEqual(h1.local_tags, ["1", "2", "3"])
        self.assertDictEqual(h1.local_attrs, {
            "host": "main",
            "blah": "minor",
            "foo": "bar",
        })
        self.assertCountEqual(h1.aliases, [h.fqdn])
