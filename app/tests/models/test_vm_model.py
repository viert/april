from ..mongo_mock_test import MongoMockTest
from app.models import Team, Group, VirtualMachine, Host, User, Role, Rack, Location
from app.lib.rbac import PermissionsNamespace


class TestVirtualMachineModel(MongoMockTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await VirtualMachine.destroy_all()
        await Host.destroy_all()
        await Group.destroy_all()
        await Team.destroy_all()
        await User.destroy_all()
        await Rack.destroy_all()
        await Location.destroy_all()

    async def test_basics(self):
        self.assertEqual(VirtualMachine.collection, "virtual_machines")
        self.assertEqual(VirtualMachine.RBAC_NAMESPACE, PermissionsNamespace.VIRTUAL_MACHINES)
        self.assertEqual(VirtualMachine.KEY_FIELD, "fqdn")

    async def test_cached_data(self):
        h = Host.create(fqdn="host1", local_tags=["host_tag"], local_attrs={"host_attr": "host_value"})
        await h.save()

        v = VirtualMachine.create(fqdn="vm", local_tags=["vm_tag"], local_attrs={"vm_attr": "vm_value"}, host_id=h.id)
        await v.save()

        r = await Role.default()
        u = User.create(username="user", email="user@example.com", role_id=r.id)
        await u.save()

        t = Team.create(name="team", owner_id=u.id)
        await t.save()

        g = Group.create(name="group", team_id=t.id, local_tags=["group_tag"], local_attrs={"group_attr": "group_value"})
        await g.save()

        self.assertCountEqual(v.owner_ids, [])
        self.assertCountEqual(await v.tags(), ["vm_tag"])
        self.assertDictEqual(await v.attrs(), {"vm_attr": "vm_value"})

        h.group_id = g.id
        await h.save()

        # host's metadata is detached from its vms and should not affect their metadata
        self.assertCountEqual(v.owner_ids, [])
        self.assertCountEqual(await v.tags(), ["vm_tag"])
        self.assertDictEqual(await v.attrs(), {"vm_attr": "vm_value"})

        v.group_id = g.id
        await v.save()

        self.assertCountEqual(v.owner_ids, [u.id])
        self.assertCountEqual(await v.tags(), ["vm_tag", "group_tag"])
        self.assertDictEqual(await v.attrs(), {"vm_attr": "vm_value", "group_attr": "group_value"})

        dc = Location.create(name="dc", local_tags=["dc_tag"], local_attrs={"dc_attr": "dc_value"})
        await dc.save()

        rack = Rack.create(
            name="1",
            local_tags=["rack_tag"],
            local_attrs={"rack_attr": "rack_value"},
            location_id=dc.id,
            num_units=50,
        )
        await rack.save()

        h.rack_id = rack.id
        h.rack_position = 1
        await h.save()

        # vm physical location is irrelevant and should not affect vm's metadata
        self.assertCountEqual(v.owner_ids, [u.id])
        self.assertCountEqual(await v.tags(), ["vm_tag", "group_tag"])
        self.assertDictEqual(await v.attrs(), {"vm_attr": "vm_value", "group_attr": "group_value"})
