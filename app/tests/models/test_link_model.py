from typing import Optional
from ..mongo_mock_test import MongoMockTest
from app.models import Link, Layout, Host, Group, Team, Role, User
from app.models.api.link import LinkResponse
from croydon.errors import InputDataError


class TestLinkModel(MongoMockTest):

    _user: Optional[User] = None
    _team: Optional[Team] = None
    _group: Optional[Group] = None
    _layout: Optional[Layout] = None

    async def user(self) -> User:
        if self._user is None:
            role = await Role.default()
            self._user = User.create(username="user", role_id=role.id)
            await self._user.save()
        return self._user

    async def team(self) -> Team:
        if self._team is None:
            user = await self.user()
            self._team = Team.create(name="team", owner_id=user.id)
            await self._team.save()
        return self._team

    async def group(self) -> Group:
        if self._group is None:
            team = await self.team()
            self._group = Group.create(name="group", team_id=team.id)
            await self._group.save()
        return self._group

    async def layout(self) -> Layout:
        if self._layout is None:
            team = await self.team()
            self._layout = Layout.create(name="layout", team_id=team.id)
            await self._layout.save()
        return self._layout

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await Link.destroy_all()
        await Layout.destroy_all()
        await Host.destroy_all()
        await Group.destroy_all()
        await Team.destroy_all()
        await User.destroy_all()
        self._user = None
        self._team = None
        self._group = None
        self._layout = None

    async def test_vm_response_model(self):
        self.assertCountEqual(Link.exposed_fields(), LinkResponse.__fields__.keys())

    async def test_basics(self):
        self.assertEqual(Link.collection, "links")
        self.assertEqual(Link.KEY_FIELD, "id")

    async def test_cascade_delete(self):
        group = await self.group()
        layout = await self.layout()

        h1 = Host.create(fqdn="host1", group_id=group.id)
        await h1.save()

        h2 = Host.create(fqdn="host2", group_id=group.id)
        await h2.save()

        h3 = Host.create(fqdn="host3", group_id=group.id)
        await h3.save()

        l1 = Link.create(layout_id=layout.id, source_id=h1.id, source_type="host", target_id=h2.id, target_type="host")
        await l1.save()

        l2 = Link.create(layout_id=layout.id, source_id=h2.id, source_type="host", target_id=h3.id, target_type="host")
        await l2.save()

        l3 = Link.create(layout_id=layout.id, source_id=h3.id, source_type="host", target_id=h1.id, target_type="host")
        await l3.save()

        link_ids = await Link.find_ids({})
        self.assertCountEqual([l1.id, l2.id, l3.id], link_ids)

        await h1.destroy()
        link_ids = await Link.find_ids({})
        self.assertCountEqual([l2.id], link_ids)

    async def test_invalid_model_type(self):
        group = await self.group()
        layout = await self.layout()
        h1 = Host.create(fqdn="host1", group_id=group.id)
        await h1.save()
        h2 = Host.create(fqdn="host2", group_id=group.id)
        await h2.save()

        link = Link.create(layout_id=layout.id, source_id=h1.id, source_type="invalid",
                           target_id=h2.id, target_type="host")
        with self.assertRaises(InputDataError):
            await link.save()

    async def test_incorrect_model_type(self):
        group = await self.group()
        layout = await self.layout()
        h1 = Host.create(fqdn="host1", group_id=group.id)
        await h1.save()
        h2 = Host.create(fqdn="host2", group_id=group.id)
        await h2.save()

        link = Link.create(layout_id=layout.id, source_id=h1.id, source_type="group",
                           target_id=h2.id, target_type="host")
        with self.assertRaises(InputDataError):
            await link.save()
