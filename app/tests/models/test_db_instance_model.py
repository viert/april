from ..mongo_mock_test import MongoMockTest
from croydon.errors import InputDataError, ObjectHasReferences
from app.models import VirtualMachine, Host, DBInstance
from app.models.api.db_instance import DBInstanceResponse


class TestDBInstanceModel(MongoMockTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await DBInstance.destroy_all()
        await Host.destroy_all()
        await VirtualMachine.destroy_all()

    async def test_host_references(self):
        db = DBInstance.create(name="db", db_type="mysql", port=3306)
        with self.assertRaises(InputDataError):
            await db.save()

        host = Host.create(fqdn="host.com")
        await host.save()

        db.host_id = host.id
        await db.save()

        with self.assertRaises(ObjectHasReferences):
            await host.destroy()

    async def test_host_response_model(self):
        self.assertCountEqual(DBInstance.exposed_fields(), DBInstanceResponse.__fields__.keys())
