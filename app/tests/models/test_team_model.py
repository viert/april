import asyncio

from ..mongo_mock_test import MongoMockTest
from app.models import Team, Group, Host, User, Role
from app.models.api.team import TeamResponse


class TestTeamModel(MongoMockTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await Host.destroy_all()
        await User.destroy_all()
        await Group.destroy_all()
        await Team.destroy_all()

    async def test_response_fields(self):
        self.assertCountEqual(
            Team.exposed_fields(),
            TeamResponse.__fields__.keys()
        )

    async def test_team_descendants_cache(self):
        role = await Role.default()

        user = User.create(username="user", role_id=role.id)
        await user.save()

        team = Team.create(name="team1", owner_id=user.id)
        await team.save()

        g1 = Group.create(name="g1", team_id=team.id)
        await g1.save()

        g2 = Group.create(name="g2", team_id=team.id)
        await g2.save()

        await g1.add_child(g2)

        g1_hosts = []
        g2_hosts = []
        detached_hosts = []
        all_hosts = []

        for i in range(1, 10):
            # hosts in g1
            host = Host.create(fqdn=f"host{i}.example.com", group_id=g1.id)
            await host.save()
            g1_hosts.append(host)
            all_hosts.append(host)
            # make sure owners are inherited
            self.assertCountEqual(host.owner_ids, g1.owner_ids)
            self.assertCountEqual(host.owner_ids, [user.id])

        for i in range(11, 20):
            # hosts in g2
            host = Host.create(fqdn=f"host{i}.example.com", group_id=g2.id)
            await host.save()
            g2_hosts.append(host)
            all_hosts.append(host)
            # make sure owners are inherited
            self.assertCountEqual(host.owner_ids, g2.owner_ids)
            self.assertCountEqual(host.owner_ids, [user.id])

        for i in range(21, 30):
            # detached hosts
            host = Host.create(fqdn=f"host{i}.example.com")
            await host.save()
            detached_hosts.append(host)
            all_hosts.append(host)
            # make sure owners are empty
            self.assertCountEqual(host.owner_ids, [])

        user2 = User.create(username="user2", role_id=role.id)
        await user2.save()

        await team.add_member(user2)
        await self.run_tasks()

        await asyncio.gather(*[host.reload() for host in all_hosts])
        await g1.reload()
        await g2.reload()

        # make sure new owners have arrived to the hosts and groups
        self.assertCountEqual(g1.owner_ids, [user.id, user2.id])
        self.assertCountEqual(g2.owner_ids, [user.id, user2.id])

        for host in g1_hosts + g2_hosts:
            self.assertCountEqual(host.owner_ids, [user.id, user2.id])
        for host in detached_hosts:
            # but the detached hosts' owners are still empty
            self.assertCountEqual(host.owner_ids, [])

        user.username = "renamed"
        await user.save()

        await asyncio.gather(*[host.reload() for host in all_hosts])
        await g1.reload()
        await g2.reload()

        # make sure new username has arrived to the hosts and groups
        self.assertCountEqual(g1.owner_ids, [user.id, user2.id])
        self.assertCountEqual(g2.owner_ids, [user.id, user2.id])

        for host in g1_hosts + g2_hosts:
            self.assertCountEqual(host.owner_ids, [user.id, user2.id])
        for host in detached_hosts:
            # but the detached hosts' owners are still empty
            self.assertCountEqual(host.owner_ids, [])
