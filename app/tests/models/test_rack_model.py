from typing import Tuple
from ..mongo_mock_test import MongoMockTest
from app.models import Rack, Location
from app.models.api.rack import RackResponse
from app.lib.rbac import PermissionsNamespace


class TestRackModel(MongoMockTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await Rack.destroy_all()
        await Location.destroy_all()

    async def setup_tree(self) -> Tuple[Location, Location, Rack]:
        loc = Location.create(name="loc")
        await loc.save()

        subloc = Location.create(name="subloc")
        await subloc.save()

        await loc.add_child(subloc)

        rack = Rack.create(name="1", location_id=subloc.id, num_units=50)
        await rack.save()

        return loc, subloc, rack

    async def test_response_model(self):
        self.assertCountEqual(Rack.exposed_fields(), RackResponse.__fields__.keys())

    async def test_basics(self):
        self.assertEqual(Rack.collection, "racks")
        self.assertEqual(Rack.RBAC_NAMESPACE, PermissionsNamespace.RACKS)
        self.assertEqual(Rack.KEY_FIELD, "full_name")

    async def test_full_name(self):
        loc, subloc, rack = await self.setup_tree()

        self.assertEqual(rack.full_name, "loc.subloc.1")

        subloc.name = "subloc1"
        await subloc.save()
        await rack.reload()

        self.assertEqual(rack.full_name, "loc.subloc1.1")

        loc.name = "loc1"
        await loc.save()
        await rack.reload()

        self.assertEqual(rack.full_name, "loc1.subloc1.1")

    async def test_metadata(self):
        loc, subloc, rack = await self.setup_tree()
        tags = await rack.tags()
        attrs = await rack.attrs()
        self.assertCountEqual(tags, [])
        self.assertDictEqual(attrs, {})

        loc.local_tags = ["loctag"]
        loc.local_attrs = {"loc": True}
        await loc.save()

        subloc.local_tags = ["subloctag"]
        subloc.local_attrs = {"subloc": True}
        await subloc.save()

        rack.local_tags = ["racktag"]
        rack.local_attrs = {"rack": True}
        await rack.save()

        tags = await rack.tags()
        attrs = await rack.attrs()
        self.assertCountEqual(["loctag", "subloctag", "racktag"], tags)
        self.assertDictEqual({"loc": True, "subloc": True, "rack": True}, attrs)
