from ..mongo_mock_test import MongoMockTest
from croydon.errors import ValidationError, NotFound, ObjectSaveRequired, IntegrityError, ObjectHasReferences
from app.models import Group, Team, User, Role
from app.errors import CyclicChain, ChildDoesNotExist, ChildExists


class TestGroupModel(MongoMockTest):

    user: User
    team: Team

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await Group.destroy_all()
        await Team.destroy_all()
        await User.destroy_all()

        role = await Role.default()
        self.user = User.create(username="u", email="u@example.com", role_id=role.id)
        await self.user.save()
        self.team = Team.create(name="t", owner_id=self.user.id)
        await self.team.save()
        self.tc1.reset()

    async def test_basics(self):
        self.assertEqual(Group.collection, "groups")

    async def test_group_chain_integrity(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()

        g11 = Group.create(name="g11", team_id=self.team.id)
        await g11.save()
        g12 = Group.create(name="g12", team_id=self.team.id)
        await g12.save()
        await g1.add_child(g11)
        await g1.add_child(g12)

        await g11.reload()
        await g12.reload()
        self.assertEqual(g11.parent_id, g1.id)
        self.assertEqual(g12.parent_id, g1.id)
        child_ids = await g1.all_child_ids()
        actual_ids = [x.id for x in [g11, g12]]
        self.assertCountEqual(actual_ids, child_ids)

        g111 = Group.create(name="g111", team_id=self.team.id)
        await g111.save()
        await g11.add_child(g111)
        g112 = Group.create(name="g112", team_id=self.team.id)
        await g112.save()
        await g11.add_child(g112)

        g121 = Group.create(name="g121", team_id=self.team.id)
        await g121.save()
        await g12.add_child(g121)
        g122 = Group.create(name="g122", team_id=self.team.id)
        await g122.save()
        await g12.add_child(g122)

        child_ids = await g1.all_child_ids()
        actual_ids = [x.id for x in [g11, g12, g111, g112, g121, g122]]
        self.assertCountEqual(actual_ids, child_ids)

        await g1.remove_child(g11)
        child_ids = await g1.all_child_ids()
        actual_ids = [x.id for x in [g12, g121, g122]]
        self.assertCountEqual(actual_ids, child_ids)

    async def test_auto_assigned_fields(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()

        self.assertEqual(g1.team_name, self.team.name)
        self.assertCountEqual(g1.owner_ids, [self.user.id])

    async def test_invalid(self):
        g1 = Group.create(name="group")
        with self.assertRaises(ValidationError):
            await g1.save()
        g1.team_id = "non-existent"
        with self.assertRaises(NotFound):
            await g1.save()
        g1.team_id = self.team.id
        g1.name = "*invalid!chars"
        with self.assertRaises(ValidationError):
            await g1.save()

        g1.name = "valid_name"
        await g1.save()

    async def test_save_required(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        g2 = Group.create(name="g2", team_id=self.team.id)
        with self.assertRaises(ObjectSaveRequired):
            await g1.add_child(g2)

        await g1.save()
        with self.assertRaises(ObjectSaveRequired):
            await g1.add_child(g2)

        await g2.save()
        await g1.add_child(g2)
        await g1.reload()
        await g2.reload()
        self.assertCountEqual([g2.id], g1.child_ids)
        self.assertEqual(g1.id, g2.parent_id)

    async def test_cyclic_chain(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()

        with self.assertRaises(CyclicChain):
            await g1.add_child(g1)

        g2 = Group.create(name="g2", team_id=self.team.id)
        await g2.save()

        await g1.add_child(g2)

        with self.assertRaises(CyclicChain):
            await g2.add_child(g1)

    async def test_child_exists(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()

        g2 = Group.create(name="g2", team_id=self.team.id)
        await g2.save()

        await g1.add_child(g2)

        with self.assertRaises(ChildExists):
            await g1.add_child(g2)

        g3 = Group.create(name="g3", team_id=self.team.id)
        await g3.save()

        with self.assertRaises(ChildDoesNotExist):
            await g1.remove_child(g3)

    async def test_add_remove_children(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()

        g2 = Group.create(name="g2", team_id=self.team.id)
        await g2.save()

        g3 = Group.create(name="g3", team_id=self.team.id)
        await g3.save()

        await g1.add_child(g2)
        await g1.add_child(g3)

        self.assertCountEqual([g2.id, g3.id], g1.child_ids)
        self.assertEqual(g1.id, g2.parent_id)
        self.assertEqual(g1.id, g3.parent_id)

        await g1.remove_child(g2)
        self.assertCountEqual([g3.id], g1.child_ids)
        self.assertEqual(None, g2.parent_id)
        self.assertEqual(g1.id, g3.parent_id)

        await g1.remove_all_children()
        await g3.reload()
        self.assertEqual(None, g3.parent_id)
        self.assertCountEqual([], g1.child_ids)

    async def test_chain(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()
        g2 = Group.create(name="g2", team_id=self.team.id)
        await g2.save()
        g3 = Group.create(name="g3", team_id=self.team.id)
        await g3.save()
        g4 = Group.create(name="g4", team_id=self.team.id)
        await g4.save()

        await g1.add_child(g2)
        await g2.add_child(g3)
        await g3.add_child(g4)

        self.assertCountEqual([g1], await g1.chain())
        self.assertCountEqual([g1, g2], await g2.chain())
        self.assertCountEqual([g1, g2, g3], await g3.chain())
        self.assertCountEqual([g1, g2, g3, g4], await g4.chain())

    async def test_all_children(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()
        g2 = Group.create(name="g2", team_id=self.team.id)
        await g2.save()
        g3 = Group.create(name="g3", team_id=self.team.id)
        await g3.save()
        g4 = Group.create(name="g4", team_id=self.team.id)
        await g4.save()

        await g1.add_child(g2)
        await g2.add_child(g3)
        await g3.add_child(g4)

        self.assertCountEqual([g2, g3, g4], await g1.all_children())

    async def test_invalid_child(self):
        t = Team.create(name="another_team", owner_id=self.user.id)
        await t.save()

        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()
        g2 = Group.create(name="g2", team_id=t.id)
        await g2.save()

        with self.assertRaises(IntegrityError):
            await g1.add_child(g2)

    async def test_tags(self):
        g1 = Group.create(name="g1", team_id=self.team.id, local_tags=["g1"])
        await g1.save()
        g2 = Group.create(name="g2", team_id=self.team.id, local_tags=["g2"])
        await g2.save()
        g3 = Group.create(name="g3", team_id=self.team.id, local_tags=["g3"])
        await g3.save()

        await g1.add_child(g2)
        await g2.add_child(g3)

        self.assertCountEqual(["g1"], await g1.tags())
        self.assertCountEqual(["g1", "g2"], await g2.tags())
        self.assertCountEqual(["g1", "g2", "g3"], await g3.tags())

        self.tc1.reset()
        g1.local_tags.append("additional")
        await g1.save()

        self.assertCountEqual(["g1", "g2", "g3", "additional"], await g3.tags())
        self.tc1.called_once("delete", f"groups.{g3.id}._tags")
        self.tc1.reset()
        await g1.remove_child(g2)
        self.tc1.called_once("delete", f"groups.{g3.id}._tags")
        self.assertCountEqual(["g2", "g3"], await g3.tags())

    async def test_attrs(self):
        g1 = Group.create(name="g1", team_id=self.team.id, local_attrs={"a": {"group": "g1"}, "c": 19})
        await g1.save()
        g2 = Group.create(name="g2", team_id=self.team.id, local_attrs={"a": {"group": "g2", "hello": "world"}})
        await g2.save()
        g3 = Group.create(name="g3", team_id=self.team.id, local_attrs={"b": 42})
        await g3.save()

        await g1.add_child(g2)
        await g2.add_child(g3)

        self.assertDictEqual(
            {
                "a": {
                    "group": "g2",
                    "hello": "world",
                },
                "b": 42,
                "c": 19,
            },
            await g3.attrs()
        )

        await g1.remove_child(g2)
        g2.local_attrs["a"]["hello"] = "galaxy"
        await g2.save()
        self.assertDictEqual(
            {
                "a": {
                    "group": "g2",
                    "hello": "galaxy",
                },
                "b": 42,
            },
            await g3.attrs()
        )

        await g2.remove_child(g3)
        self.assertDictEqual({"b": 42}, await g3.attrs())

    async def test_destroy_behaviour(self):
        g1 = Group.create(name="g1", team_id=self.team.id)
        await g1.save()
        g2 = Group.create(name="g2", team_id=self.team.id)
        await g2.save()

        await g1.add_child(g2)

        with self.assertRaises(ObjectHasReferences):
            await g1.destroy()

        self.assertCountEqual([g2.id], g1.child_ids)
        await g2.destroy()
        await g1.reload()
        self.assertCountEqual([], g1.child_ids)

    async def test_group_response_fields(self):
        from app.models.api.group import GroupResponse, GroupBaseResponse

        self.assertCountEqual(Group.exposed_base_fields(), GroupBaseResponse.__fields__.keys())
        self.assertCountEqual(Group.exposed_fields(), GroupResponse.__fields__.keys())
