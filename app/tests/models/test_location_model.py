from typing import Tuple
from ..mongo_mock_test import MongoMockTest
from app.models import Rack, Location
from app.models.api.location import LocationResponse
from app.lib.rbac import PermissionsNamespace


class TestLocationModel(MongoMockTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await Rack.destroy_all()
        await Location.destroy_all()

    async def setup_tree(self) -> Tuple[Location, Location, Rack]:
        loc = Location.create(name="loc")
        await loc.save()

        subloc = Location.create(name="subloc")
        await subloc.save()

        await loc.add_child(subloc)

        rack = Rack.create(name="1", location_id=subloc.id, num_units=50)
        await rack.save()

        return loc, subloc, rack

    async def test_response_model(self):
        self.assertCountEqual(Location.exposed_fields(), LocationResponse.__fields__.keys())

    async def test_basics(self):
        self.assertEqual(Location.collection, "locations")
        self.assertEqual(Location.RBAC_NAMESPACE, PermissionsNamespace.LOCATIONS)
        self.assertEqual(Location.KEY_FIELD, "full_name")

    async def test_full_name(self):
        loc, subloc, _ = await self.setup_tree()

        self.assertEqual(subloc.full_name, "loc.subloc")

        loc.name = "loc1"
        await loc.save()
        await subloc.reload()

        self.assertEqual(subloc.full_name, "loc1.subloc")

    async def test_metadata(self):
        loc, subloc, _ = await self.setup_tree()
        tags = await subloc.tags()
        attrs = await subloc.attrs()
        self.assertCountEqual(tags, [])
        self.assertDictEqual(attrs, {})

        loc.local_tags = ["loctag"]
        loc.local_attrs = {"loc": True}
        await loc.save()

        subloc.local_tags = ["subloctag"]
        subloc.local_attrs = {"subloc": True}
        await subloc.save()

        tags = await subloc.tags()
        attrs = await subloc.attrs()
        self.assertCountEqual(["loctag", "subloctag"], tags)
        self.assertDictEqual({"loc": True, "subloc": True}, attrs)
