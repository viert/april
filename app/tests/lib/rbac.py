from unittest import TestCase
from app.lib.rbac import DEFAULT_PERMISSIONS
from copy import deepcopy


class TestRBAC(TestCase):

    def test_permissions_cmp(self):
        p1 = deepcopy(DEFAULT_PERMISSIONS)
        p2 = deepcopy(DEFAULT_PERMISSIONS)

        # check the permissions are not the same instance
        self.assertFalse(p1 is p2)
        # but they are essentially equal
        self.assertEqual(p1, p2)

        p1.users.others.update = True

        self.assertNotEqual(p1, p2)
        # check that p1 now exceeds p2
        self.assertTrue(p1.exceeds(p2))
        # but p2 does not exceed p1
        self.assertFalse(p2.exceeds(p1))

        p2.teams.others.update = True

        self.assertNotEqual(p1, p2)
        # check that p2 now exceeds p1
        self.assertTrue(p2.exceeds(p1))
        # check that p1 still exceeds p2
        self.assertTrue(p1.exceeds(p2))
