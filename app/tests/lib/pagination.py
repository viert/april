from dataclasses import dataclass
from croydon.models import StorableModel
from croydon.models.fields import Field
from app.lib.pagination import paginated, PaginationParams
from ..mongo_mock_test import MongoMockTest


class TestPagination(MongoMockTest):

    async def test_pagination(self):
        class Model(StorableModel):
            COLLECTION = "models"
            a = Field()

            @dataclass
            class Response:
                a: str

        async def transform(model: Model) -> Model.Response:
            dct = await model.to_dict_ext(fields=["a"])
            return Model.Response(**dct)

        for i in range(100):
            m = Model.create(a=i)
            await m.save()

        cur = Model.find({})
        data = await paginated(cur.sort("a", 1), transform=transform, params=PaginationParams(page=3, limit=10))
        self.assertEqual(data.count, 100)
        self.assertEqual(data.page, 3)
        self.assertEqual(data.total_pages, 10)
        nums = [x.a for x in data.data]
        self.assertCountEqual(nums, list(range(20, 30)))

        cur = Model.find({"a": {"$gte": 50}})
        data = await paginated(cur.sort("a", 1), transform=transform, params=PaginationParams(page=3, limit=10))
        self.assertEqual(data.count, 50)
        self.assertEqual(data.page, 3)
        self.assertEqual(data.total_pages, 5)
        nums = [x.a for x in data.data]
        self.assertCountEqual(nums, list(range(70, 80)))
