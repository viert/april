from croydon import ctx
from httpx import Cookies
from ..app_controller_test import AppControllerTest
from app.models.session import Session


class TestAccountController(AppControllerTest):

    async def test_me(self):
        resp = await self.get("/api/v1/account/me", auth=False)
        self.assertEqual(resp.status_code, 401)

        resp = await self.get("/api/v1/account/me")
        self.assertEqual(resp.status_code, 200)

        data = resp.json()
        self.assertEqual(data["id"], str(self.user.id))

    async def test_authenticate(self):
        resp = await self.get("/api/v1/account/me", auth=False)

        session_id = resp.cookies.get(ctx.cfg.session.cookie)
        session = await Session.get(session_id)
        self.assertIsNone(session)

        cookies = Cookies({ctx.cfg.session.cookie: session_id})
        auth_data = {
            "username": self.user.username,
            "password": self.user_password,
        }
        self.client.cookies = cookies
        resp = self.client.post("/api/v1/account/authenticate", json=auth_data)
        self.assertEqual(resp.status_code, 200)

        session_id = resp.cookies.get(ctx.cfg.session.cookie)
        session = await Session.get(session_id)
        self.assertIsNotNone(session)
        self.assertEqual(session.user_id, self.user.id)
