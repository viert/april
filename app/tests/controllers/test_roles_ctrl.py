from typing import Optional, Dict, Any, Union
from copy import deepcopy
from croydon.errors import ModelDestroyed
from app.models import Role
from app.lib.rbac import DEFAULT_PERMISSIONS

from ..app_controller_test import AppControllerTest

API_PREFIX = "/api/v1/roles"


class TestRolesController(AppControllerTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        default = await Role.default()
        superuser = await Role.superuser()
        await Role.destroy_many({"_id": {"$not": {"$in": [default.id, superuser.id]}}})

    @staticmethod
    async def create_roles(r: Union[range, int], copy_from: Optional[Role] = None):
        if copy_from is None:
            copy_from = await Role.default()

        if isinstance(r, int):
            r = range(1, r + 1)
        for i in r:
            role = Role.create(name=f"test_role_{i}", stored_permissions=copy_from.stored_permissions)
            await role.save()

    async def test_roles_index_default(self):
        resp = await self.get(f"{API_PREFIX}/")
        data = resp.json()
        self.assertEqual(data["count"], 2)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 1)

        names = [item["name"] for item in data["data"]]
        default = await Role.default()
        superuser = await Role.superuser()
        self.assertCountEqual(names, [default.name, superuser.name])

    async def test_roles_index_pagination(self):
        await self.create_roles(28)  # plus two existing defaults

        resp = await self.get(f"{API_PREFIX}/?limit=10")
        data = resp.json()
        self.assertEqual(data["count"], 30)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 3)

        role_names = [x["name"] for x in data["data"]]
        self.assertCountEqual(
            role_names,
            [
                "default",
                "superuser",
                "test_role_1",
                "test_role_10",
                "test_role_11",
                "test_role_12",
                "test_role_13",
                "test_role_14",
                "test_role_15",
                "test_role_16",
            ]
        )

        resp = await self.get(f"{API_PREFIX}/?limit=10&page=2")
        data = resp.json()
        self.assertEqual(data["count"], 30)
        self.assertEqual(data["page"], 2)
        self.assertEqual(data["total_pages"], 3)

        role_names = [x["name"] for x in data["data"]]
        self.assertCountEqual(
            role_names,
            [
                "test_role_17",
                "test_role_18",
                "test_role_19",
                "test_role_2",
                "test_role_20",
                "test_role_21",
                "test_role_22",
                "test_role_23",
                "test_role_24",
                "test_role_25",
            ]
        )

    async def test_index_name_filter(self):
        await self.create_roles(20)

        resp = await self.get(f"{API_PREFIX}/?name=def")
        data = resp.json()
        self.assertEqual(data["count"], 1)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 1)
        role_names = [x["name"] for x in data["data"]]
        self.assertCountEqual(role_names, ["default"])

    async def test_index_fields(self):
        resp = await self.get(f"{API_PREFIX}/?fields=name,stored_permissions,doesntexist")
        data = resp.json()
        self.assertEqual(data["count"], 2)
        dct: Dict[str, Any] = data["data"][0]
        self.assertCountEqual(dct.keys(), ["name", "stored_permissions"])

    async def test_index_permissions(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.roles.all.read = False
        t = await self.user.create_token(permissions=p.dict())
        resp = await self.get(f"{API_PREFIX}/", token=t)
        data = resp.json()
        self.assertEqual(data["count"], 0)

    async def test_show(self):
        role = await Role.default()
        resp = await self.get(f"{API_PREFIX}/{role.id}")
        self.assertEqual(resp.status_code, 200)
        data = resp.json()
        self.assertEqual(data["id"], str(role.id))
        self.assertEqual(data["name"], role.name)
        self.assertEqual(data["stored_permissions"], role.stored_permissions)

    async def test_show_fields(self):
        role = await Role.default()
        resp = await self.get(f"{API_PREFIX}/{role.id}?fields=name,doesntexist")
        self.assertEqual(resp.status_code, 200)
        data = resp.json()
        self.assertCountEqual(data.keys(), ["name"])

    async def test_show_no_permissions(self):
        role = await Role.default()
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.roles.all.read = False
        t = await self.user.create_token(permissions=p.dict())
        resp = await self.get(f"{API_PREFIX}/{role.id}", token=t)
        self.assertEqual(resp.status_code, 404)

    async def test_create(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.users.others.read = False

        payload = {
            "name": "test_role",
            "stored_permissions": p.dict()
        }
        resp = await self.post(f"{API_PREFIX}/", data=payload)
        self.assertEqual(resp.status_code, 201)

        role = await Role.get("test_role")
        self.assertIsNotNone(role)

        tp = role.permissions()
        self.assertFalse(tp.users.others.read)
        tp.users.others.read = True
        self.assertEqual(tp, DEFAULT_PERMISSIONS)

    async def test_create_no_permissions(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.roles.all.create = False
        payload = {
            "name": "test_role",
            "stored_permissions": p.dict()
        }
        t = await self.user.create_token(permissions=p.dict())
        resp = await self.post(f"{API_PREFIX}/", data=payload, token=t)
        self.assertEqual(resp.status_code, 403)

    async def test_update(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        r = Role.create(name="test_role", stored_permissions=p.dict())
        await r.save()

        self.assertTrue(r.permissions().users.others.read)

        p.users.others.read = False
        payload = {
            "stored_permissions": p.dict()
        }

        resp = await self.patch(f"{API_PREFIX}/{r.id}", data=payload, user=self.superuser)
        self.assertEqual(resp.status_code, 200)

        await r.reload()
        self.assertFalse(r.permissions().users.others.read)

    async def test_update_no_permissions(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        r = Role.create(name="test_role", stored_permissions=p.dict())
        await r.save()
        payload = {
            "stored_permissions": p.dict()
        }

        resp = await self.patch(f"{API_PREFIX}/{r.id}", data=payload)
        self.assertEqual(resp.status_code, 403)

    async def test_delete(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        r = Role.create(name="test_role", stored_permissions=p.dict())
        await r.save()

        resp = await self.delete(f"{API_PREFIX}/{r.id}", user=self.superuser)
        self.assertEqual(resp.status_code, 200)

        with self.assertRaises(ModelDestroyed):
            await r.reload()

    async def test_delete_no_permissions(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        r = Role.create(name="test_role", stored_permissions=p.dict())
        await r.save()

        resp = await self.delete(f"{API_PREFIX}/{r.id}")
        self.assertEqual(resp.status_code, 403)
