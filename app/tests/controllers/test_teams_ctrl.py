from typing import Optional, Dict, Any, Union
from copy import deepcopy
from app.models import Team, User
from app.lib.rbac import DEFAULT_PERMISSIONS

from ..app_controller_test import AppControllerTest

API_PREFIX = "/api/v1/teams"


class TestTeamsController(AppControllerTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await Team.destroy_all()

    async def create_teams(self, r: Union[range, int], user: Optional[User] = None):
        if user is None:
            user = self.user
        if isinstance(r, int):
            r = range(1, r + 1)
        for i in r:
            team = Team.create(name=f"team{i}", owner_id=user.id)
            await team.save()

    async def test_index_empty(self):
        resp = await self.get(f"{API_PREFIX}/")
        data = resp.json()
        self.assertEqual(data["count"], 0)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 0)
        self.assertCountEqual(data["data"], [])

    async def test_index_pagination(self):
        await self.create_teams(30)

        resp = await self.get(f"{API_PREFIX}/?limit=10")
        data = resp.json()
        self.assertEqual(data["count"], 30)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 3)

        team_names = [x["name"] for x in data["data"]]
        self.assertCountEqual(
            team_names,
            ["team1", "team10", "team11", "team12", "team13", "team14", "team15", "team16", "team17", "team18"]
        )

        resp = await self.get(f"{API_PREFIX}/?limit=10&page=2")
        data = resp.json()
        self.assertEqual(data["count"], 30)
        self.assertEqual(data["page"], 2)
        self.assertEqual(data["total_pages"], 3)

        team_names = [x["name"] for x in data["data"]]
        self.assertCountEqual(
            team_names,
            ["team19", "team2", "team20", "team21", "team22", "team23", "team24", "team25", "team26", "team27"]
        )

    async def test_index_mine_filter(self):
        await self.create_teams(range(1, 6))
        await self.create_teams(range(6, 11), user=self.superuser)

        resp = await self.get(f"{API_PREFIX}/")
        data = resp.json()
        self.assertEqual(data["count"], 10)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 1)

        resp = await self.get(f"{API_PREFIX}/?mine_only=true")
        data = resp.json()
        self.assertEqual(data["count"], 5)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 1)

        team_names = [x["name"] for x in data["data"]]
        self.assertCountEqual(
            team_names,
            ["team1", "team2", "team3", "team4", "team5"]
        )

    async def test_index_name_filter(self):
        await self.create_teams(21)

        resp = await self.get(f"{API_PREFIX}/?name=team2")
        data = resp.json()
        self.assertEqual(data["count"], 3)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 1)
        team_names = [x["name"] for x in data["data"]]
        self.assertCountEqual(team_names, ["team2", "team20", "team21"])

    async def test_index_fields(self):
        await self.create_teams(1)

        resp = await self.get(f"{API_PREFIX}/?fields=id,name")
        data = resp.json()
        self.assertEqual(data["count"], 1)
        dct: Dict[str, Any] = data["data"][0]
        self.assertCountEqual(dct.keys(), ["id", "name"])

    async def test_index_permissions(self):
        team_owned = Team.create(name="team_owned", owner_id=self.user.id)
        await team_owned.save()

        team_member = Team.create(name="team_member", owner_id=self.superuser.id, member_ids=[self.user.id])
        await team_member.save()

        team_other = Team.create(name="team_other", owner_id=self.superuser.id)
        await team_other.save()

        p = deepcopy(DEFAULT_PERMISSIONS)
        p.teams.others.read = False
        p.teams.member.read = False
        t_owned_only = await self.user.create_token(permissions=p.dict())

        p = deepcopy(DEFAULT_PERMISSIONS)
        p.teams.owned.read = False
        p.teams.others.read = False
        t_member_only = await self.user.create_token(permissions=p.dict())

        p = deepcopy(DEFAULT_PERMISSIONS)
        p.teams.owned.read = False
        p.teams.member.read = False
        t_others_only = await self.user.create_token(permissions=p.dict())

        resp = await self.get(f"{API_PREFIX}/", token=t_owned_only)
        data = resp.json()
        self.assertEqual(data["count"], 1)
        dct: Dict[str, Any] = data["data"][0]
        self.assertEqual(dct["name"], team_owned.name)

        resp = await self.get(f"{API_PREFIX}/", token=t_member_only)
        data = resp.json()
        self.assertEqual(data["count"], 1)
        dct: Dict[str, Any] = data["data"][0]
        self.assertEqual(dct["name"], team_member.name)

        resp = await self.get(f"{API_PREFIX}/", token=t_others_only)
        data = resp.json()
        self.assertEqual(data["count"], 1)
        dct: Dict[str, Any] = data["data"][0]
        self.assertEqual(dct["name"], team_other.name)

        p = deepcopy(DEFAULT_PERMISSIONS)
        p.teams.others.read = False
        t_member_and_owned = await self.user.create_token(permissions=p.dict())

        resp = await self.get(f"{API_PREFIX}/", token=t_member_and_owned)
        data = resp.json()
        self.assertEqual(data["count"], 2)
        team_names = [x["name"] for x in data["data"]]
        self.assertCountEqual(team_names, [team_owned.name, team_member.name])

    async def test_show(self):
        team = Team.create(name="team1", owner_id=self.user.id)
        await team.save()

        resp = await self.get(f"{API_PREFIX}/{team.id}")
        data = resp.json()
        self.assertEqual(data["id"], str(team.id))
        self.assertEqual(data["name"], team.name)

        resp = await self.get(f"{API_PREFIX}/{team.name}")
        data = resp.json()
        self.assertEqual(data["id"], str(team.id))
        self.assertEqual(data["name"], team.name)

        resp = await self.get(f"{API_PREFIX}/random")
        self.assertEqual(resp.status_code, 404)

    async def test_show_fields(self):
        team = Team.create(name="team1", owner_id=self.user.id)
        await team.save()

        resp = await self.get(f"{API_PREFIX}/{team.id}?fields=id,name")
        data = resp.json()
        self.assertCountEqual(data.keys(), ["id", "name"])

    async def test_show_no_permissions(self):
        team = Team.create(name="team1", owner_id=self.superuser.id)
        await team.save()

        p = deepcopy(DEFAULT_PERMISSIONS)
        p.teams.others.read = False
        t = await self.user.create_token(permissions=p.dict())

        resp = await self.get(f"{API_PREFIX}/{team.id}", token=t)
        self.assertEqual(resp.status_code, 404)

    async def test_create(self):
        payload = {"name": "hello_team"}
        resp = await self.post(f"{API_PREFIX}/", data=payload)
        self.assertEqual(resp.status_code, 201, resp.content)

        data = resp.json()
        self.assertEqual(data["data"]["name"], payload["name"])

        team = await Team.get(data["data"]["id"])
        self.assertIsNotNone(team)
        self.assertEqual(team.name, payload["name"])
        self.assertEqual(team.owner_id, self.user.id)

    async def test_create_no_permissions(self):
        payload = {"name": "hello_team"}
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.teams.others.create = False
        t = await self.user.create_token(permissions=p.dict())
        resp = await self.post(f"{API_PREFIX}/", data=payload, token=t)
        self.assertEqual(resp.status_code, 403, resp.content)
        self.assertEqual(await Team.find({}).count(), 0)

    async def test_update(self):
        team = Team.create(name="team1", owner_id=self.user.id)
        await team.save()

        payload = {"name": "hello_team"}
        resp = await self.patch(f"{API_PREFIX}/{team.id}", data=payload)
        self.assertEqual(resp.status_code, 200)

        data = resp.json()
        self.assertEqual(data["data"]["name"], payload["name"])

        await team.reload()
        self.assertEqual(team.name, payload["name"])

    async def test_update_no_permissions(self):
        team = Team.create(name="team1", owner_id=self.user.id)
        await team.save()

        payload = {"name": "hello_team"}
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.teams.owned.update = False
        t = await self.user.create_token(permissions=p.dict())
        resp = await self.patch(f"{API_PREFIX}/{team.id}", data=payload, token=t)
        self.assertEqual(resp.status_code, 403)

    async def test_delete(self):
        team = Team.create(name="team1", owner_id=self.user.id)
        await team.save()

        self.assertEqual(await Team.find({}).count(), 1)

        resp = await self.delete(f"{API_PREFIX}/{team.id}")
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(await Team.find({}).count(), 0)

    async def test_delete_no_permissions(self):
        team = Team.create(name="team1", owner_id=self.superuser.id)
        await team.save()

        resp = await self.delete(f"{API_PREFIX}/{team.id}")
        self.assertEqual(resp.status_code, 403)

    async def test_change_owner(self):
        team = Team.create(name="team1", owner_id=self.user.id)
        await team.save()

        payload = {"owner_id": str(self.superuser.id)}
        resp = await self.post(f"{API_PREFIX}/{team.id}/owner", data=payload)
        self.assertEqual(resp.status_code, 200)

        await team.reload()
        self.assertEqual(team.owner_id, self.superuser.id)

    async def test_set_members(self):
        team = Team.create(name="team1", owner_id=self.user.id)
        await team.save()

        self.assertCountEqual(team.member_ids, [])

        payload = {"member_ids": [str(self.superuser.id)]}
        resp = await self.post(f"{API_PREFIX}/{team.id}/members", data=payload)
        self.assertEqual(resp.status_code, 200)

        await team.reload()
        self.assertCountEqual(team.member_ids, [self.superuser.id])
