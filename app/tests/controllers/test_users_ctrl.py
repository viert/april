from typing import Optional, Dict, Any, Union
from copy import deepcopy
from croydon.errors import ModelDestroyed
from app.models import User, Role, Token
from app.lib.rbac import DEFAULT_PERMISSIONS

from ..app_controller_test import AppControllerTest

API_PREFIX = "/api/v1/users"


class TestUsersController(AppControllerTest):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        await User.destroy_many({"_id": {"$not": {"$in": [self.user.id, self.superuser.id]}}})

    @staticmethod
    async def create_users(r: Union[range, int], role: Optional[Role] = None):
        if role is None:
            role = await Role.default()
        if isinstance(r, int):
            r = range(1, r + 1)
        for i in r:
            user = User.create(
                username=f"test_user_{i}",
                email=f"test_user_{i}@example.com",
                role_id=role.id
            )
            await user.save()

    @staticmethod
    async def user_t():
        r = await Role.default()
        u = User.create(
            username="t",
            email="t@example.com",
            role_id=r.id,
        )
        u.set_password("password1")
        await u.save()
        return u

    async def test_users_index_default(self):
        resp = await self.get(f"{API_PREFIX}/")
        data = resp.json()
        self.assertEqual(data["count"], 2)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 1)

        names = [item["username"] for item in data["data"]]
        self.assertCountEqual(names, [self.user.username, self.superuser.username])

    async def test_users_index_pagination(self):
        await self.create_users(28)  # plus two existing defaults

        resp = await self.get(f"{API_PREFIX}/?limit=10")
        data = resp.json()
        self.assertEqual(data["count"], 30)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 3)

        user_names = [x["username"] for x in data["data"]]
        self.assertCountEqual(
            user_names,
            [
                "root",
                "test_user",
                "test_user_1",
                "test_user_10",
                "test_user_11",
                "test_user_12",
                "test_user_13",
                "test_user_14",
                "test_user_15",
                "test_user_16",
            ]
        )

        resp = await self.get(f"{API_PREFIX}/?limit=10&page=2")
        data = resp.json()
        self.assertEqual(data["count"], 30)
        self.assertEqual(data["page"], 2)
        self.assertEqual(data["total_pages"], 3)

        user_names = [x["username"] for x in data["data"]]
        self.assertCountEqual(
            user_names,
            [
                "test_user_17",
                "test_user_18",
                "test_user_19",
                "test_user_2",
                "test_user_20",
                "test_user_21",
                "test_user_22",
                "test_user_23",
                "test_user_24",
                "test_user_25",
            ]
        )

    async def test_index_name_filter(self):
        await self.create_users(20)

        resp = await self.get(f"{API_PREFIX}/?name=roo")
        data = resp.json()
        self.assertEqual(data["count"], 1)
        self.assertEqual(data["page"], 1)
        self.assertEqual(data["total_pages"], 1)
        user_names = [x["username"] for x in data["data"]]
        self.assertCountEqual(user_names, ["root"])

    async def test_index_fields(self):
        resp = await self.get(f"{API_PREFIX}/?fields=username,email,doesntexist")
        data = resp.json()
        self.assertEqual(data["count"], 2)
        dct: Dict[str, Any] = data["data"][0]
        self.assertCountEqual(dct.keys(), ["username", "email"])

    async def test_index_permissions(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.users.others.read = False
        t = await self.user.create_token(permissions=p.dict())
        resp = await self.get(f"{API_PREFIX}/", token=t)
        data = resp.json()
        self.assertEqual(data["count"], 1)
        self.assertEqual(data["data"][0]["username"], self.user.username)

    async def test_show(self):
        resp = await self.get(f"{API_PREFIX}/{self.user.id}")
        self.assertEqual(resp.status_code, 200)
        data = resp.json()
        self.assertEqual(data["id"], str(self.user.id))
        self.assertEqual(data["username"], self.user.username)
        self.assertEqual(data["email"], self.user.email)

    async def test_show_fields(self):
        resp = await self.get(f"{API_PREFIX}/{self.user.id}?fields=username,email,doesntexist")
        self.assertEqual(resp.status_code, 200)
        data = resp.json()
        self.assertCountEqual(data.keys(), ["username", "email"])

    async def test_show_no_permissions(self):
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.users.myself.read = False
        t = await self.user.create_token(permissions=p.dict())
        resp = await self.get(f"{API_PREFIX}/{self.user.id}", token=t)
        self.assertEqual(resp.status_code, 404)

    async def test_create(self):
        role = await Role.default()
        payload = {
            "username": "test_username",
            "email": "test_username@example.com",
            "role_id": str(role.id)
        }
        resp = await self.post(f"{API_PREFIX}/", data=payload, user=self.superuser)
        self.assertEqual(resp.status_code, 201)

        user = await User.get("test_username")
        self.assertIsNotNone(user)
        self.assertEqual(user.email, "test_username@example.com")

    async def test_create_no_permissions(self):
        role = await Role.default()

        payload = {
            "username": "test_username",
            "email": "test_username@example.com",
            "role_id": str(role.id)
        }
        resp = await self.post(f"{API_PREFIX}/", data=payload)
        self.assertEqual(resp.status_code, 403)

    async def test_update(self):
        u = await self.user_t()

        payload = {
            "username": "t2"
        }

        resp = await self.patch(f"{API_PREFIX}/{u.id}", data=payload, user=u)
        self.assertEqual(resp.status_code, 200, resp.content)
        await u.reload()
        self.assertEqual(u.username, "t2")

        superrole = await Role.superuser()
        payload["role_id"] = str(superrole.id)

        resp = await self.patch(f"{API_PREFIX}/{u.id}", data=payload, user=u)
        self.assertEqual(403, resp.status_code)

    async def test_update_no_permissions(self):
        u = await self.user_t()

        payload = {
            "username": "t2"
        }

        resp = await self.patch(f"{API_PREFIX}/{u.id}", data=payload)
        self.assertEqual(403, resp.status_code, resp.content)

    async def test_delete(self):
        u = await self.user_t()

        resp = await self.delete(f"{API_PREFIX}/{u.id}", user=self.superuser)
        self.assertEqual(200, resp.status_code, resp.content)

        with self.assertRaises(ModelDestroyed):
            await u.reload()

    async def test_delete_no_permissions(self):
        u = await self.user_t()

        resp = await self.delete(f"{API_PREFIX}/{u.id}")
        self.assertEqual(403, resp.status_code, resp.content)

    async def test_set_password(self):
        u = await self.user_t()
        u.set_password("password1")
        await u.save()

        self.assertTrue(u.check_password("password1"))

        payload = {
            "password": "password2",
            "password_confirm": "password2"
        }

        resp = await self.post(f"{API_PREFIX}/{u.id}/set_password", data=payload, user=u)
        self.assertEqual(200, resp.status_code, resp.content)

        await u.reload()
        self.assertFalse(u.check_password("password1"))
        self.assertTrue(u.check_password("password2"))

    async def test_create_token(self):
        u = await self.user_t()
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.users.others.read = False

        payload = {
            "permissions": p.dict()
        }

        resp = await self.post(f"{API_PREFIX}/{u.id}/tokens", data=payload, user=u)
        self.assertEqual(201, resp.status_code, resp.content)
        data = resp.json()
        self.assertIn("token", data)
        token = await Token.get(data["token"])
        self.assertIsNotNone(token)
        self.assertEqual(token.permissions(), p)

        p.users.others.assign_exceeding_roles = True
        payload = {
            "permissions": p.dict()
        }

        resp = await self.post(f"{API_PREFIX}/{u.id}/tokens", data=payload, user=u)
        self.assertEqual(403, resp.status_code, resp.content)

    async def test_revoke_token(self):
        u = await self.user_t()
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.users.others.read = False
        t = await u.create_token(permissions=p.dict())

        self.assertIsNotNone(t)
        resp = await self.delete(f"{API_PREFIX}/{u.id}/tokens/{t.token}", user=u)
        self.assertEqual(200, resp.status_code, resp.content)

        with self.assertRaises(ModelDestroyed):
            await t.reload()

    async def test_revoke_all_tokens(self):
        u = await self.user_t()
        p = deepcopy(DEFAULT_PERMISSIONS)
        p.users.others.read = False
        t1 = await u.create_token(permissions=p.dict())
        t2 = await u.create_token(permissions=p.dict())

        self.assertIsNotNone(t1)
        self.assertIsNotNone(t2)

        count = await u.tokens().count()
        self.assertEqual(2, count)

        resp = await self.delete(f"{API_PREFIX}/{u.id}/tokens", user=u)
        self.assertEqual(200, resp.status_code, resp.content)

        count = await u.tokens().count()
        self.assertEqual(0, count)
