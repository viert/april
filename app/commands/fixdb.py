from croydon.command import Command
from app.models import Location, Host, Group, Team, Rack


class FixDB(Command):

    NAME = "fixdb"
    HELP = "restores links between models and inherited properties"

    async def run_async(self) -> None:
        from app import app
        app.initialise()

        async for team in Team.find({}):
            await team.save()
        async for group in Group.find({}):
            await group.save()
        async for host in Host.find({}):
            await host.save()

        async for dc in Location.find({}):
            await dc.save()
        async for rack in Rack.find({}):
            await rack.save()
