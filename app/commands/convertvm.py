from croydon.command import Command
from app.models import VirtualMachine, Host, Group, Team, Rack


class ConvertVM(Command):

    NAME = "convertvm"
    HELP = "converts virtual machines to vm-type hosts"

    async def run_async(self) -> None:
        for vm in await VirtualMachine.find({}).all():
            if vm.vm_type != "docker":
                fqdn = vm.fqdn
                if "." not in vm.fqdn:
                    vm_host = await vm.host()
                    domain = vm_host.fqdn.split(".")[-1]
                    fqdn = f"{vm.fqdn}.{domain}"

                ex_host = await Host.get(fqdn)
                if ex_host:
                    print(f"converting to existing {fqdn}")
                    ex_host.vm_type = vm.vm_type
                    ex_host.vcpu = vm.vcpu
                    ex_host.vm_host_id = vm.host_id
                    ex_host.memory = vm.memory
                    ex_host.image = vm.image
                    ex_host.status = vm.status
                    ex_host.rack_id = None
                    await ex_host.save()
                else:
                    print(f"converting to new {fqdn}")
                    host = Host.create(
                        fqdn=fqdn,
                        group_id=vm.group_id,
                        description=vm.description,
                        local_tags=vm.local_tags,
                        local_attrs=vm.local_attrs,
                        vm_host_id=vm.host_id,
                        vm_type=vm.vm_type,
                        vcpu=vm.vcpu,
                        memory=vm.memory,
                        image=vm.image,
                        status=vm.status,
                    )
                    await host.save()
            await vm.destroy()
